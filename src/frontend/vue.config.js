// { defineConfig } = require('@vue/cli-service')
// vue.config.js



module.exports = {
  // https://cli.vuejs.org/config/#devserver-proxy
  devServer: {
    port: 3000,
    proxy: {
      '/api': {
        target: 'http://localhost:8081',
        ws: true,
        changeOrigin: true
      },
      '/output': {
        target: 'http://localhost:8081',
        changeOrigin: true
      }

    }
  },

  productionSourceMap: false, // Üretimde kaynak haritalarını devre dışı bırakır
  chainWebpack: config => {
    config.optimization.minimize(true); // Dosyaları küçültme işlemi
    if (process.env.NODE_ENV === 'production') {
      config.optimization.splitChunks({
        cacheGroups: {
          vendors: {
            name: 'chunk-vendors',
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
            chunks: 'initial'
          },
          common: {
            name: 'chunk-common',
            minChunks: 2,
            priority: -20,
            chunks: 'initial',
            reuseExistingChunk: true
          }
        }
      });
    }
  },









}
/*const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
    port: 3000,
    proxy:{
      '/api/test/': {
        target:"http://localhost:8080",
        ws: true,
        changeOrigin: true
      },

    }
  }
})*/


/*
module.exports = {
    devServer: {
        port: 8081
    }
}*/
