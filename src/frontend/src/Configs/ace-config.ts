import ace from 'ace-builds';

import modeJsonUrl from 'ace-builds/src-noconflict/mode-json';
ace.config.setModuleUrl('ace/mode/json', modeJsonUrl);

import modeJavascriptUrl from 'ace-builds/src-noconflict/mode-javascript';
ace.config.setModuleUrl('ace/mode/javascript', modeJavascriptUrl);

import modeHtmlUrl from 'ace-builds/src-noconflict/mode-html';
ace.config.setModuleUrl('ace/mode/html', modeHtmlUrl);

import modeYamlUrl from 'ace-builds/src-noconflict/mode-yaml';
ace.config.setModuleUrl('ace/mode/yaml', modeYamlUrl);

import themeGithubUrl from 'ace-builds/src-noconflict/theme-github';
ace.config.setModuleUrl('ace/theme/github', themeGithubUrl);

import themeChromeUrl from 'ace-builds/src-noconflict/theme-chrome';
ace.config.setModuleUrl('ace/theme/chrome', themeChromeUrl);

import themeMonokaiUrl from 'ace-builds/src-noconflict/theme-monokai';
ace.config.setModuleUrl('ace/theme/monokai', themeMonokaiUrl);

import workerBaseUrl from 'ace-builds/src-noconflict/worker-base';
// @ts-ignore
ace.config.setModuleUrl('ace/mode/base', workerBaseUrl);

import workerJsonUrl from 'ace-builds/src-noconflict/worker-json';
// @ts-ignore
ace.config.setModuleUrl('ace/mode/json_worker', workerJsonUrl);

import workerJavascriptUrl from 'ace-builds/src-noconflict/worker-javascript';
// @ts-ignore
ace.config.setModuleUrl('ace/mode/javascript_worker', workerJavascriptUrl);

import workerHtmlUrl from 'ace-builds/src-noconflict/worker-html';
// @ts-ignore
ace.config.setModuleUrl('ace/mode/html_worker', workerHtmlUrl);

import workerYamlUrl from 'ace-builds/src-noconflict/worker-yaml';
ace.config.setModuleUrl('ace/mode/yaml_worker', workerYamlUrl);

import snippetsHtmlUrl from 'ace-builds/src-noconflict/snippets/html';
ace.config.setModuleUrl('ace/snippets/html', snippetsHtmlUrl);

import snippetsJsUrl from 'ace-builds/src-noconflict/snippets/javascript';
ace.config.setModuleUrl('ace/snippets/javascript', snippetsJsUrl);

import snippetsYamlUrl from 'ace-builds/src-noconflict/snippets/yaml';
ace.config.setModuleUrl('ace/snippets/javascript', snippetsYamlUrl);

import snippetsJsonUrl from 'ace-builds/src-noconflict/snippets/json';
ace.config.setModuleUrl('ace/snippets/json', snippetsJsonUrl);

import 'ace-builds/src-noconflict/ext-language_tools';
ace.require("ace/ext/language_tools");