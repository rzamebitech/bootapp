import axios from 'axios';
import authHeader from './auth-header';
const API_ADMIN_URL = '/api/admin/';

class envelopeService {

    downloadEnvelope(data) {

        return axios.post(API_ADMIN_URL + 'download-envelope', null ,{headers: authHeader(),params:data});
    }

    downloadEnvelopes(data) {

        return axios.post(API_ADMIN_URL + 'download-envelopes', data ,{headers: authHeader(),responseType:"blob"});
    }







}
export default new envelopeService