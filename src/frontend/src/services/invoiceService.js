import axios from 'axios';
import authHeader from './auth-header';
const API_ADMIN_URL = '/api/admin/';

class InvoiceService {
    CheckInvoiceStatus(data) {

        return axios.post(API_ADMIN_URL + 'user/check-invoice', null ,{headers: authHeader(),params:data});
    }

   /* ModifyXml(data) {
        return axios.post(API_ADMIN_URL + 'user/modify-xml', null ,{headers: authHeader(),params:data,responseType:"blob"});
    }*/

    ModifyXml(data) {
        return axios.post(API_ADMIN_URL + 'user/modify-xml', data, {headers: authHeader(),responseType:"blob"});
    }

    SendXmlList(data) {
        return axios.post(API_ADMIN_URL + 'user/modify-xml-zip', data, {headers: authHeader(),responseType:"blob"});
    }

    SendDenyXml(data) {
        return axios.post(API_ADMIN_URL + 'user/denyQuery', data, {headers: authHeader(),responseType:"blob"});
    }
    AddStartToCSV(data) {
        return axios.post(API_ADMIN_URL + 'user/sendCsvAndChange', data, {headers: authHeader(),});
    }

    getXsltFromXml(data) {
        return axios.post(API_ADMIN_URL + 'user/getXsltFromXml', data, {headers: authHeader(),responseType:"blob"});

    }

    updateXslt(data) {
        return axios.post(API_ADMIN_URL + 'user/updateXslt', data, {headers: authHeader(),});
    }

    exportXsltRelatedData(data) {

        return axios.post(API_ADMIN_URL + 'user/exportXsltRelatedData', data ,{headers: authHeader(),responseType:"blob"});
    }
}

export default new InvoiceService();