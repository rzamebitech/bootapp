import axios from 'axios';
import authHeader from './auth-header';

/*const API_URL = '/api/test/';*/
const API_ADMIN_URL = '/api/admin/';


class QueueService {

    SendEmail(data) {
        return axios.post(API_ADMIN_URL + 'user/sendEmailsTest', data, {headers: authHeader(),});
    }

}

export default new QueueService();