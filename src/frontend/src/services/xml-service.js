import axios from 'axios';
import authHeader from './auth-header';

/*const API_URL = '/api/test/';*/
const API_ADMIN_URL = '/api/admin/';

class XmlService {

    SendXmlToServer(data) {
        return axios.post(API_ADMIN_URL + 'user/convertXml', data, {headers: authHeader(),});
    }

    getHtlContent(url) {
        return axios.post(API_ADMIN_URL + 'user/getHtmlContent', null, {headers: authHeader(), params: url});
    }

    CheckSchematron(data) {
        return axios.post(API_ADMIN_URL + 'user/xmlSchematron', data, {headers: authHeader(),});
    }

    SignXml(data){
        return axios.post(API_ADMIN_URL + 'user/signXml', data, {headers: authHeader(),});
    }

    CheckXml(data){
        return axios.post(API_ADMIN_URL + 'user/checkSign', data, {headers: authHeader(),});
    }


}

export default new XmlService();