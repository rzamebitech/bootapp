import axios from 'axios';
import authHeader from './auth-header';
const API_ADMIN_URL = '/api/admin/';

class LogService {


    requestLog(data) {
        return axios.post(API_ADMIN_URL + 'user/getLogSummary', data, {headers: authHeader(),});
    }


    requestYesterdayOrCustomLog(data) {
         return axios.post(API_ADMIN_URL + 'user/getYestedayLogSummary', data,{headers: authHeader()});
    }

}

export default new LogService();