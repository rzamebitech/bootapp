import axios from 'axios';
import authHeader from './auth-header';


const API_ADMIN_URL = '/api/admin/';

class ReportService{

    getReports(formData) {
        return axios.post(API_ADMIN_URL + 'reports',formData, {headers: authHeader()});
    }

    getIssues(currentPage,searchterm) {
        return axios.get(API_ADMIN_URL + 'search/issues?page='+currentPage+'&s='+searchterm, {headers: authHeader()});
    }

    getEmployess() {
        return axios.get(API_ADMIN_URL + 'search/employess', {headers: authHeader()});
    }

    searchCompnaies() {
        return axios.get(API_ADMIN_URL + 'search/companies', {headers: authHeader()});
    }

    saveReport(data) {
        return axios.post(API_ADMIN_URL + 'saveReport', data, {headers: authHeader(),});
    }

}

export default new ReportService();