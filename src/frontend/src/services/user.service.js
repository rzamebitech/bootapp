import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/test/';
const API_ADMIN_URL = '/api/admin/';

class UserService {
    getPublicContent() {
        return axios.get(API_URL + '');
    }

    getUserBoard() {

        return axios.get(API_URL + 'user', {headers: authHeader()});
    }

    getModeratorBoard() {
        return axios.get(API_URL + 'mod', {headers: authHeader()});
    }

    getAdminBoard() {
        console.info(authHeader())
        return axios.get(API_URL + 'admin', {headers: authHeader()});
    }
 CheckToken() {
        console.info(authHeader())
        return axios.get(API_ADMIN_URL + 'checkToken', {headers: authHeader()});
    }

    getAllUsers() {
        return axios.get(API_ADMIN_URL + 'users', {headers: authHeader()});
    }

    getAllRoles() {
        return axios.get(API_ADMIN_URL + 'roles/load', {headers: authHeader()});
    }

    ModifyUser(data) {

        return axios.post(API_ADMIN_URL + 'user/darkmode', null, {headers: authHeader(), params: data});
    }
    getUserRolesOnModal(data) {

        return axios.post(API_ADMIN_URL + 'user/loadUserModal', null, {headers: authHeader(), params: data});
    }

    ModifyUserModal(data) {

        return axios.post(API_ADMIN_URL + 'user/modifyUser', null, {headers: authHeader(), params: data});
    }
}

export default new UserService();