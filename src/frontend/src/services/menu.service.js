import axios from 'axios';
import authHeader from './auth-header';

/*const API_URL = '/api/test/';*/
const API_ADMIN_URL = '/api/admin/';

class MenuService {
    getMenuContent() {
        return axios.get(API_ADMIN_URL + 'MenuManagement', {headers: authHeader()});
    }

    getAllMenusForAdmin() {
        return axios.get(API_ADMIN_URL + 'menus/load', {headers: authHeader()});
    }

    getAllIcons() {
        return axios.get(API_ADMIN_URL + 'icons/load', {headers: authHeader()});
    }


}

export default new MenuService();