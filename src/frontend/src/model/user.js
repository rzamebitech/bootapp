export default class User {
    constructor(username, email, password, darkmode) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.darkmode = darkmode;
    }
}