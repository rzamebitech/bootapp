import {createWebHistory, createRouter} from 'vue-router'
import HomeView from "@/components/Home";
import Login from "@/components/Login";

import NotFound from "@/components/NotFound";
import Unauthorized from "@/components/Unauthorized";
import Register from "@/components/Register";
import ModifyXml from "@/components/ModifyXml";
import EnvelopeDownload from "@/components/EnvelopeDownloader";
import DenyQuery from "@/components/DenyQuery";
import UserManagement from "@/components/UserManagement";
import XsltEditor from "@/components/XsltEditor";
import LogViewer from "@/components/LogViewer";
import ReportView from "@/components/ReportView";


const Profile = () => import("@/components/Profile")
const BoardAdmin = () => import("@/components/BoardAdmin")
const BoardModerator = () => import("@/components/BoardModerator")
const BoardUser = () => import("@/components/BoardUser")

const XmlToPdfView = () => import("@/components/XmlToPdfView")
const CheckStatusAll = () => import("@/components/CheckStatusAll")

const LiveAudio = () => import("@/components/LiveAudio")

const routes = [
    {
        path: '/',
        name: "home",
        component: HomeView
    },
    {
        path: '/home',
        component: HomeView
    },
    {
        path: '/login',
        component: Login
    },


    {
        path: '/register',
        name: 'RegisterView',
        component: Register
    },
    {
        path: '/profile',
        name: 'ProfileView',
        // lazy-loaded
        component: Profile,
    },
    {
        path: '/admin',
        name: 'admin',
        component: BoardAdmin

    },


    {
        path: "/user",
        name: "user",
        // lazy-loaded
        component: BoardUser,
    },
    {
        path: '/mod',
        name: 'moderator',
        // lazy-loaded
        component: BoardModerator
    },
    {
        path: '/ModifyXml',
        name: 'ModifyXml',
        component: ModifyXml


    },
    {
        path: '/EnvelopeDownload',
        name: 'EnvelopeDownload',
        component: EnvelopeDownload


    },
    {
        path: '/DenyQuery',
        name: 'DenyQuery',
        component: DenyQuery


    },
    {
        path: '/XmlToPdfView',
        name: 'XmlToPdfView',
        component: XmlToPdfView


    },
    {
        path: '/CheckStatusAll',
        name: 'CheckStatusAll',
        component: CheckStatusAll
    },
    {
        path: '/XsltEditor',
        name: 'XsltEditor',
        component: XsltEditor,
    },
    {
        path: '/LogViewer',
        name: 'LogViewer',
        component: LogViewer,
    },
    {
        path: '/Report',
        name: 'ReportView',
        component: ReportView,
    },

    {
        path: '/LiveAudio',
        name: 'LiveAudio',
        component: LiveAudio
    },
    {
        path: '/UserManagement',
        name: 'UserManagement',
        component: UserManagement
    },

    {
        path: '/404',
        component: NotFound
    },
    {
        path: '/401',
        component: Unauthorized
    },
    {
        path: '/403',
        component: Login
    },
    {
        path: "/:catchAll(.*)",
        redirect: '/404'
    }
]

const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: "active",
    routes,
});


router.beforeEach((to, from, next) => {
    const publicPages = ['/login', '/register', '/home'];
    const authRequired = !publicPages.includes(to.path);
    //const authRequiredf = !publicPages.includes(from.path);
    const loggedIn = localStorage.getItem('user');

    if (authRequired && !loggedIn) {
        const loginpath = window.location.pathname;
        if(loginpath!=='/login'){
            //next({ name: '/login', query: { from: loginpath } });
            next('/login');
        }

    } else if (!authRequired && loggedIn) next('/login');
    else next();

  /*  if(authRequired && loggedIn!==null){
        let parsedJSON = JSON.parse(loggedIn)
        const jwtPayload = parseJwt(parsedJSON["token"]);
        if (jwtPayload.exp < Date.now()/1000) {
            // token expired

            next("/login");
        }
    }*/
    /* console.warn('authRequired : '  + authRequired)
     console.warn('loggedIn '+ loggedIn)*/
    // trying to access a restricted page + not logged in
    // redirect to login page
   /* if (authRequired &&  loggedIn==null) {
        next('/login');
    } else {
        next();
    }*/
});


/*function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}*/
/*registerInterceptor(router)*/
export default router