
import 'bootstrap/dist/css/bootstrap.css';

import {createApp} from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'



import VueObserveVisibility from 'vue-observe-visibility'

import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
import Sortable from 'vue-sortable'
import dayjs from 'dayjs';



import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

library.add(fas)
const app = createApp(App)
app.config.globalProperties.$dayjs = dayjs;
// Import Bootstrap an BootstrapVue CSS files (order is important)

/*import 'bootstrap-vue/dist/bootstrap-vue.css'*/



//////////////////////////////////////
app.use(router).use(store).use(Sortable).use(VueObserveVisibility)

app.component("font-awesome-icon", FontAwesomeIcon)
app.mount('#app')

import "bootstrap/dist/js/bootstrap.js"