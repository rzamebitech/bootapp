package com.mebi.bootapp.Security;

import com.mebi.bootapp.Model.ERole;
import com.mebi.bootapp.Model.Role;
import com.mebi.bootapp.Model.User;
import com.mebi.bootapp.Repository.RoleRepository;
import com.mebi.bootapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mebi.bootapp.Utilities.Log.*;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return UserDetailsImpl.build(user);
    }

    @Bean
    public void CreateAdminIfNotExist() {

        try {

            Optional<User> user = userRepository.findById(3l);
            if (!user.isPresent()) {
                //Create UserRole for first time #1
                Role usr = new Role();
                Role mdrt = new Role();
                Role admn = new Role();
                usr.setName(ERole.ROLE_USER);
                mdrt.setName(ERole.ROLE_MODERATOR);
                admn.setName(ERole.ROLE_ADMIN);

                ArrayList<Role> rls = new ArrayList<>();
                rls.add(usr);
                rls.add(mdrt);
                rls.add(admn);

                //roleRepository.saveAll(rls);
                System.out.println(ANSI_GREEN + "UserDeatilServiceImpl" + ANSI_RESET + " : UserRoles Inserted To DB");


                //Create Admin for first time
                User admin = new User();

                admin.setUsername("user@localhost.com");
                admin.setPassword(passwordEncoder.encode("123123"));
                admin.setName("User");


                Set<Role> roles = new HashSet<>();
                Role role = roleRepository.findRoleByName(ERole.ROLE_ADMIN);
                Role mod = roleRepository.findRoleByName(ERole.ROLE_MODERATOR);
                roles.add(role);
                roles.add(mod);


                admin.setRoles(roles);
                admin.setEmail(admin.getUsername());

                userRepository.save(admin);

                System.out.println(ANSI_GREEN + "UserDeatilServiceImpl" + ANSI_RESET + " : Admin has been created for first time");

            }

        } catch (Exception exception) {
            System.out.println(ANSI_GREEN + "UserDeatilServiceImpl" + ANSI_RED + exception);
        }


    }
}
