package com.mebi.bootapp.Security;

import com.mebi.bootapp.Config.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.security.KeyStore;

@Configuration
public class CustomWebMvcAutoConfig implements WebMvcConfigurer {
    String myExternalFilePath = "file:"+ Properties.path; // end your path with a /

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/output/**").addResourceLocations(myExternalFilePath);
    }

}
