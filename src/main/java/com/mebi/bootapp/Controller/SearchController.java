package com.mebi.bootapp.Controller;


import com.mebi.bootapp.Model.Company;
import com.mebi.bootapp.Model.Employee;
import com.mebi.bootapp.Model.Report;
import com.mebi.bootapp.Service.ReportService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class SearchController {

    @Autowired
    ReportService reportService;


    @GetMapping(value = "/search/issues", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getReports(@RequestParam(name = "page", required = false, defaultValue = "0") int currentPage
            , @RequestParam(value = "s") String searchTerm, HttpServletResponse response) {

        try {
            Page<Report> page = reportService.searchAllReportByIssueNo(searchTerm,currentPage);
            ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
            ObjectNode content = mapper.createObjectNode();
            ArrayNode jsonRep = mapper.createArrayNode();
            for(Report report : page) {
                ObjectNode json = mapper.createObjectNode();
                json.put("value", report.getId());
                json.put("label", report.getIssue());
                jsonRep.add(json);
            }

            content.put("content", jsonRep);
            ObjectNode jsonPage = mapper.createObjectNode();
            jsonPage.put("total", page.getTotalPages());
            jsonPage.put("number", page.getNumber() + 1);
            jsonPage.put("items", 1);
            content.put("page", jsonPage);

            String jsonreport = mapper.writeValueAsString(content);
            return jsonreport;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }



    @GetMapping(value = "/search/employess", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String serachEmployess(@RequestParam(name = "page", required = false, defaultValue = "1") int currentPage
          , HttpServletResponse response) {

        try {
            Page<Employee> page = reportService.searchAllEmployessByNameOrMail(currentPage);
            ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
            ObjectNode content = mapper.createObjectNode();
            ArrayNode jsonRep = mapper.createArrayNode();
            for(Employee employee : page) {
                ObjectNode json = mapper.createObjectNode();
                json.put("value", employee.getId());
                json.put("label", employee.getName());
                jsonRep.add(json);
            }

            content.put("content", jsonRep);
            ObjectNode jsonPage = mapper.createObjectNode();
            jsonPage.put("total", page.getTotalPages());
            jsonPage.put("number", page.getNumber() + 1);
            jsonPage.put("items", 1);
            content.put("page", jsonPage);

            String jsonreport = mapper.writeValueAsString(content);
            return jsonreport;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "/search/companies", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String serachCompanies(@RequestParam(name = "page", required = false, defaultValue = "1") int currentPage
            , HttpServletResponse response) {

        try {
            Page<Company> page = reportService.searchAllCompanies(currentPage);
            ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
            ObjectNode content = mapper.createObjectNode();
            ArrayNode jsonRep = mapper.createArrayNode();
            for(Company company : page) {
                ObjectNode json = mapper.createObjectNode();
                json.put("value", company.getId());
                json.put("label", company.getName());
                jsonRep.add(json);
            }

            content.put("content", jsonRep);
            ObjectNode jsonPage = mapper.createObjectNode();
            jsonPage.put("total", page.getTotalPages());
            jsonPage.put("number", page.getNumber() + 1);
            jsonPage.put("items", 1);
            content.put("page", jsonPage);

            String jsonreport = mapper.writeValueAsString(content);
            return jsonreport;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }







}
