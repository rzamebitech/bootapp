package com.mebi.bootapp.Controller.Admin;


import com.mebi.bootapp.Model.ERole;
import com.mebi.bootapp.Model.Menu.Icon;
import com.mebi.bootapp.Model.Menu.Menu;
import com.mebi.bootapp.Model.Menu.MenuItems;
import com.mebi.bootapp.Model.User;
import com.mebi.bootapp.Repository.IconRepository;
import com.mebi.bootapp.Service.MenuService;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminMenuController {

    @Autowired
    MenuService menuService;

    @Autowired
    IconRepository iconRepository;

    @GetMapping("/MenuManagement")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return "Menu Board.";
    }

    @GetMapping(value = "/menus/load", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getAllMenus(HttpServletResponse response, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                List<Menu> menus = menuService.findAll();

                if (menus.size() > 0) {
                    ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                            false);
                    /*   ObjectNode json = mapper.createObjectNode();*/
                    ArrayNode MenuArray = mapper.createArrayNode();
                    for (Menu menu : menus) {
                        ObjectNode node = mapper.createObjectNode();
                        node.put("id", menu.getId());
                        node.put("name", menu.getName());
                        node.put("cls", menu.getCls());
                        node.put("expanded", menu.getExpanded());
                        node.put("permission", menu.getVue_permission());
                        Set<MenuItems> items = menu.getMenuItems();

                        if (items != null) {

                            ArrayNode contractArray = mapper.createArrayNode();
                            for (MenuItems contract : items) {
                                ObjectNode jsonC = mapper.createObjectNode();
                                jsonC.put("id", contract.getId());
                                jsonC.put("name", contract.getName());
                                jsonC.put("link", contract.getLink());
                                jsonC.put("iconid", contract.getIcon().getId());
                                jsonC.put("icon", contract.getIcon().getName());
                                jsonC.put("role", contract.getRole().getName().name());
                                jsonC.put("roleOrdinal", contract.getRole().getName().ordinal());
                                contractArray.add(jsonC);

                            }

                            node.put("items", contractArray);
                        }


                        MenuArray.add(node);
                    }
                    return mapper.writeValueAsString(MenuArray);

                }


                return null;
            }


        } catch (Exception e) {
            /* logger.error("Error load USERS in adminpanel: " + " with error" + e.getMessage());*/
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "/icons/load", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getAllIcons(HttpServletResponse response, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                List<Icon> menus = iconRepository.findAll();

                if (menus.size() > 0) {
                    ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                            false);

                    ArrayNode MenuArray = mapper.createArrayNode();
                    for (Icon menu : menus) {
                        ObjectNode node = mapper.createObjectNode();
                        node.put("value", menu.getId());
                        node.put("label", menu.getName());

                        MenuArray.add(node);
                    }
                    return mapper.writeValueAsString(MenuArray);
                }
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "/menus/save", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String saveMenuItem(HttpServletResponse response, Authentication authentication) {

        return null;
    }

    @GetMapping(value = "/menus/change", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String changeMenuItemIndex(HttpServletResponse response, Authentication authentication) {


        return null;
    }
}

