package com.mebi.bootapp.Controller.Admin;


import com.mebi.bootapp.Form.XmlResponse;
import com.mebi.bootapp.Utilities.GetAppPropertiesData;
import com.mebi.bootapp.Utilities.SaxHandler;
import com.mebi.bootapp.Utilities.minIO.MinIOConf;
import com.mebi.bootapp.Utilities.minIO.MinIOUtl;
import com.mebi.bootapp.Utilities.minIO.ZipUtl;
import io.minio.MinioClient;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.mebi.bootapp.Controller.Admin.adminMQsender.multipartToFile;

@CrossOrigin(origins = { "*" }, maxAge = 3600L)
@RestController
@RequestMapping({ "/api/admin" })
public class AdminEnvelopeController {

    private static  String EnvelopeUUID = "" ;
    private static  String DBPASS ="" ;
    private static  String DBYUSER ="" ;
    private static String DBURL = "";







    @PostMapping(value = "/download-envelopes",  produces="application/zip")
    @PreAuthorize("hasRole('MODERATOR')")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> DownloadEnvelopeZip(HttpServletResponse response, @RequestPart("file") MultipartFile file,@RequestPart("edit") String edit, Authentication authentication) {
         String NOT_FOUND = "";
        try{

            if (file != null && file.toString().length() > 0) {


                boolean willEdit = edit.equals("true");

                System.out.println(file.getOriginalFilename() + " is loaded");
                File myFile = multipartToFile(file, "EnvelopeForDownload" + Math.random() * 49 + 1 + ".txt");
                Scanner sc = new Scanner(Paths.get(myFile.getPath()));
                //ArrayList<String> list = new ArrayList();
                HashMap<String, String> map = new HashMap<String, String>();
                ArrayList<String> unzipedLst = new ArrayList<>();
                List<String> textlines = new ArrayList<>(Collections.emptyList());
                Path textFile = null;

                System.out.println("EnvelopeForDownload class created");
                while (sc.hasNextLine()) {
                    String envelopeUUID = sc.nextLine();
                  prepareListOfEnvelopes(envelopeUUID,map);//TODO hepsi tek istekte giderse daha iyi
                }
                if(willEdit){
                    System.out.println("UUID will change!");
                     textFile = Paths.get(com.mebi.bootapp.Config.Properties.path+"/"+"envelopes.txt");
                    //boolean result = Files.deleteIfExists(textFile);

                }

                if(!map.isEmpty()){
                    System.out.println(map.size() + " envelope(s) found");
                    for (Map.Entry<String, String> set :
                            map.entrySet()) {

                        String[] yearMountDay =  set.getValue().split("-");
                        char firstChar = set.getKey().charAt(0);




                        boolean found = false;
                        int nextDay = Integer.parseInt(yearMountDay[2])+1 ;
                        String path = ""+yearMountDay[0]+"/"+yearMountDay[1]+"/"+yearMountDay[2]+"/"+Character.toUpperCase(firstChar)+"/";
                        String pathNextDay = ""+yearMountDay[0]+"/"+yearMountDay[1]+"/"+nextDay+"/"+Character.toUpperCase(firstChar)+"/";

                      String filePath = "/mnt/minio/envelope_tmp/"+path+set.getKey()+".zip";
                       //local :
                         // String filePath = "Z:/envelope_tmp/"+path+set.getKey()+".zip";

                        Path pat = Paths.get(filePath);

                        if(Files.exists(pat)){
                            found = true;

                        }else{
                            filePath = "/mnt/minio/envelope_tmp/"+pathNextDay+set.getKey()+".zip";
                            pat = Paths.get(filePath);
                            if(Files.exists(pat)){
                                found = true;
                            }
                        }

                        if (found) {
                            long bytes = Files.size(pat);
                            System.out.println("Start extracting zip file ");
                            Path unzippedPath = ZipUtl.unzipFolder(pat, Paths.get(com.mebi.bootapp.Config.Properties.path+"/unzip/"));
                            System.out.println("Zip extracted on path : " + unzippedPath.toString());
                            if(!unzippedPath.getFileName().toString().equals("")){
                                    if(willEdit){
                                        String editedPath = changeEnvelopeUUIDforFiles(new File(unzippedPath.toString()));
                                        if(editedPath!=null){

                                            textlines.add(unzippedPath.getFileName()+" => "+ Paths.get(editedPath).getFileName());
                                            unzipedLst.add(editedPath);
                                        }else{
                                            System.out.println("Extracted file issue on path : " + unzippedPath.toString());
                                            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                                            NOT_FOUND+= set.getKey() + " , ";
                                        }

                                    }else{
                                        unzipedLst.add(unzippedPath.toString());
                                    }


                            }else{
                               System.out.println("Extracted file not found on path : " + unzippedPath.toString());
                                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                                NOT_FOUND+= set.getKey() + " , ";
                            }

                        }else{

                            System.out.println("can not find file on path : " + filePath );
                            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                            NOT_FOUND+= set.getKey() + " , ";
                        }
                    }

                   if(textFile!=null){
                       Files.write(textFile, textlines, StandardCharsets.UTF_8);
                       unzipedLst.add(textFile.toString());
                   }


                }else{
                    System.out.println("not found any envelope on path : ");
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                }

                String zipPath = com.mebi.bootapp.Config.Properties.path+"/temp.zip";
                try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipPath))) {

                    if(unzipedLst.size()>0){
                        for (String filePath : unzipedLst) {
                            File fileToZip = new File(filePath);
                            zipOut.putNextEntry(new ZipEntry(fileToZip.getName()));
                            Files.copy(fileToZip.toPath(), zipOut);
                            System.out.println(filePath + " Added to zip file");
                        }
                        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/zip"))
                                .header("Content-Disposition", "attachment; filename=\"Test.zip\"").header("Access-Control",NOT_FOUND)
                                .body(outputStream -> {

                                    try (OutputStream os = outputStream; InputStream inputStream = new FileInputStream(zipPath)) {
                                        IOUtils.copy(inputStream, os);
                                        zipOut.close();
                                        File zipdir = new File(com.mebi.bootapp.Config.Properties.path+"/unzip/");
                                        FileUtils.cleanDirectory(zipdir);

                                    }
                                });
                    }else{
                        response.setStatus(HttpStatus.NO_CONTENT.value());
                       return null;
                    }


                }


            }



        }catch (Exception e){
            if(authentication==null){
                response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            }else{
                e.printStackTrace();
                System.out.println(e.getMessage() + e.getCause());
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }

        }

        return null;
    }

    private void prepareListOfEnvelopes(String envelopeUUID, HashMap<String, String> map) throws SQLException {



        String[] parts = envelopeUUID.split(",");

        envelopeUUID = parts[0];
        String date = parts[1];


        LocalDate localDate = LocalDate.parse(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        String strDate = localDate.format(formatter);
        map.put(envelopeUUID,strDate);

    }

    public String changeEnvelopeUUIDforFiles(File file)  {

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            SaxHandler userhandler = new SaxHandler();
            saxParser.parse(file, userhandler);


            System.out.println(userhandler.getInstanceIdentifierValue());


            String path = com.mebi.bootapp.Config.Properties.path+"/unzip/"+userhandler.getInstanceIdentifierValue()+".xml";
            FileWriter writer = new FileWriter(path);
            writer.write(userhandler.getXmlContent());
            writer.close();

            return path;

        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;

    }




}
