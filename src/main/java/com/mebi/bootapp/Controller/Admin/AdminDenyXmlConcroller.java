
package com.mebi.bootapp.Controller.Admin;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.io.FileWriter;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Transformer;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import java.io.StringWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;
import java.io.InputStream;
import java.nio.file.Path;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.util.Date;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import java.util.Properties;
import org.springframework.http.HttpStatus;
import org.apache.commons.io.IOUtils;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import org.w3c.dom.Element;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FilenameUtils;
import com.mebi.bootapp.Utilities.GetAppPropertiesData;
import org.springframework.security.core.Authentication;
import com.mebi.bootapp.Form.DenyForm;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.mebi.bootapp.Config.WebSocket;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = { "*" }, maxAge = 3600L)
@RestController
@RequestMapping({ "/api/admin" })
public class AdminDenyXmlConcroller
{
    @Autowired
    private WebSocket webSocket;
    public static DocumentBuilderFactory dbf;
    static String filepath;

    @PostMapping(value = { "/user/denyQuery" }, produces = { "application/octet-stream" })
    @ResponseBody
    public byte[] CreateDenyQuery(final HttpServletResponse response, @RequestPart("file") final MultipartFile file, @RequestParam("shopId") final String shopId, final DenyForm denyForm, final Authentication authentication) {
        try {
            final Object user = authentication.getPrincipal();
            if (file != null && file.toString().length() > 0) {
                final GetAppPropertiesData prop = new GetAppPropertiesData();
                final Properties result = prop.GetProp();
                if (result != null) {
                    denyForm.setDBURL(result.getProperty("efatura.prod.url"));
                    denyForm.setDBYUSER(result.getProperty("efatura.prod.username"));
                    denyForm.setDBPASS(result.getProperty("efatura.prod.password"));
                }
                this.webSocket.sendOneMessage("DPS" + shopId, " <span style='color:deepskyblue'> File Upload Successfully </span>");
                final File myfile = this.convertMultiPartToFile(file);
                AdminDenyXmlConcroller.dbf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
                final DocumentBuilder db = AdminDenyXmlConcroller.dbf.newDocumentBuilder();
                final Document doc = db.parse(myfile);
                final String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:deepskyblue'> Start Creating Envelope's Query For Invoice: </span>" + fileNameWithOutExt);
                doc.getDocumentElement().normalize();
                final NodeList BusinessDocuments = doc.getElementsByTagName("sh:StandardBusinessDocument");
                if (BusinessDocuments.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:StandardBusinessDocument Tag </span> ");
                }
                final UUID uuid = UUID.randomUUID();
                denyForm.setOID(uuid.toString().replace("-", ""));
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:deepskyblue'> New Oid for envelope : </span>" + denyForm.getOID());
                final Node BusinessDocument = BusinessDocuments.item(0);
                final Element element = (Element)BusinessDocument;
                if(element==null){
                    return null; //TODO return error
                }
                final NodeList HeaderNodeList = element.getElementsByTagName("sh:StandardBusinessDocumentHeader");
                if (HeaderNodeList.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:StandardBusinessDocumentHeader Tag </span> ");
                }
                final Node HeaderNode = HeaderNodeList.item(0);
                final Element HeaderEl = (Element)HeaderNode;
                final NodeList DocumentIdentifications = HeaderEl.getElementsByTagName("sh:DocumentIdentification");
                if (DocumentIdentifications.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:DocumentIdentification Tag </span>");
                }
                final Node EvelopeNode = DocumentIdentifications.item(0);
                final Element EnvelopeEl = (Element)EvelopeNode;
                final NodeList InstanceIdentifiers = EnvelopeEl.getElementsByTagName("sh:InstanceIdentifier");
                if (InstanceIdentifiers.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:InstanceIdentifier Tag </span>");
                }
                else {
                    final Node InstanceIdentifier = InstanceIdentifiers.item(0);
                    denyForm.setENVELOPEUUID(InstanceIdentifier.getTextContent());
                }
                final NodeList Receivers = HeaderEl.getElementsByTagName("sh:Receiver");
                if (Receivers.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:Receiver Tag </span>");
                }
                final Node Receiver = Receivers.item(0);
                final Element ReceiverIdeEl = (Element)Receiver;
                final NodeList Identifiers = ReceiverIdeEl.getElementsByTagName("sh:Identifier");
                if (Identifiers.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:Identifier Tag </span>");
                }
                else {
                    final Node Identifier = Identifiers.item(0);
                    denyForm.setRECEIVERIDENTIFIER(Identifier.getTextContent());
                }
                final NodeList Senders = HeaderEl.getElementsByTagName("sh:Sender");
                if (Senders.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:Sender Tag </span>");
                }
                final Node Sender = Senders.item(0);
                final Element SendersIdeEl = (Element)Sender;
                final NodeList SenderIdentifiers = SendersIdeEl.getElementsByTagName("sh:Identifier");
                if (SenderIdentifiers.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:Identifier Tag </span>");
                }
                else {
                    final Node SenderIdentifier = SenderIdentifiers.item(0);
                    denyForm.setSENDERIDENTIFIER(SenderIdentifier.getTextContent());
                }
                final NodeList SenderContactInformations = SendersIdeEl.getElementsByTagName("sh:ContactInformation");
                if (SenderContactInformations.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:ContactInformation Tag  </span>");
                }
                else {
                    for (int temp = 0; temp < SenderContactInformations.getLength(); ++temp) {
                        final Node ContactInformation = SenderContactInformations.item(temp);
                        if (ContactInformation.getNodeType() == 1) {
                            final Element elm = (Element)ContactInformation;
                            String key = "";
                            String val = "";
                            final Node vkn = elm.getElementsByTagName("sh:ContactTypeIdentifier").item(0);
                            final Node title = elm.getElementsByTagName("sh:Contact").item(0);
                            if (vkn != null && vkn.getTextContent() != null) {
                                key = vkn.getTextContent();
                                if (key.equals("VKN_TCKN")) {
                                    val = title.getTextContent();
                                    denyForm.setSENDERVKNTCKN(val);
                                }
                                else if (title != null && title.getTextContent() != null) {
                                    val = title.getTextContent();
                                    denyForm.setSENDERTITLE(val);
                                }
                            }
                        }
                    }
                    final Node SenderIdentifier2 = SenderIdentifiers.item(0);
                    denyForm.setSENDERIDENTIFIER(SenderIdentifier2.getTextContent());
                }
                final Node DocumentIdentification = DocumentIdentifications.item(0);
                final Element DateTimeEl = (Element)DocumentIdentification;
                final NodeList CreationDateAndTimes = DateTimeEl.getElementsByTagName("sh:CreationDateAndTime");
                if (DocumentIdentifications.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:CreationDateAndTime Tag </span>");
                }
                else {
                    final Node CreationDateAndTime = CreationDateAndTimes.item(0);
                    denyForm.setCREATIONDATETIME(CreationDateAndTime.getTextContent().replace("T", " "));
                    final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    final Date date = dt.parse(denyForm.getCREATIONDATETIME());
                    denyForm.setCREATIONDATETIME("TIMESTAMP'" + dt.format(date) + "'");
                }
                final NodeList Packages = element.getElementsByTagName("ef:Package");
                if (Packages.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:Package Tag </span>");
                }
                final Node Package = Packages.item(0);
                final Element PackageEl = (Element)Package;
                final NodeList ApplicationResponses = PackageEl.getElementsByTagName("ApplicationResponse");
                final Node ElementList = PackageEl.getElementsByTagName("ElementList").item(0);
                if (ApplicationResponses.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:ApplicationResponse Tag </span>");
                }
                final Node ApplicationResponse = ApplicationResponses.item(0);
                final Element ApplicationResponseEl = (Element)ApplicationResponse;
                final NodeList ReceiverParties = ApplicationResponseEl.getElementsByTagName("cac:ReceiverParty");
                if (ReceiverParties.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:ReceiverParty Tag </span>");
                }
                final Node ReceiverPartie = ReceiverParties.item(0);
                final Element ReceiverEl = (Element)ReceiverPartie;
                final NodeList PartyNames = ReceiverEl.getElementsByTagName("cac:PartyName");
                if (PartyNames.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:PartyName Tag </span>");
                }
                final Node PartyName = PartyNames.item(0);
                final Element PartyNameEl = (Element)PartyName;
                final NodeList Names = PartyNameEl.getElementsByTagName("cbc:Name");
                if (Names.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'>Can not Find cbc:Name Tag </span>");
                }
                else {
                    final Node Name = Names.item(0);
                    denyForm.setRECEIVERTITLE(Name.getTextContent());
                }
                final NodeList PartyIdentifications = ReceiverEl.getElementsByTagName("cac:PartyIdentification");
                if (PartyIdentifications.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find cac:PartyName Tag </span>");
                }
                final Node PartyIdentification = PartyIdentifications.item(0);
                final Element PartyIdentificationEL = (Element)PartyIdentification;
                final NodeList ids = PartyIdentificationEL.getElementsByTagName("cbc:ID");
                if (ids.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find cbc:ID Tag</span> ");
                }
                else {
                    final Node id = ids.item(0);
                    denyForm.setRECEIVERVKNTCKN(id.getTextContent());
                }
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:deepskyblue'> GET CUSTOMER OID FROM DB </span>");
                TimeUnit.SECONDS.sleep(1L);
                final String INBOUND_INVOICES_QUERY = "SELECT c.OID  FROM EFATURAPROD.CUSTOMERS c WHERE c.TAXID  = '" + denyForm.getRECEIVERVKNTCKN() + "'";
                final Connection connectionTestDb = DriverManager.getConnection(denyForm.getDBURL(), denyForm.getDBYUSER(), denyForm.getDBPASS());
                final PreparedStatement preparedStatement = connectionTestDb.prepareStatement(INBOUND_INVOICES_QUERY);
                final ResultSet RecieverOid = preparedStatement.executeQuery();
                while (RecieverOid.next()) {
                    denyForm.setCUSTOMEROID(RecieverOid.getString("OID"));
                }
                denyForm.setHASDRAFT(0);
                denyForm.setELEMENTTYPE("APPLICATIONRESPONSE");
                denyForm.setENVELOPESIZE(5408);
                denyForm.setENVELOPETYPE("INBOUND_POSTBOX");
                denyForm.setSQL_TRIGGER(5);
                denyForm.setCMSGTYPE("RED");
                denyForm.setCTEST("TO_CLOB('RED')");
                denyForm.setLASTRECORD(1);
                denyForm.setRESPONSEDDOCUMENTTYPE("OUTBOUND_INVOICE");
                final NodeList UUIDS = ApplicationResponseEl.getElementsByTagName("cbc:UUID");
                if (UUIDS.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find cbc:UUIDS Tag  </span>");
                }
                else {
                    final Node UUID = UUIDS.item(0);
                    denyForm.setRESPONSEUUID(UUID.getTextContent());
                    denyForm.setCFUNC(UUID.getTextContent());
                }
                final String EnvelopeQuery = "INSERT INTO EFATURAPROD.ENVELOPES (\n          \"OID\",              \n          CREATIONDATETIME,    \n          CUSTOMEROID,\n          ENVELOPETYPE,\n          ENVELOPEUUID,         \n          RECEIVERIDENTIFIER,  \n          RECEIVERTITLE,        \n          RECEIVERVKNTCKN,    \n          SENDERIDENTIFIER,    \n          SENDERTITLE,          \n          SENDERVKNTCKN,       \n          HASDRAFT,\n          ELEMENTTYPE,\n          ENVELOPESIZE\n) VALUES ('" + denyForm.getOID() + "'," + denyForm.getCREATIONDATETIME() + ",'" + denyForm.getCUSTOMEROID() + "','" + denyForm.getENVELOPETYPE() + "','" + denyForm.getENVELOPEUUID() + "','" + denyForm.getRECEIVERIDENTIFIER() + "','" + denyForm.getRECEIVERTITLE() + "','" + denyForm.getRECEIVERVKNTCKN() + "','" + denyForm.getSENDERIDENTIFIER() + "','" + denyForm.getSENDERTITLE() + "','" + denyForm.getSENDERVKNTCKN() + "'," + denyForm.getHASDRAFT() + ",'" + denyForm.getELEMENTTYPE() + "'," + denyForm.getENVELOPESIZE() + ");";
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:green;'> Envelope Query:  </span>" + EnvelopeQuery);
                final NodeList DocumentResponses = ApplicationResponseEl.getElementsByTagName("cac:DocumentResponse");
                if (DocumentResponses.getLength() < 1) {
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find cac:DocumentResponse Tag </span>");
                }
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:deepskyblue'>Start Creating INBOUND APP RESPONSES Query </span>");
                TimeUnit.SECONDS.sleep(1L);
                final Node DocumentResponse = DocumentResponses.item(0);
                final Element DocumentResponseEl = (Element)DocumentResponse;
                final NodeList DocumentReferences = DocumentResponseEl.getElementsByTagName("cac:DocumentReference");
                if (DocumentReferences.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find sh:DocumentReference Tag </span>");
                }
                final Node DocumentReference = DocumentReferences.item(0);
                final Element DDocumentReferenceEl = (Element)DocumentReference;
                final NodeList IDS = DDocumentReferenceEl.getElementsByTagName("cbc:ID");
                if (IDS.getLength() < 1) {
                    TimeUnit.SECONDS.sleep(1L);
                    this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:red'> Can not Find cbc:ID Tag </span>");
                }
                else {
                    final Node ID = IDS.item(0);
                    denyForm.setCGROUPID(ID.getTextContent());
                    denyForm.setRESPONSEDDOCUMENTUUID(denyForm.getCGROUPID());
                }
                denyForm.setCRCVPRN(denyForm.getRECEIVERVKNTCKN());
                denyForm.setCSTATUS(5);
                denyForm.setRESPONSECODE("RED");
                denyForm.setRESPONSEDESCRIPTION("RED");
                denyForm.setRESPONSEISSUEDATETIME(denyForm.getCREATIONDATETIME());
                final String InvoiceQuery = "INSERT INTO EFATURAPROD.INBOUND_APP_RESPONSES (\n          \"OID\",              \n          CGROUPID,    \n          RESPONSEDDOCUMENTUUID,\n          CRCVPRN,\n          CSTATUS,         \n          CUSTOMEROID,  \n          ENVELOPEUUID,        \n          RESPONSECODE,    \n          RESPONSEDESCRIPTION,    \n          RESPONSEISSUEDATETIME,          \n          RESPONSEUUID,       \n          CFUNC,\n          SQL_TRIGGER,\n          CMSGTYPE,\n          CTEST,\n          LASTRECORD,\n          RESPONSEDDOCUMENTTYPE\n) VALUES ('" + denyForm.getOID() + "','" + denyForm.getCGROUPID() + "','" + denyForm.getRESPONSEDDOCUMENTUUID() + "','" + denyForm.getCRCVPRN() + "','" + denyForm.getCSTATUS() + "','" + denyForm.getCUSTOMEROID() + "','" + denyForm.getENVELOPEUUID() + "','" + denyForm.getRESPONSECODE() + "','" + denyForm.getRESPONSEDESCRIPTION() + "'," + denyForm.getRESPONSEISSUEDATETIME() + ",'" + denyForm.getRESPONSEUUID() + "','" + denyForm.getCFUNC() + "'," + denyForm.getSQL_TRIGGER() + ",'" + denyForm.getCMSGTYPE() + "'," + denyForm.getCTEST() + "," + denyForm.getLASTRECORD() + ",'" + denyForm.getRESPONSEDDOCUMENTTYPE() + "');";
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:green;'>INBOUND_APP_RESPONSES'S QUERY: </span> " + InvoiceQuery);
                TimeUnit.SECONDS.sleep(1L);
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:deepskyblue'>Please wait for download Application Response xml File </span>");
                TimeUnit.SECONDS.sleep(1L);
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:green'>Update OutBound Invoice Status</span> UPDATE EFATURAPROD.OUTBOUND_INVOICES oi SET RESPONSETYPE='RED',DESCRIPTION='system' WHERE oi.INVOICEUUID ='" + denyForm.getRESPONSEDDOCUMENTUUID() + "'");
                final File invdir = new File(com.mebi.bootapp.Config.Properties.path+"/output/");
                if (!invdir.exists()) {
                    invdir.mkdir();
                }
                else {
                    FileUtils.cleanDirectory(invdir);
                }
                final String AppResponse = nodeToString(ElementList);
                final String ResponsePath = this.stringToDom(AppResponse, denyForm.getENVELOPEUUID());
                final Path path = Paths.get(ResponsePath, new String[0]);
                final InputStream fileStream = Files.newInputStream(path, StandardOpenOption.DELETE_ON_CLOSE);
                this.webSocket.sendOneMessage("DPS" + shopId, "<span style='color:green'>Paste downloaded  ApplicationResponse xml file content to CLOB Field on this table :  </span>SELECT * FROM EFATURAPROD.INBOUND_APP_RESPONSES iar WHERE iar.ENVELOPEUUID = '" + denyForm.getENVELOPEUUID() + "'");
                return IOUtils.toByteArray(fileStream);
            }
        }
        catch (Exception e) {
            if (authentication == null) {
                response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            }
            else {
                e.printStackTrace();
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }
        }
        return null;
    }

    private File convertMultiPartToFile( MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private static String nodeToString( Node node) throws TransformerException {
        StringWriter buf = new StringWriter();
        Transformer xform = TransformerFactory.newInstance().newTransformer();
        xform.setOutputProperty("omit-xml-declaration", "yes");
        xform.setOutputProperty("indent", "yes");
        xform.transform(new DOMSource(node), new StreamResult(buf));
        String fileData = buf.toString().trim();
        int start = fileData.indexOf("<ApplicationResponse");
        int end = fileData.indexOf("</ElementList>");
        String nodeString = fileData.substring(start, end);
        return nodeString.trim();
    }

    public String stringToDom( String xmlSource,  String envelopeuuid) throws SAXException, ParserConfigurationException, IOException, TransformerException {
        String path = AdminDenyXmlConcroller.filepath + envelopeuuid + "[appResponse].xml";
        FileWriter fw = new FileWriter(path);
        fw.write(xmlSource);
        fw.close();
        return path;
    }

    static {
        AdminDenyXmlConcroller.dbf = DocumentBuilderFactory.newInstance();
        AdminDenyXmlConcroller.filepath = com.mebi.bootapp.Config.Properties.path+"/output/";
    }
}