package com.mebi.bootapp.Controller.Admin;

import com.mebi.bootapp.Config.Properties;
import com.mebi.bootapp.Form.LogForm;

import com.mebi.bootapp.Model.LogArchive;
import com.mebi.bootapp.Service.LogService;
import com.mebi.bootapp.Utilities.FileUtl;
import com.mebi.bootapp.Utilities.Time;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;


import static com.mebi.bootapp.Utilities.Log.textSimilarity;

@CrossOrigin(origins = "*", maxAge = 3600L)
@RestController
@RequestMapping("/api/admin")
public class AdminLogController {

    @Autowired
    LogService logService;



    @PostMapping(value = "/user/getYestedayLogSummary", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('MODERATOR')")
    public String getYesterdayLogSummary(HttpServletResponse response, @RequestParam(value = "date", required = false) String date) {
        try {

            if(date.equals("")){
                LocalDate yesterday = LocalDate.now().minusDays(1);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                date  = yesterday.format(formatter);

            }

            List<Map<String, Object>> res = logService.findTotalCountByLevelAndType(date);

            ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
            ArrayNode jsonRep = mapper.createArrayNode();

            for (Map<String, Object> row : res) {
                String type = (String) row.get("type");
                BigDecimal errorCount = (BigDecimal) row.get("error_count");
                long errorCountLong = errorCount.longValue();

                BigDecimal warnCount = (BigDecimal) row.get("warn_count");
                long warnCountLong = warnCount.longValue();

                ObjectNode json = mapper.createObjectNode();
                json.put("type", type);
                json.put("error",errorCountLong);
                json.put("warn", warnCountLong);
                jsonRep.add(json);
            }

            return mapper.writeValueAsString(jsonRep);


        }catch (Exception exception){
            exception.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return exception.getMessage();
        }

    }







    @PostMapping(value = "/user/getLogSummary", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('MODERATOR')")
    public String getLogSummary(HttpServletResponse response,@Valid @ModelAttribute LogForm form,Authentication authentication) throws IOException {

        ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
        ArrayNode jsonRep = mapper.createArrayNode();
        ObjectNode baseJson = mapper.createObjectNode();
        int totalCount = 0;
        String fileSize = "";

        form.setDateString("");
        boolean isToday = Time.isToday(form.getLogdate());
        if(!isToday){
            form.setDateString("." + Time.getDateString(form.getLogdate()));
        }else{
            form.setDateString("");
        }

        //FIND FILE ON PATH
        boolean foundOnPath = false;
        String filePath = Properties.mntPath + "Logs" + form.getPath() + form.getType() + form.getDateString() + ".log";
        Path path = Paths.get(filePath);
        if(Files.exists(path)){
            foundOnPath = true;
            fileSize = FileUtl.formatFileSize(Files.size(path));


        }

       /* if (!Time.isSameDate(form.getLogdate())) {*/
            List<LogArchive> logs = logService.getLogFromDB(form.getType(),form.getLevel(),Time.getDateString(form.getLogdate()));




            if (logs.size() == 0) {
                System.out.println("WARN: DB'de " + form.getLogdate() + " Tarhili " + form.getType() + " log dosyasi için " + form.getLevel() + " logu bulunmadi !");
            } else {
                System.out.println("INFO: DB'de " + form.getLogdate() + " Tarhili " + form.getType() + " log dosyasi için " + form.getLevel() + " logu bulundu !");
                for (LogArchive log: logs){
                    ObjectNode json = mapper.createObjectNode();
                    json.put("title", log.getTitle());
                    json.put("count", log.getCount());
                    json.put("level", log.getLevel());

                    jsonRep.add(json);
                    totalCount+=log.getCount();


                }
                baseJson.put("data", jsonRep);
                ArrayNode infoArray = mapper.createArrayNode();

                    ObjectNode node = mapper.createObjectNode();
                    node.put("total", totalCount);
                    node.put("isfound", foundOnPath);
                    node.put("fileSize", fileSize);
                infoArray.add(node);
                baseJson.put("info", infoArray);

                return mapper.writeValueAsString(baseJson);


            }



        String errorLogFilePath = Properties.path+"input/"+form.getType()+Math.random()+".txt"; // Error log dosyasının yazılacağı yol
        System.out.println("Logun okunacağı adres : " + filePath);

        try {

            if(foundOnPath){
                System.out.println("Dosya bulundu  ! " + filePath);
                try (BufferedReader reader = new BufferedReader(new FileReader(filePath));
                     BufferedWriter writer = new BufferedWriter(new FileWriter(errorLogFilePath))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (line.contains(form.getLevel())) {
                            int index = line.indexOf(form.getLevel());
                            if (index != -1) {
                                String errorText = line.substring(index);
                                writer.write(errorText);
                                writer.newLine();
                            }

                        }
                    }
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int lineCount = 0;
                    Map<String, Integer> dissimilarErrorsLst = new HashMap<>();
                    // List<String> dissimilarErrors = new ArrayList<>();
                    try (BufferedReader textReader = new BufferedReader(new FileReader(errorLogFilePath))) {
                        String textLine;

                        while ((textLine = textReader.readLine()) != null) {
                            lineCount++;

                            boolean isErrorExist = false;
                            if(dissimilarErrorsLst.isEmpty()){
                                // dissimilarErrors.add(textLine);
                                dissimilarErrorsLst.put(textLine,1);
                                continue;
                            }
                            for (String err : dissimilarErrorsLst.keySet()){
                                double similarityScore = textSimilarity(textLine, err);
                                if (similarityScore > 0.9) {
                                    isErrorExist = true;
                                    Integer value = dissimilarErrorsLst.get(err);
                                    dissimilarErrorsLst.put(err, value+1);
                                    break;
                                }
                            }
                            if(!isErrorExist){
                                // dissimilarErrors.add(textLine);
                                dissimilarErrorsLst.put(textLine,1);
                            }
                        }

                        int textCount = 0;

                        System.out.println("Toplam " + lineCount+ " Hatada benzer olmayan error/warn bulundu:");
                        for (String error : dissimilarErrorsLst.keySet()) {
                            textCount++;
                            ObjectNode json = mapper.createObjectNode();
                            json.put("title", error);
                            json.put("count", dissimilarErrorsLst.get(error));
                            json.put("level", form.getLevel());

                            jsonRep.add(json);
                            totalCount+=dissimilarErrorsLst.get(error);


                            if(!isToday){
                                try {
                                    LogArchive log = new LogArchive();
                                    log.setTitle(error);
                                    log.setCount(dissimilarErrorsLst.get(error));
                                    log.setLogdate(Time.getDateString(form.getLogdate()));
                                    log.setType(form.getType());
                                    log.setLevel(form.getLevel());
                                    logService.save(log);
                                }catch (Exception exception){
                                    System.out.println(exception.getMessage() + " On line: " + textCount);
                                }

                            }



                        }

                        baseJson.put("data", jsonRep);
                        ArrayNode infoArray = mapper.createArrayNode();

                        ObjectNode node = mapper.createObjectNode();
                        node.put("total", totalCount);
                        node.put("isfound", foundOnPath);
                        node.put("fileSize", fileSize);
                        infoArray.add(node);
                        baseJson.put("info", infoArray);


                        /*ObjectNode countJson = mapper.createObjectNode();
                        countJson.put("total", lineCount);
                        jsonRep.add(countJson);*/

                    } catch (IOException e) {
                        System.out.println("Error txt on line: " +lineCount);
                        e.printStackTrace() ;
                    }
                }
            }else{
                System.out.println( filePath + " Adresinde log bulunamadi");
                response.setStatus(HttpStatus.NOT_FOUND.value());
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        File file = new File(errorLogFilePath);
        if(file.exists()){
            boolean isDeleted = file.delete();
            if (isDeleted) {
                System.out.println("Dosya başarıyla silindi.");
            } else {
                System.out.println("Dosya silinirken bir hata oluştu.");
            }
        }

        String jsonreport = mapper.writeValueAsString(baseJson);
        return jsonreport;




    }




}
