package com.mebi.bootapp.Controller.Admin;


import com.mebi.bootapp.Form.EnvelopeForm;
import com.mebi.bootapp.Form.InvoiceForm;
import com.mebi.bootapp.Service.LogService;
import com.mebi.bootapp.Service.Xml2HtmlConnverterService;
import com.mebi.bootapp.Utilities.GetAppPropertiesData;
import com.mebi.bootapp.Utilities.XmlFileModify;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;


import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.mebi.bootapp.Controller.Admin.adminMQsender.multipartToFile;


/**
 * @author Riza on 8/8/2022
 * @project IntelliJ IDEA
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminInvoiceController {

    @Autowired
    LogService logService;

    private static String DBURL = null;
    private static String DBYUSER = null;
    private static String DBPASS =null ;

    private static String ENVELOPEUUID =null ;
    private static String ENVELOPESTATUS =null ;
    private static String ENVELOPESTATUSDESC =null ;
    private static String ENVELOPECREATIONDATETIME =null ;
    private static String RECEIVERVKNTCKN =null ;
    private static String ENVELOPETYPE =null ;
    private static String RECEIVERTITLE =null ;
    private static String SENDERVKNTCKN =null ;
    private static String ENVELOPEUPDATEDATE =null ;
    private static String SENDERTITLE =null ;

    private static String ENVELOPESTATUSDETAIL =null ;
    private static String ENVELOPELASTMODIFIED =null ;
    private static String ENVELOPESTATUSCODE =null ;
    private static String DOCUMENTSTATUS =null ;


    private static String ENVELOPESTATUS404 ="ENVELOPE_STATUS_DETAILS TABLOSUNDA VERİ BULUNAMADI ! " ;







    private static String INVOICESERIALNO =null ;
    private static String INVOICEUUID =null ;

    private static String TYPE =null ;



    @PostMapping(value = "/user/check-invoice", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String getInvoiceStatus(HttpServletResponse response, @RequestParam("data") String serial, Authentication authentication) {
        try {

            Object user = authentication.getPrincipal();
            if (user != null) {
                GetAppPropertiesData prop = new GetAppPropertiesData();
                Properties result = prop.GetProp();
                if(result!=null){
                    DBURL = result.getProperty("efatura.prod.url");
                    DBYUSER = result.getProperty("efatura.prod.username");
                    DBPASS = result.getProperty("efatura.prod.password");
                }
                INVOICESERIALNO = "'"+serial+"%'";
                String OUTBOUND_INVOICES_QUERY = getOutboundInvoiceQuery(INVOICESERIALNO);
                String INBOUND_INVOICES_QUERY = getInboundInvoiceQuery(INVOICESERIALNO);



                ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,false);
                ArrayNode parentArray = mapper.createArrayNode();
                ObjectNode node = mapper.createObjectNode();

                Connection connectionTestDb = DriverManager.getConnection(DBURL, DBYUSER, DBPASS);
                PreparedStatement preparedStatement;
                preparedStatement  = connectionTestDb.prepareStatement(OUTBOUND_INVOICES_QUERY);
                ResultSet OutboundResultSet = preparedStatement.executeQuery();




                while (OutboundResultSet.next()) {

                    InvoiceForm invoiceForm = new InvoiceForm();
                    ObjectNode invoiceJson = invoiceForm.CreateInvoice(OutboundResultSet, "Giden Fatura");

                    
                    ENVELOPEUUID = "'"+OutboundResultSet.getString("ENVELOPEUUID")+"'";
                    INVOICESERIALNO = "'"+OutboundResultSet.getString("INVOICESERIALNO")+"'";


                    String query = getEnvelopeQuery(ENVELOPEUUID,INVOICESERIALNO );
                    preparedStatement  = connectionTestDb.prepareStatement(query);
                    ResultSet EnvelopeResultSet = preparedStatement.executeQuery();

                    ArrayNode envelopeArray = mapper.createArrayNode();
                    while (EnvelopeResultSet.next()) {

                        EnvelopeForm envelopeForm = new EnvelopeForm();
                        ObjectNode envelopeJson = envelopeForm.createEnvelope(EnvelopeResultSet);
                        envelopeArray.add(envelopeJson);

                        String EnvelopeDetailQuery = GetEnvelopeDetailQuery(ENVELOPEUUID);
                        preparedStatement  = connectionTestDb.prepareStatement(EnvelopeDetailQuery);
                        ResultSet EnvelopeDetailResultSet = preparedStatement.executeQuery();

                        boolean hasEnvelopeDetail = false;
                        ArrayNode envelopeDetailArray = mapper.createArrayNode();
                            while (EnvelopeDetailResultSet.next()) {
                                ObjectNode envelopeDetailJson = envelopeForm.createEnvelopeDeatil(EnvelopeDetailResultSet);
                                envelopeDetailArray.add(envelopeDetailJson);
                                hasEnvelopeDetail = true;
                            }
                        envelopeJson.put("EnvelopeDetail",envelopeDetailArray);

                            if(!hasEnvelopeDetail){
                                System.out.println(ENVELOPESTATUS404);
                            }
                        invoiceJson.put("Envelopes",envelopeArray);
                    }
                    parentArray.add(invoiceJson);
                }
                if(parentArray.size()>0){
                    node.put("invoices",parentArray);
                    return mapper.writeValueAsString(node);
                }


                preparedStatement  = connectionTestDb.prepareStatement(INBOUND_INVOICES_QUERY);
                ResultSet InboundResultSet = preparedStatement.executeQuery();

                ArrayNode envelopeArray = mapper.createArrayNode();
                while (InboundResultSet.next()) {


                    InvoiceForm invoiceForm = new InvoiceForm();
                    ObjectNode invoiceJson = invoiceForm.CreateInvoice(InboundResultSet, "Gelen Fatura");


                    ENVELOPEUUID = "'"+InboundResultSet.getString("ENVELOPEUUID")+"'";
                    String query = getEnvelopeQuery(ENVELOPEUUID,INVOICESERIALNO);
                    preparedStatement  = connectionTestDb.prepareStatement(query);
                    ResultSet EnvelopeResultSet = preparedStatement.executeQuery();

                    while (EnvelopeResultSet.next()) {

                        EnvelopeForm envelopeForm = new EnvelopeForm();
                        ObjectNode envelopeJson = envelopeForm.createEnvelope(EnvelopeResultSet);
                        envelopeArray.add(envelopeJson);
                        //TODO APP RESPONSE
                        //TODO SYSTEM APP RESPONSE
                        invoiceJson.put("Envelopes",envelopeArray);

                    }
                    parentArray.add(invoiceJson);
                    node.put("invoices",parentArray);
                    return mapper.writeValueAsString(node);
                }
                preparedStatement.close();
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return "Sonuç Bulunamadı !";
            }

            return null;
        } catch (Exception e) {

            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getMessage().toString();
        }



    }

    @PostMapping(value = "/user/modify-xml-zip",  produces="application/zip")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> ModifyXmlToZip(HttpServletResponse response, @RequestPart("file") MultipartFile file,@Valid @ModelAttribute InvoiceForm form, Authentication authentication) {

        try{
            Object user = authentication.getPrincipal();
            if (file != null && file.toString().length() > 0) {
                String NOT_FOUND = "";
                System.out.println(file.getOriginalFilename() + " is loaded");
                File myFile = multipartToFile(file, "invoiceForModify" + Math.random() * 49 + 1 + ".txt");
                Scanner sc = new Scanner(Paths.get(myFile.getPath()));
                ArrayList<String>  list = new ArrayList();
                XmlFileModify modify = new XmlFileModify(true);
                System.out.println("XmlFileModify class created");
                  while (sc.hasNextLine()) {
                        String serialNo = sc.nextLine();
                        form.setSerialNo(serialNo);
                      String resault = modifiedXml(form,modify);
                      if(resault!=""){
                          list.add(resault);
                      }else{
                          NOT_FOUND+= serialNo + " , ";
                      }
                    }
                System.out.println(list.size() + " invoice found");
                String zipPath = com.mebi.bootapp.Config.Properties.path+"/temp.zip";
                try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipPath))) {
                    for (String filePath : list) {
                        File fileToZip = new File(filePath);
                        zipOut.putNextEntry(new ZipEntry(fileToZip.getName()));
                        Files.copy(fileToZip.toPath(), zipOut);
                        System.out.println(filePath + " Added to zip file");
                    }


                    return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/zip"))
                            .header("Content-Disposition", "attachment; filename=\"Test.zip\"").header("Access-Control",NOT_FOUND)
                            .body(outputStream -> {

                                try (OutputStream os = outputStream; InputStream inputStream = new FileInputStream(zipPath)) {
                                    IOUtils.copy(inputStream, os);
                                    zipOut.close();
                                    File invdir = new File(com.mebi.bootapp.Config.Properties.path+"/Fatura/");
                                    File desdir = new File(com.mebi.bootapp.Config.Properties.path+"/Irsaliye/");
                                    FileUtils.cleanDirectory(invdir);
                                    FileUtils.cleanDirectory(desdir);

                                }
                            });


                }


            }



        }catch (Exception e){
            if(authentication==null){
                response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            }else{
                e.printStackTrace();
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }

        }

        return null;
    }



    @PostMapping(value = "/user/modify-xml", produces="application/xml")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> ModifyXml(HttpServletResponse response, @Valid @ModelAttribute InvoiceForm form, Authentication authentication) {
        try{
            Object user = authentication.getPrincipal();
            if (user != null) {
                XmlFileModify modify = new XmlFileModify(true);
                String resault = modifiedXml(form,modify);
                if(resault!=""){
                    Path path = Paths.get(resault);

                        return ResponseEntity.ok()
                                .contentType(MediaType.APPLICATION_XML)
                                .header("Content-Disposition", "attachment; filename=\"output.xml\"")
                                .header("Access-Control", "")
                                .body((OutputStream outputStream) -> {
                                    try (OutputStream os = outputStream) {
                                        InputStream inputStream = new FileInputStream(path.toString());
                                        IOUtils.copy(inputStream, os);
                                    } catch (IOException e) {
                                        // Hata yönetimi
                                        // ...
                                    } finally {

                                        File dir = new File(path.toString());
                                        dir.delete();
                                    }
                                });

                }else{
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return null;
                }


            }
        }catch (Exception e){
            if(authentication==null){
                response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            }else{
                e.printStackTrace();
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }


        }

        return null;
    }

    @PostMapping(value = { "/user/getXsltFromXml" }, produces = { "application/octet-stream" })
    @ResponseBody
    public byte[] getXsltFromXml(final HttpServletResponse response, @RequestPart("file") final MultipartFile file, final Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {

                if (file != null && file.toString().length() > 0) {
                    byte[] xmlBytes = file.getBytes();
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    InputStream is = new ByteArrayInputStream(xmlBytes);
                    Document doc = builder.parse(new InputSource(is));
                    XPathFactory xpf = XPathFactory.newInstance();
                    XPath xp = xpf.newXPath();
                    xp.setNamespaceContext(new NamespaceContext() {
                        @Override
                        public String getNamespaceURI(String prefix) {
                            if (prefix.equals("cbc")) {
                                return "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
                            } else if (prefix.equals("cac")) {
                                return "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
                            } else {
                                return null;
                            }
                        }

                        @Override
                        public String getPrefix(String namespaceURI) {
                            return null;
                        }

                        @Override
                        public Iterator<String> getPrefixes(String namespaceURI) {
                            return null;
                        }
                    });

                    byte[] decodedBytes = null;
                    NodeList nodes = (NodeList) xp.evaluate("//AdditionalDocumentReference/Attachment/EmbeddedDocumentBinaryObject", doc, XPathConstants.NODESET);
                    if (nodes.getLength() > 0) {
                        Node node;
                        if(nodes.getLength()>1){//arsiv faturalarinda bazen normal xslt disinda pdf embedded de oluyor
                            node = nodes.item(1);
                        }else{
                            node = nodes.item(0);
                        }

                        String binaryData = node.getTextContent();
                        decodedBytes = Base64.getDecoder().decode(binaryData);
                        //System.out.println(binaryData);
                    } else {
                        response.setStatus(HttpStatus.BAD_REQUEST.value());

                        System.out.println("EmbeddedDocumentBinaryObject değeri bulunamadı.");
                        return "EmbeddedDocumentBinaryObject değeri bulunamadı.".getBytes(StandardCharsets.UTF_8);
                    }
                    /*Path path = myFile.toPath();
                    final InputStream fileStream = Files.newInputStream(path, StandardOpenOption.DELETE_ON_CLOSE);*/
                    return decodedBytes;
                }
            }
        }catch (Exception e){
            if(authentication==null){
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }else{
                e.printStackTrace();
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }


        }

        return null;
    }

    @PostMapping(value = { "/user/updateXslt" }, produces = { "application/octet-stream" })
    @ResponseBody
    public String updateXslt(final HttpServletResponse response, @RequestPart("file") final MultipartFile file, @RequestPart("xslt") final String xslt , final Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {

                if (file != null && file.toString().length() > 0 && xslt.length()>0) {

                    String encodedString = Base64.getEncoder().encodeToString(xslt.getBytes());
                    byte[] fileBytes = file.getBytes();
                    String fileString = new String(fileBytes, StandardCharsets.UTF_8);


                   String modifiedXml =  updateXsltXml(fileString,encodedString);

                    File directory = new File(com.mebi.bootapp.Config.Properties.path);
                    if (!directory.exists()) {
                        directory.mkdir();
                    }

                    String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());

                    Xml2HtmlConnverterService estateUtilService = new Xml2HtmlConnverterService();
                    String res = estateUtilService.xmlToHtml(null, directory.getPath() + "/" + fileNameWithOutExt + ".htm",modifiedXml);
                    response.setStatus(HttpStatus.OK.value());
                    return res;

                }
            }
        }catch (Exception e){
            if(authentication==null){
                response.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
            }else{
                e.printStackTrace();
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }


        }

        return null;
    }



    @PostMapping(value = "/user/exportXsltRelatedData",  produces="application/zip")
    @PreAuthorize("hasRole('MODERATOR')")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> exportXsltRelatedData(final HttpServletResponse response, @RequestPart("file") final MultipartFile file, @RequestPart("xslt") final String xslt , final Authentication authentication) {


        try {

            Object user = authentication.getPrincipal();
            if (user != null) {
                if (file != null && file.toString().length() > 0 && xslt.length()>0) {

                    String encodedString = Base64.getEncoder().encodeToString(xslt.getBytes());
                    byte[] fileBytes = file.getBytes();
                    String fileString = new String(fileBytes, StandardCharsets.UTF_8);



                    File directory = new File(com.mebi.bootapp.Config.Properties.path+"/xslt-editor/");
                    if (!directory.exists()) {
                        directory.mkdir();
                    }
                    final String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());

                        //01 Create txt file from encodedString
                        String encodedStringFile = directory.getPath() + "/" + fileNameWithOutExt + "-xslt-encoded.txt";
                       // BufferedWriter writer = new BufferedWriter(new FileWriter(encodedStringFile));
                             File encodedFile = new File(encodedStringFile);
                             FileWriter encodedWriter = new FileWriter(encodedFile);
                             encodedWriter.write(encodedString);
                             encodedWriter.close();
                        System.out.println("Dosya başarıyla yazıldı => "+ encodedStringFile);

                         //02 Create xslt file from xslt
                        String decodedStringFile = directory.getPath() + "/" + fileNameWithOutExt + "-xslt-decoded.xml";
                        File decodedFile = new File(decodedStringFile);
                        FileWriter decodedWriter = new FileWriter(decodedFile);
                        decodedWriter.write(xslt);
                        decodedWriter.flush();
                        decodedWriter.close();
                        System.out.println("XSLT dosyası oluşturuldu: " + decodedStringFile);

                         //03 Create pdf file from xml
                        String modifiedXml =  updateXsltXml(fileString, encodedString);
                        Xml2HtmlConnverterService estateUtilService = new Xml2HtmlConnverterService();
                        String PdfRes = estateUtilService.xmlToHtml(null, directory.getPath() + "/" + fileNameWithOutExt + ".htm",modifiedXml);
                        System.out.println("PDF dosyası oluşturuldu: " + PdfRes);


                         //04 save xml

                        String xmlStringFile = directory.getPath() + "/" + fileNameWithOutExt + ".xml";
                        File xmlFile = new File(xmlStringFile);
                        FileWriter xmlWriter = new FileWriter(xmlFile);
                        xmlWriter.write(modifiedXml);
                        xmlWriter.flush();
                        xmlWriter.close();
                        System.out.println("XML dosyası oluşturuldu: " + xmlStringFile);

                    //05 create zip file and response
                    String[] filenames = {encodedStringFile,decodedStringFile, directory.getPath() + "/"+PdfRes, xmlStringFile}; // zip'lenecek dosyaların isimleri

                    String zipPath = com.mebi.bootapp.Config.Properties.path+"/temp.zip";
                    try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipPath))) {
                        for (String filePath : filenames) {
                            File fileToZip = new File(filePath);
                            zipOut.putNextEntry(new ZipEntry(fileToZip.getName()));
                            Files.copy(fileToZip.toPath(), zipOut);
                            System.out.println(filePath + " Added to zip file");
                        }


                        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/zip"))
                                .header("Content-Disposition", "attachment; filename=\"Test.zip\"").header("Access-Control","")
                                .body(outputStream -> {

                                    try (OutputStream os = outputStream; InputStream inputStream = new FileInputStream(zipPath)) {
                                        IOUtils.copy(inputStream, os);
                                        zipOut.close();
                                        File dir = new File(directory.getPath());
                                        FileUtils.cleanDirectory(dir);


                                    }
                                });
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());

        }





        return null;
    }


    private String modifiedXml(InvoiceForm invoiceForm,XmlFileModify modify) throws SQLException {
        ResultSet resultSet;
        boolean isInvoice=false,isDespach=false;
        String resault = "";
        GetAppPropertiesData prop = new GetAppPropertiesData();
        Properties result = prop.GetProp();
        if(result!=null){
            DBURL = result.getProperty("efatura.prod.url");
            DBYUSER = result.getProperty("efatura.prod.username");
            DBPASS = result.getProperty("efatura.prod.password");
        }
        INVOICESERIALNO = "'"+invoiceForm.getSerialNo()+"'";
        String OUTBOUND_INVOICES_QUERY = getOutboundInvoiceQueryForMofify(INVOICESERIALNO);


        Connection connectionTestDb = DriverManager.getConnection(DBURL, DBYUSER, DBPASS);
        PreparedStatement preparedStatement;
        preparedStatement  = connectionTestDb.prepareStatement(OUTBOUND_INVOICES_QUERY);
        resultSet = preparedStatement.executeQuery();
        System.out.println("modifiedXml: Query Commited");


        while (resultSet.next()) {
            //TODO bu alan UI'dan gelmeli fatura mı irsaliye mi diye
            isInvoice = true;
            resault = modify.prepareInvoiceXml(resultSet,invoiceForm);
        }

        if(!isInvoice){
            String OUTBOUND_Despach_QUERY = getOutboundDespatchQuery(INVOICESERIALNO);
            preparedStatement  = connectionTestDb.prepareStatement(OUTBOUND_Despach_QUERY);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                isDespach = true;
                resault =   modify.prepareDespatchXml(resultSet);
            }
        }
        preparedStatement.close();

        return resault;
    }

    private String getInboundInvoiceQuery(String INVOICESERIALNO) {

        String InvoiceQuery =  "SELECT *  FROM EFATURAPROD.INBOUND_INVOICES oi WHERE oi.INVOICESERIALNO ="+INVOICESERIALNO;
        return InvoiceQuery;
    }

    private String GetEnvelopeDetailQuery(String ENVELOPEUUID) {
        String ENVELOPE_QUERY = "SELECT * FROM EFATURAPROD.ENVELOPE_STATUS_DETAILS es  WHERE  es.ENVELOPEUUID="+ENVELOPEUUID;
        return  ENVELOPE_QUERY;
    }

    private String getEnvelopeQuery(String ENVELOPEUUID, String INVOICESERIALNO) {
        String ENVELOPE_QUERY = " SELECT * FROM EFATURAPROD.ENVELOPES e  WHERE e.DOCUMENTID ="+ INVOICESERIALNO +" AND e.ENVELOPEUUID="+ENVELOPEUUID;
        return ENVELOPE_QUERY;
    }

    private String getOutboundInvoiceQuery(String INVOICESERIALNO){
        String InvoiceQuery ="SELECT *  FROM EFATURAPROD.OUTBOUND_INVOICES oi WHERE oi.INVOICESERIALNO LIKE "+INVOICESERIALNO;
        return InvoiceQuery;
    }

    private String getOutboundInvoiceQueryForMofify(String INVOICESERIALNO){
        String sql = " SELECT oi.INVOICESERIALNO ,oi.INVOICECONTENT,c.TAXID ,c.TITLE ,oi.INVOICEDATE, oi.ACTIONDATE,\n" +
                " e.STATUSDESC , e.SENDERVKNTCKN , e.RECEIVERVKNTCKN ,esd.STATUSCODE ,e.RECEIVERTITLE\n" +
                " FROM EFATURAPROD.OUTBOUND_INVOICES oi\n" +
                " LEFT OUTER JOIN EFATURAPROD.ENVELOPES e ON e.ENVELOPEUUID = oi.ENVELOPEUUID AND e.ENVELOPETYPE = 'OUTBOUND_SENDER'\n" +
                " LEFT OUTER JOIN EFATURAPROD.ENVELOPE_STATUS_DETAILS esd ON esd.ENVELOPEUUID = e.ENVELOPEUUID\n" +
                " LEFT OUTER JOIN EFATURAPROD.CUSTOMERS c ON c.\"OID\" =oi.CUSTOMEROID \n" +

                " where oi.INVOICESERIALNO = " +
                INVOICESERIALNO+
                "\n" +
                " ORDER BY c.TAXID , oi.INVOICESERIALNO";
        return sql;
    }

    private String getOutboundDespatchQuery(String DESPACHSERIALNO){
        String sql = " SELECT\n" + //Added By RZA for add party name
                "  od.DESPATCH_SERIAL_NO ,\n" +
                "  da.CONTENT,\n" +
                "  od.DESPATCH_DATE , \n" +
                "  od.DESPATCH_DATE_TIME,\n" +
                "  od.ACTIONDATE ,  esd.STATUSCODE ,  esd.STATUSDETAIL,  e.STATUSDESC , e.SENDERVKNTCKN ,e.RECEIVERTITLE, e.RECEIVERVKNTCKN \n" +
                " FROM EFATURAPROD.OUTBOUND_DESPATCHES od \n" +
                " LEFT OUTER JOIN EFATURAPROD.ENVELOPES e ON e.ENVELOPEUUID = od.ENVELOPEUUID AND e.ENVELOPETYPE = 'OUTBOUND_SENDER'\n" +
                " LEFT OUTER JOIN EFATURAPROD.ENVELOPE_STATUS_DETAILS esd ON esd.ENVELOPEUUID = e.ENVELOPEUUID\n" +
                " INNER JOIN EFATURAPROD.DESPATCHADDITIONAL da ON da.\"OID\" =od.\"OID\" \n" +
                " LEFT OUTER JOIN EFATURAPROD.CUSTOMERS c ON c.\"OID\" =od.CUSTOMEROID \n" +
                " WHERE od.DESPATCH_SERIAL_NO = " +
                DESPACHSERIALNO+
                "\n" +
                " ORDER BY od.DESPATCH_SERIAL_NO, c.TAXID ,od.ACTIONDATE DESC";
        return sql;
    }

    public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int x = 1; x <= columns; x++) {
            if (columnName.equals(rsmd.getColumnName(x))) {
                return true;
            }
        }
        return false;
    }

    private String updateXsltXml(String fileString, String encodedString) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource inputSource = new InputSource(new StringReader(fileString));
        Document doc = builder.parse(inputSource);

        NodeList binaryObjects = doc.getElementsByTagName("cbc:EmbeddedDocumentBinaryObject");

        for (int i = 0; i < binaryObjects.getLength(); i++) {
            Node binaryObject = binaryObjects.item(i);
            binaryObject.setTextContent(encodedString);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        transformer.transform(domSource, streamResult);
        String modifiedXml = writer.toString();
        return modifiedXml;
    }



}
