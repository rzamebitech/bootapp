package com.mebi.bootapp.Controller.Admin;


import com.mebi.bootapp.Config.WebSocket;

import com.mebi.bootapp.Utilities.SendMQ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class adminMQsender {

    @Autowired
    private WebSocket webSocket;


    @PostMapping(value = "/user/sendEmailsTest", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String sendMessageQueues(HttpServletResponse response, @RequestPart("file") MultipartFile file, Authentication authentication) {
        try {
            int limit=0;
            Object user = authentication.getPrincipal();
            if (user != null) {
                if (file != null && file.toString().length() > 0) {
                    File myFile = multipartToFile(file, "sendMessageQueues" + Math.random() * 49 + 1 + ".txt");


                    Scanner sc = new Scanner(Paths.get(myFile.getPath()));

                    //TODO FOR DIFFERENT ACTIVEMQ PROCESS WE CAN USE SWITCH
                    //TODO FOR SOFT SEND MAIL ////////////////////////////////////////////////////////////
                    while (sc.hasNextLine()) {
                        String txt = sc.nextLine();
                        SendMQ sendMQ = new SendMQ();
                        sendMQ.sendHpEmailQueue(txt);

                    }
                    /////////////////////////////////////////////////////////////////////////////////////
                    //TODO SEND BIS ENVELOPE AS XML
                 /* String base64 = DatatypeConverter.printBase64Binary(Files.readAllBytes(
                            Paths.get(myFile.getPath())));*/
                  /*  SendMQ sendMQ = new SendMQ();

                  sendMQ.sendHpEmailQueue(base64);*/
                  /////////////////////////////////////////////////////////////////////////////////////////



                    //TODO FOR MULTIPLE EMAIL SENT //////////////////////////////////////////////////////////
                  /*  int lineNumber = 0;
                   *//* StringBuilder strs = new StringBuilder();*//*
                    while (sc.hasNextLine()) {
                        lineNumber++;
                        limit++;

                        if(limit>=101){
                            System.out.println("Wait 1 Minutes");
                            webSocket.sendOneMessage("DPS007",lineNumber + " Satır Başarıyla Eklendi  ~ 30 Saniye bekleniyor ... ");
                            Thread.sleep(30000);
                            limit = 0;
                        }
                        SendMQ sendMQ = new SendMQ();
                        sendMQ.sendHpEmailQueue(sc.nextLine());
                        webSocket.sendOneMessage("DPS007"," "+lineNumber + " Satır Başarıyla Eklendi ");

                    }*/
                    //////////////////////////////////////////////////////////////////////////////////





                    boolean tt = myFile.delete();

                    return "##OK##" + "Toplamda "+ 1 + " Satır Başarıyla Eklendi !";
                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return "Text file not found!";
                }
            } else {
                return "You are not authorized to perform this action!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getMessage().toString();
        }


    }

    public static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "" + fileName);
        multipart.transferTo(convFile);
        return convFile;
    }

}
