package com.mebi.bootapp.Controller.Admin;

import com.mebi.bootapp.Model.Menu.Icon;
import com.mebi.bootapp.Model.Role;
import com.mebi.bootapp.Model.User;
import com.mebi.bootapp.Repository.RoleRepository;
import com.mebi.bootapp.Repository.UserRepository;
import com.mebi.bootapp.Security.UserDetailServiceImpl;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.KeyStore;
import java.util.*;

import static com.mebi.bootapp.Utilities.Log.ANSI_GREEN;
import static com.mebi.bootapp.Utilities.Log.ANSI_RESET;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminUserController {


    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;


    @GetMapping(value = "/users", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getAllUsers(HttpServletResponse response, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {

                List<User> users = userRepository.findAll();

                if (users.size() > 0) {
                    ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                            false);
                    /*   ObjectNode json = mapper.createObjectNode();*/
                    ArrayNode userArray = mapper.createArrayNode();

                    for (User usr : users) {
                        ObjectNode node = mapper.createObjectNode();
                        node.put("id", usr.getId());
                        node.put("email", usr.getUsername());
                        node.put("name", usr.getName());

                        List<Role> allRoles = roleRepository.findAll();

                        Set<Role> userRoles = usr.getRoles();
                        ArrayNode roleArray = mapper.createArrayNode();
                        for (Role role:userRoles) {

                            //roleNode.put("name", role.getName().name());


                            roleArray.add(role.getName().name());
                        }
                        ArrayNode roleAllArray = mapper.createArrayNode();
                        for (Role role:allRoles) {

                            //roleNode.put("name", role.getName().name());


                            roleAllArray.add(role.getName().name());
                        }


                        node.put("roles",roleArray);
                        node.put("allRoles",roleAllArray);

                        userArray.add(node);
                    }
                    return mapper.writeValueAsString(userArray);

                }

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }


        } catch (Exception e) {
            /* logger.error("Error load USERS in adminpanel: " + " with error" + e.getMessage());*/
            e.printStackTrace();
        }
        return null;
    }
    @GetMapping(value = "/checkToken", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String checkToken(HttpServletResponse response, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                response.setStatus(HttpStatus.OK.value());

            }else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
            }


        } catch (Exception e) {
            /* logger.error("Error load USERS in adminpanel: " + " with error" + e.getMessage());*/
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        return null;
    }
    @GetMapping(value = "/roles/load", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getAllIcons(HttpServletResponse response, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                List<Role> roles = roleRepository.findAll();

                if (roles.size() > 0) {
                    ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                            false);

                    ArrayNode MenuArray = mapper.createArrayNode();
                    for (Role role : roles) {
                        ObjectNode node = mapper.createObjectNode();
                        node.put("value", role.getName().ordinal());
                        node.put("label", role.getName().name());

                        MenuArray.add(node);
                    }
                    return mapper.writeValueAsString(MenuArray);
                }
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping(value = "/user/darkmode", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String saveScreenMode(HttpServletResponse response, @RequestParam(value = "darkmode", required = false) String darkmode, Authentication authentication) {
        try {

            Object name = authentication.getName();
            if (name != null) {
                User account = userRepository.findUserByUsername(name.toString());
                if (account != null) {
                    account.setDarkmode(darkmode);
                    userRepository.save(account);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }


        response.setStatus(HttpStatus.OK.value());
        return null;

    }

    @PostMapping(value = "/user/loadUserModal", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getUserModal(HttpServletResponse response, @RequestParam(value = "id", required = true) Long id, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {


                Optional<User> account = userRepository.findById(id);
                if (account.isPresent()) {
                    ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                            false);
                    /*   ObjectNode json = mapper.createObjectNode();*/

                        ObjectNode node = mapper.createObjectNode();
                        node.put("id", account.get().getId());
                        node.put("email",  account.get().getUsername());
                        node.put("name",  account.get().getName());

                        List<Role> allRoles = roleRepository.findAll();

                    ArrayNode roleAllArray = mapper.createArrayNode();
                    for (Role role:allRoles) {
                        roleAllArray.add(role.getName().name());
                    }
                    node.put("allRoles",roleAllArray);


                    Set<Role> userRoles =  account.get().getRoles();
                        ArrayNode roleArray = mapper.createArrayNode();
                        for (Role role:userRoles) {

                            //roleNode.put("name", role.getName().name());


                            roleArray.add(role.getName().name());
                        }

                        node.put("roles",roleArray);

                    return mapper.writeValueAsString(node);

                }



                return null;
            }


        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }


        response.setStatus(HttpStatus.OK.value());
        return null;

    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    @PostMapping(value = "/user/modifyUser", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String modifyUser(HttpServletResponse response,
                             @RequestParam(value = "id", required = true) long id,
                             @RequestParam(value = "name", required = true) String name,
                             @RequestParam(value = "email", required = true) String email,
                             @RequestParam(value="roles") String roles,
                             Authentication authentication) {

        try {
            User user = userRepository.findUserById(id);
            if(user!=null){

                user.setName(name);
                user.setEmail(email);
                user.setUsername(email);
                if(roles!=""){
                    List<String> list = Arrays.asList(roles.split(","));
                    user.setRoles(roleRepository.findRolesByName(list));
                    userRepository.save(user);
                }else{

                }



            }


        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }


        response.setStatus(HttpStatus.OK.value());
        return null;

    }


}
