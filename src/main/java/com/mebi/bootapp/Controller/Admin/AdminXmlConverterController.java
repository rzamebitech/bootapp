package com.mebi.bootapp.Controller.Admin;


import com.mebi.bootapp.Config.Properties;
import com.mebi.bootapp.Service.Xml2HtmlConnverterService;
import com.mebi.bootapp.Utilities.CustomCertificateLoader;
import com.mebi.bootapp.Utilities.CustomKeyPairLoader;

import com.mebi.bootapp.Utilities.Sign.SignerUtility;
import com.mebi.bootapp.Utilities.XmlUtil.XmlUtilities;
import org.apache.commons.io.FilenameUtils;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.jsoup.Jsoup;


import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import java.io.*;


import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;




@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminXmlConverterController {

    @PostMapping(value = "/user/convertXml", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String convertXml(HttpServletResponse response, @RequestPart("file") MultipartFile file, @RequestPart("type") String type, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                if (file != null && file.toString().length() > 0) {
                    File myFile = multipartToFile(file, Math.random() * 49 + 1 + ".xml");
                    Xml2HtmlConnverterService estateUtilService = new Xml2HtmlConnverterService();
                    File directory = new File(Properties.path);
                    if (!directory.exists()) {
                        directory.mkdir();
                    }
                    String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());

                    String res = estateUtilService.xmlToHtml(myFile, directory.getPath() + "/" + fileNameWithOutExt + ".htm",null);
                    return res;
                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return "XML not found!";
                }
            } else {
                return "You are not authorized to perform this action!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }


    }

    @PostMapping(value = "/user/xmlSchematron", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public HashMap<String, List<String>> SchematronCheck(HttpServletResponse response, @RequestPart("file") MultipartFile file,@RequestPart("type") String type, Authentication authentication) {

        List<String> resLst = new ArrayList<>();
        HashMap<String, List<String>> resArray = new HashMap<>();
        try {
            Object user = authentication.getPrincipal();


            if (user != null) {
                if (file != null && file.toString().length() > 0) {
                    File myFile = multipartToFile(file, Math.random() * 49 + 1 + ".xml");

                    File directory = new File(Properties.path);
                    if (!directory.exists()) {
                        directory.mkdir();
                    }
                    // String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());

                    XmlUtilities utilities = new XmlUtilities();
                    HashMap<String, List<String>> res = utilities.schematronCheck(myFile.getPath(),type);


                    return res;


                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    resLst.add("XML not found!");
                    resArray.put("XML Konrol Sonucu: ",resLst);
                    return resArray;
                }
            } else {
                resLst.add("You are not authorized to perform this action!");
                resArray.put("XML Konrol Sonucu: ",resLst);
                return resArray;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            resLst.add(e.getMessage());
            resArray.put("XML Konrol Sonucu: ",resLst);
            return resArray;
        }


    }

    @PostMapping(value = "/user/signXml", produces="application/xml")
    @ResponseBody
    public ResponseEntity<StreamingResponseBody>  signXml(HttpServletResponse response, @RequestPart("file") MultipartFile file, Authentication authentication) {



        try {
            Object user = authentication.getPrincipal();


            if (user != null) {
                if (file.toString().length() > 0) {
                    File myFile = multipartToFile(file, Math.random() * 49 + 1 + ".xml");
                    File outputFile = new File(myFile.getPath());
                    final byte[] xmlByte = new byte[(int)outputFile.length()];

                    Security.addProvider(new BouncyCastleProvider());
                    FileInputStream privateKeyFile = new FileInputStream(com.mebi.bootapp.Config.Properties.path+"privateKey.der");
                    FileInputStream certificateFile = new FileInputStream(com.mebi.bootapp.Config.Properties.path+"certificate.crt");
                    KeyPair keyPair = CustomKeyPairLoader.loadPrivateKey(privateKeyFile);
                    X509Certificate certificate = CustomCertificateLoader.loadCertificate(certificateFile);

                     byte[] signature = SignerUtility.signDataWithJavaSecurity(xmlByte, keyPair.getPrivate());
                    // String signature2 = SignerUtility.signDataWithX509Certificate(xmlByte, certificate, keyPair);

                    String pat = com.mebi.bootapp.Config.Properties.path+outputFile.getName();
                    FileOutputStream encryptedXmlFileOutputStream = new FileOutputStream(pat);
                    encryptedXmlFileOutputStream.write(signature);
                    encryptedXmlFileOutputStream.close();




                return ResponseEntity.ok()
                            .contentType(MediaType.APPLICATION_XML)
                            .header("Content-Disposition", "attachment; filename=\"output.xml\"")
                            .header("Access-Control", "")
                            .body((OutputStream outputStream) -> {
                                try (OutputStream os = outputStream) {
                                    InputStream inputStream = new FileInputStream(pat);
                                    IOUtils.copy(inputStream, os);
                                } catch (IOException e) {
                                    // Hata yönetimi
                                    // ...
                                } finally {

                                    File dir = new File(pat);
                                    dir.delete();
                                }
                            });



                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return null;
                }
            } else {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
        } catch (Exception e) {

            System.out.println(e.getMessage());

        }

return  null;
    }



    @PostMapping(value = "/user/checkSign", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String CheckSign(HttpServletResponse response, @RequestPart("file") MultipartFile file, @RequestPart("type") String type, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                if (file != null && file.toString().length() > 0) {
                    File myFile = multipartToFile(file, Math.random() * 49 + 1 + ".xml");
                    File outputFile = new File(myFile.getPath());
                    final byte[] xmlByte = new byte[(int)outputFile.length()];



                    // Sertifika dosyasını yükle
                    FileInputStream certificateFileInputStream = new FileInputStream(myFile.getPath());
                    CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
                    X509Certificate  certificate = (X509Certificate) certificateFactory.generateCertificate(certificateFileInputStream);
                    certificateFileInputStream.close();

                    // İmza verisini ve imza algoritmasını ayır
                    int signatureStartIndex = xmlByte.length - certificate.getEncoded().length - 1;
                    byte[] signature = new byte[xmlByte.length - signatureStartIndex];
                    byte[] data = new byte[signatureStartIndex];
                    System.arraycopy(xmlByte, 0, data, 0, signatureStartIndex);
                    System.arraycopy(xmlByte, signatureStartIndex, signature, 0, signature.length);

                    // İmza doğrulama
                    Signature verifier = Signature.getInstance("SHA256withRSA"); // Kullanılan imza algoritması
                    verifier.initVerify(certificate.getPublicKey());
                    verifier.update(data);
                    boolean verified = verifier.verify(signature);
                    System.out.println(verified);

                    return null;



                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return "XML not found!";
                }
            } else {
                return "You are not authorized to perform this action!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }


    }


    private Document obtenerDocumentDeByte(byte[] documentoXml) throws Exception {




        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new ByteArrayInputStream(documentoXml));
    }

    public static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
        multipart.transferTo(convFile);
        return convFile;
    }

    @PostMapping(value = "/user/getHtmlContent", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String getHtmlContent(HttpServletResponse response, @RequestParam(value = "url", required = false) String url, Authentication authentication) {
        try {
            Object user = authentication.getPrincipal();
            if (user != null) {
                //get HTML content here
                String webPage = "file:///" + url;
                File dir = new File(url);
                String html = String.valueOf(Jsoup.parse(dir, "UTF-8"));
                return html;
            } else {
                return "You are not authorized to perform this action!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getMessage();
        }


    }
}
