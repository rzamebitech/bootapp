package com.mebi.bootapp.Controller.Admin;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.mebi.bootapp.Form.EmployeeDetail;
import com.mebi.bootapp.Form.filter.ReportFilterForm;
import com.mebi.bootapp.Model.Company;
import com.mebi.bootapp.Model.Employee;
import com.mebi.bootapp.Model.Report;
import com.mebi.bootapp.Model.ReportEmployee;
import com.mebi.bootapp.Service.ReportService;

import com.mebi.bootapp.Utilities.Time;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.awt.print.Pageable;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminReportController {

    @Autowired
    ReportService reportService;


    @PostMapping(value = "/reports", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getReports(@RequestParam(name = "page", required = false, defaultValue = "1") int currentPage,
                             @ModelAttribute ReportFilterForm reportFilterForm,
                             HttpServletResponse response, Authentication authentication) {

        //TODO authentication check here

        //TODO use response


        try {
            Page<Report> reportPage = reportService.findAllByFilterForm(reportFilterForm);
            ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,
                    false);
            ArrayNode jsonRep = mapper.createArrayNode();
            for (Report report : reportPage.getContent()) {
                ObjectNode json = mapper.createObjectNode();
                ArrayNode employeeRep = mapper.createArrayNode();
                ArrayNode employeesIds = mapper.createArrayNode();

                json.put("id", report.getId());
                json.put("issue", report.getIssue());
                json.put("summary", report.getSummary());
                json.put("company", report.getCompany().getName());
                json.put("companyId", report.getCompany().getId());
                json.put("taxId",  report.getCompany().getTaxId());
                if(report.getCreateDate()!=null){
                    json.put("ticketDay",  Time.getDateString(report.getCreateDate()));

                }


                ArrayList<ReportEmployee> employees = reportService.findEmployessByReportId(report.getId());

                if(employees.size()>0 && employees!=null){

                    for (ReportEmployee reportEmployee :employees) {
                        ObjectNode employeeJson  = mapper.createObjectNode();
                        employeeJson.put("id", reportEmployee.getEmployeeId());
                        employeeJson.put("time_spent", reportEmployee.getTimeSpent()); //update on save

                        if(reportEmployee.getDay()!=0f){
                            employeeJson.put("time_type", "d");
                            employeeJson.put("time_value", reportEmployee.getDay());
                        }
                        else if(reportEmployee.getHour()!=0f){
                            employeeJson.put("time_type", "h");
                            employeeJson.put("time_value", reportEmployee.getHour());
                        }
                        else if(reportEmployee.getMinute()!=0){
                            employeeJson.put("time_type", "m");
                            employeeJson.put("time_value", reportEmployee.getMinute());
                        }

                        Optional<Employee> employee = reportService.findEmployeeById(reportEmployee.getEmployeeId());
                        employeeJson.put("name", employee.get().getName());

                        employeeRep.add(employeeJson);
                    }
                    json.put("employees", employeeRep);

                }



                jsonRep.add(json);
            }

            ObjectNode page = mapper.createObjectNode();
            page.put("total", reportPage.getTotalPages());
            page.put("size", reportPage.getTotalElements());
            page.put("number", reportPage.getNumber() + 1);
            jsonRep.add(page);
            String jsonreport = mapper.writeValueAsString(jsonRep);
            return jsonreport;
        }catch (Exception exception){
            exception.printStackTrace();
        }

        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return null;
    }

    @GetMapping(value = "/report", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getReport(HttpServletResponse response, Authentication authentication,@RequestPart("reportId") String reportId) {


        Report report = reportService.findIssueById(reportId);






        return null;
    }


    @GetMapping(value = "/employees", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getEmployees(HttpServletResponse response, Authentication authentication) {





        return null;
    }

    @GetMapping(value = "/employee", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getEmployee(HttpServletResponse response, Authentication authentication) {





        return null;
    }





    @GetMapping(value = "/companies", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getCompanies(HttpServletResponse response, Authentication authentication) {





        return null;
    }

    @GetMapping(value = "/company", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public String getCompany(HttpServletResponse response, Authentication authentication) {





        return null;
    }

    @Transactional
    @PostMapping(value = "/saveReport", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public void saveReport(HttpServletResponse response, Authentication authentication,@RequestPart(value = "reportId",required = false) String reportId,
                           @RequestPart("ticketId") String ticketId,
                           @RequestPart("companyId") String companyId,
                           @RequestPart("ticketDay") String ticketDay,
                           @RequestPart(value = "summury",required = false) String summury,
                           @RequestPart("employeeDetail") String employeeDetail) throws ParseException, IOException {

        response.setContentType("text/plain");

        Gson gson = new Gson();
        Type listType = new TypeToken<List<EmployeeDetail>>() {}.getType();
        List<EmployeeDetail> employeeDetails = gson.fromJson(employeeDetail, listType);

        if(reportId==null){ //CREATE
            //check for issue no duplicate
           Report rep =  reportService.findIssueById(ticketId);
           if(rep!=null){

               response.setStatus(HttpServletResponse.SC_BAD_REQUEST); // Hata kodu
               PrintWriter out = response.getWriter();
               out.println(ticketId+ " Numaralı ticket daha önce kaydolmuş !..."); // Metin yanıtı
               out.close();

               return;
           }

            Report report= new Report();
            report.setIssue(ticketId);
            report.setIssueDate(Time.ConvertVueDateToJavaDate(ticketDay));
            report.setSummary(summury);
            report.setCompany(reportService.findCompanyById(companyId));

            LocalDateTime now = LocalDateTime.now();
            ZoneId zoneId = ZoneId.systemDefault(); // Varsayılan sistem saat dilimini al
            Date date = Date.from(now.atZone(zoneId).toInstant());
            report.setCreateDate(date);
            reportService.save(report);


            for (EmployeeDetail detail : employeeDetails) {
                ReportEmployee re = new ReportEmployee();
                re.setReportId(report.getId());
                re.setEmployeeId(detail.getEmployeeId());
                if(detail.getTimeType().equals("m")){
                    re.setMinute(Integer.parseInt(detail.getTimeValue()));
                }else if (detail.getTimeType().equals("h")){
                    re.setHour(Float.parseFloat(detail.getTimeValue()));
                }else if (detail.getTimeType().equals("d")){
                    re.setDay(Float.parseFloat(detail.getTimeValue()));
                }
                re.setEmployeeId(detail.getEmployeeId());
                re.setTimeSpent(Time.CalculateTimeSpent(detail.getTimeValue(), detail.getTimeType()));
                reportService.saveReportEmployee(re);
            }
            response.setStatus(HttpServletResponse.SC_OK);
        }else{ //UPDATE

            //delete all time values
            Optional<Report> rep =  reportService.findReportByReportId(reportId);
            if(rep.isPresent()){
                ArrayList<ReportEmployee> reportEmployees =  reportService.findEmployessByReportId(Long.valueOf(reportId));

                for (ReportEmployee reportEmployee: reportEmployees) {
                    reportService.deleteReportEmployeeById(reportEmployee.getId());
                }

               /* reportService.deleteReportEmployeeByReportId(reportId);*/
                rep.get().setIssue(ticketId);
                rep.get().setIssueDate(Time.ConvertVueDateToJavaDate(ticketDay));
                rep.get().setSummary(summury);
                rep.get().setCompany(reportService.findCompanyById(companyId));
                reportService.save(rep.get());


                for (EmployeeDetail detail : employeeDetails) {
                    ReportEmployee re = new ReportEmployee();
                    re.setReportId(rep.get().getId());
                    re.setEmployeeId(detail.getEmployeeId());
                    if(detail.getTimeValue()==null){
                        detail.setTimeValue("1");
                    }
                    if(detail.getTimeType().equals("m")){

                        re.setMinute(Integer.parseInt(detail.getTimeValue()));
                    }else if (detail.getTimeType().equals("h")){
                        re.setHour(Float.parseFloat(detail.getTimeValue()));
                    }else if (detail.getTimeType().equals("d")){
                        re.setDay(Float.parseFloat(detail.getTimeValue()));
                    }
                    re.setEmployeeId(detail.getEmployeeId());
                    re.setTimeSpent(Time.CalculateTimeSpent(detail.getTimeValue(), detail.getTimeType()));
                    reportService.saveReportEmployee(re);
                }

                response.setStatus(HttpServletResponse.SC_OK);

            }




        }






















    }

    @PostMapping(value = "/saveEmployee", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public void saveEmployee(HttpServletResponse response, Authentication authentication) {

        //TODO SAVE AND EDIT Employee




    }

    @PostMapping(value = "/saveCompany", produces = "application/json; charset=UTF-8")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public void saveCompany(HttpServletResponse response, Authentication authentication) {

        //TODO SAVE AND EDIT Company




    }






}
