package com.mebi.bootapp.Controller.Admin;

import com.mebi.bootapp.Utilities.SendMQ;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;
import java.util.Scanner;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/admin")
public class AdminCsvController {

    @PostMapping(value = "/user/sendCsvAndChange", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public String sendMessageQueues(HttpServletResponse response, @RequestPart("file") MultipartFile file, Authentication authentication) {
        try {

            Object user = authentication.getPrincipal();
            if (user != null) {
                if (file != null && file.toString().length() > 0) {
                    //File myFile = multipartToFile(file, "csvTemp" + Math.random() * 49 + 1 + ".csv");
                    InputStream inputStream = file.getInputStream();
                    // Geçici dosya oluşturur
                    File tempFile = File.createTempFile("temp-csv-", ".csv");
                 FileWriter writer = new FileWriter(tempFile);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));


                    String line = "";
                    int start = 3122;
                    int yev = 3122;
                    int lineNumber = 10069;
                    int count = 0;
                    String tax = "6130799733";
                    while ((line = reader.readLine()) != null) {
                        count++;
                        String[] values = line.split("\\t");


                      /*  String yevmiye = values[7];
                        yev++;
                        lineNumber++;
                        count++;*/

                        StringBuilder sb = new StringBuilder();;
                        for(int i = 0; i < values.length; i++) {

                          /*  if(i==0) {
                                values[0] = ""+tax;
                            }
                            if(i==8) {
                                values[8] = ""+yev;
                            }
                            if(i==9) {
                                values[9] = ""+lineNumber;
                            }
                            if(i==10) {
                                values[10] = ""+yev;
                            }*/
                            if(values.length==1){
                            }else{
                                sb.append(values[i]);
                                if (i != values.length - 1) {
                                    sb.append("\t");
                                }
                            }




                        }

                       /* int length = 23-sb.toString().split("\\t").length; //kolokn sayısını 23'e tamamlamak için
                        for(int i = 0; i < length; i++) {
                            sb.append("\t");
                        }*/
                        if(values.length!=1){
                            sb.append(System.getProperty("line.separator")); // satır sonu break yapsın

                            writer.write(sb.toString());
                        }



                    }
                    writer.close();
                    reader.close();

                    response.setStatus(HttpStatus.OK.value());
                    return tempFile.getAbsolutePath();
                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return "Csv file not found!";
                }
            } else {
                return "You are not authorized to perform this action!";
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return e.getMessage().toString();
        }


    }

    public static File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "" + fileName);
        multipart.transferTo(convFile);
        return convFile;
    }
}
