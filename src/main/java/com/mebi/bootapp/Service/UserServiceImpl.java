package com.mebi.bootapp.Service;

import com.mebi.bootapp.Model.ERole;
import com.mebi.bootapp.Model.User;
import com.mebi.bootapp.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User saveUser(User user) {

        //Date not set

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }


    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User deleteUser(Long userId) {
        return null;
    }

    @Override
    public User changeRole(ERole role, String username) {
        User user = userRepository.findByUsername(username).orElseThrow(RuntimeException::new);

        return userRepository.save(user);
    }


}
