package com.mebi.bootapp.Service;

import com.mebi.bootapp.Model.Menu.Menu;
import java.util.List;

public interface MenuService
{
    List<Menu> findAll();
}