package com.mebi.bootapp.Service;

import com.mebi.bootapp.Form.LogForm;

import com.mebi.bootapp.Model.LogArchive;

import java.util.List;
import java.util.Map;

public interface LogService {




    void save(LogArchive log);

    List<LogArchive> getLogFromDB(String type, String level, String sameDate);

    List<Map<String, Object>>  findTotalCountByLevelAndType(String logdate);


}
