package com.mebi.bootapp.Service;

import com.mebi.bootapp.Model.Menu.Menu;
import com.mebi.bootapp.Repository.MenuReposiyory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuSeriveImpl implements MenuService {

    @Autowired
    MenuReposiyory menuReposiyory;


    @Override
    public List<Menu> findAll() {
        return menuReposiyory.findAll();
    }
}
