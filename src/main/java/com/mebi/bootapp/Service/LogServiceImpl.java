package com.mebi.bootapp.Service;

import com.mebi.bootapp.Model.LogArchive;
import com.mebi.bootapp.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService{

    @Autowired
    LogRepository logRepository;



    @Override
    public void save(LogArchive log) {
        logRepository.saveAndFlush(log);
    }

    @Override
    public List<LogArchive> getLogFromDB(String type, String level, String sameDate) {
        return logRepository.loadAllLogByTypeAndLevelAndDate(type,level,sameDate);
    }
    @Override
    public  List<Map<String, Object>> findTotalCountByLevelAndType(String logdate) {
        return logRepository.findErrorAndWarnCountByType(logdate);
    }



}
