package com.mebi.bootapp.Service;

import com.mebi.bootapp.Form.filter.ReportFilterForm;
import com.mebi.bootapp.Model.Company;
import com.mebi.bootapp.Model.Employee;
import com.mebi.bootapp.Model.Report;
import com.mebi.bootapp.Model.ReportEmployee;
import com.mebi.bootapp.Repository.CompanyRepository;
import com.mebi.bootapp.Repository.EmployeeRepository;
import com.mebi.bootapp.Repository.ReportEmployeeRepository;
import com.mebi.bootapp.Repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class ReportServiceImpl implements  ReportService{

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    EmployeeRepository employeeReposiyory;

    @Autowired
    CompanyRepository companyRepository;


     @Autowired
     ReportEmployeeRepository reportEmployeeRepository;




    @Override
    public void save(Report report) {
        reportRepository.save(report);

    }

    @Override
    public Page<Report> findAll(int page, int size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id"); // "price" sütunu üzerinde azalan sıralama
        Pageable pageable = PageRequest.of(page, size,sort);
        return reportRepository.findAll(pageable);
    }

    @Override
    public Page<Report> searchAllReportByIssueNo(String searchTerm, int currentPage) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(currentPage - 1, 20,sort);
        return  reportRepository.searchAllReportByIssueNo(searchTerm,pageable);


    }

    @Override
    public Page<Employee> searchAllEmployessByNameOrMail( int currentPage) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(currentPage - 1, 50,sort);
        return employeeReposiyory.findAll(pageable);
    }

    @Override
    public Page<Company> searchAllCompanies(int currentPage) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(currentPage - 1, 50,sort);
        return companyRepository.findAll(pageable);
    }

    @Override
    public ArrayList<ReportEmployee> findEmployessByReportId(Long id) {

       return reportEmployeeRepository.findByReportId(id);
    }

    @Override
    public Optional<Employee> findEmployeeById(long employeeId) {
        return employeeReposiyory.findById(employeeId);
    }

    @Override
    public Company findCompanyById(String companyId) {
        return companyRepository.getById(Long.valueOf(companyId));
    }

    @Override
    public void saveReportEmployee(ReportEmployee re) {
        reportEmployeeRepository.save(re);
    }

    @Override
    public Report findIssueById(String ticketId) {
        return reportRepository.findByIssue(ticketId);
    }

    @Override
    public Optional<Report> findReportByReportId(String reportId) {
        return reportRepository.findById(Long.valueOf(reportId));
    }

    @Override
    public void deleteReportEmployeeByReportId(String reportId) {

    reportEmployeeRepository.deleteByReportId(Long.valueOf(reportId));
    }

    @Override
    public Page<Report> findAllByFilterForm(ReportFilterForm form) {
        return reportRepository.findAll(form.predicate(),form.getPage());
    }

    @Override
    public void deleteReportEmployeeById(Long id) {
        reportEmployeeRepository.deleteById(id);
    }
}
