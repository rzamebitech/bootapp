package com.mebi.bootapp.Service;

import com.mebi.bootapp.Model.ERole;
import java.util.Optional;
import com.mebi.bootapp.Model.User;

public interface UserService
{
    User saveUser(final User user);

    Optional<User> findByUsername(final String username);

    User deleteUser(final Long userId);

    User changeRole(final ERole role, final String username);
}