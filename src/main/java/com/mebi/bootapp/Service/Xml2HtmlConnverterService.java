package com.mebi.bootapp.Service;


import com.mebi.bootapp.Config.Properties;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.xml.transform.StringSource;
import org.springframework.xml.xpath.Jaxp13XPathTemplate;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Xml2HtmlConnverterService {




    private Document parseDOM(InputStream iStream, String encoding) throws Exception {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory
                    .newDocumentBuilder();
            InputSource source = new InputSource(iStream);
            if (encoding != null)
                source.setEncoding(encoding);
            return documentBuilder.parse(source);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    public String xmlToHtml(File xmlFile, String outFileName, String xmlString) {
        File outputFile = null;
        try {
            String xml = "";

            if(xmlString!=null){
                xml = xmlString;
            }else{
                byte[] xmlByte;
                outputFile = xmlFile;
                FileInputStream fis = null;
                xmlByte = new byte[(int) outputFile.length()];
                fis = new FileInputStream(outputFile);
                fis.read(xmlByte);
                fis.close();
                xml = new String(xmlByte).replace("\uFFFD", "");
            }



            Document appResponse = null;
            System.setProperty("javax.xml.xpath.XPathFactory:" + XPathFactory.DEFAULT_OBJECT_MODEL_URI, "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl");
            try {
                appResponse = parseDOM(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_16)), "UTF-16");
            } catch (Exception e) {
                throw new RuntimeException("Convert Process Failed Error : " + e.getMessage());
            }
            Jaxp13XPathTemplate jaxp13xPathTemplate = new Jaxp13XPathTemplate();
            DOMSource domSource = new DOMSource(appResponse);
            String xsltFile = jaxp13xPathTemplate.evaluateAsString("//Attachment/EmbeddedDocumentBinaryObject[contains(@filename,'.xslt')]", domSource);
            if (xsltFile == null || !(xsltFile.trim().length() > 0)) {
                xsltFile = jaxp13xPathTemplate.evaluateAsString("//Attachment/EmbeddedDocumentBinaryObject[contains(@mimeCode,'application/xml')]", domSource);
            }

            StreamSource xsltSource;
            xsltSource = new StringSource(new String(Base64.decodeBase64(xsltFile.replace("77u/", ""))));
            TransformerFactory tf = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", Thread.currentThread().getContextClassLoader());
            StreamSource xmlSource = new StringSource(xml);
            StringWriter writer;
            /* "\uFFFD"*/
            try {
                Transformer transformer = tf.newTransformer(xsltSource);
                writer = new StringWriter();
                transformer.transform(xmlSource, new StreamResult(writer));
            } catch (Exception e) {
                Transformer transformer = tf.newTransformer(xsltSource);
                writer = new StringWriter();
                transformer.transform(xmlSource, new StreamResult(writer));
            }
            File pdfFile;
            try {
                /*  byte[] bytes = writer.toString().getBytes ( StandardCharsets.UTF_8 );*/
                Files.write(Paths.get(outFileName), writer.toString().getBytes(StandardCharsets.UTF_8));

                String htmlFileName = outFileName;
                String fileNameWithOutExt = FilenameUtils.removeExtension(outFileName).replace(" ", "-");
                String pdfFileName = fileNameWithOutExt + ".pdf";
                pdfFile = new File(pdfFileName);
                String scape = "Portrait";
                //String scape = "Landscape";

                //createQrCode(xml) ;


                executeCommand(Properties.pdfConverterPath,
                        "-O", scape,
                        "--footer-center", "1" + " - [page]/[topage]",
                        "--footer-font-size", "6",
                        htmlFileName, pdfFileName);

                int count = 0;
                while (!pdfFile.exists()) {
                    count++;
                    Thread.sleep(100);
                    if (count > 50) {
                        throw new RuntimeException("Some Error on convert to PDF ! ");
                    }
                }
                File htmlFile = new File(htmlFileName);
                if (htmlFile.exists()) {
                    htmlFile.delete();

                }
            } catch (IOException ex) {
                if(outputFile!=null){
                    outputFile.delete();
                }

                throw new RuntimeException("Convert Process Failed Error : " + ex.getMessage());
            }
            if(outputFile!=null){
                outputFile.delete();
            }
            return pdfFile.getName();
        } catch (Exception ex) {
            if(outputFile!=null){
                outputFile.delete();
            }
            /*System.out.println("Html donusum tamamlanamadı:"+ex.getMessage());*/
            throw new RuntimeException("Convert Process Failed Error : " + ex.getMessage());
        }
    }

    private void createQrCode(String content) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(content)));
        Element rootElement = document.getDocumentElement();
        NodeList nList = document.getElementsByTagName("cac:AccountingSupplierParty");
        Element eElement = (Element) nList.item(0);
        String g = getString("cbc:ID",eElement);
        System.out.println(g);

    }

    private String getString(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }

        return null;
    }

    private void executeCommand(String... args) {
        try {
            ProcessBuilder pBuilder = new ProcessBuilder(args);
            final Process process = pBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()),4096);


            String line = reader.readLine();
            while (line != null) {
                System.out.println(line); //Büyük faturalarda process takıldığı için production logunda kontrol amacıyla eklendi

                if (line.trim().equals("Done")) {
                    process.destroy();
                    System.out.println("destroyed process");
                    break;
                }
                line = reader.readLine();

            }
            process.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



/*    private void executeCommand(String... args) {
        try {
            ProcessBuilder pBuilder = new ProcessBuilder(args);
            final Process process = pBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8),116384);
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
            int exitCode = process.exitValue();
            if (exitCode == 0) {
                System.out.println("Conversion successful.");
            } else {
                System.out.println("Conversion failed with exit code: " + exitCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
