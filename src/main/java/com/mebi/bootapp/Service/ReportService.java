package com.mebi.bootapp.Service;

import com.mebi.bootapp.Form.filter.ReportFilterForm;
import com.mebi.bootapp.Model.Company;
import com.mebi.bootapp.Model.Employee;
import com.mebi.bootapp.Model.Report;
import com.mebi.bootapp.Model.ReportEmployee;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Optional;

public interface ReportService {


    void save(Report report);



    Page<Report> findAll(int page, int pagesize);

    Page<Report> searchAllReportByIssueNo(String searchTerm, int currentPage);

    Page<Employee> searchAllEmployessByNameOrMail( int page);

    Page<Company> searchAllCompanies(int currentPage);

    ArrayList<ReportEmployee> findEmployessByReportId(Long id);

    Optional<Employee> findEmployeeById(long employeeId);



    Company findCompanyById(String companyId);



    void saveReportEmployee(ReportEmployee re);

    Report findIssueById(String ticketId);

    Optional<Report> findReportByReportId(String reportId);

    void deleteReportEmployeeByReportId(String reportId);

    Page<Report> findAllByFilterForm(ReportFilterForm reportFilterForm);

    void deleteReportEmployeeById(Long id);
}
