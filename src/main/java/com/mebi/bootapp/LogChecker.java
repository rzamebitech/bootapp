/*
package com.mebi.bootapp;

import com.mebi.bootapp.Enums.LogTypes;
import com.mebi.bootapp.Utilities.Log;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.mebi.bootapp.Utilities.Log.*;

*/
/**
 * @author Riza on 8/3/2022
 * @project IntelliJ IDEA
 *//*

public class LogChecker {

    static String folderpath  = "C:/Helper-output/logs/";

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println(ANSI_RESET+"Start Log Check Process"  );
        File folder = new File(folderpath);
        File[] listOfFiles = folder.listFiles();




        List<String> ErrorLogs = new ArrayList<>();
        List<String> WarnLogs = new ArrayList<>();
        int currentLine = 0;
        int delayedLine = 0;
        boolean isGetErrorOrWarn = false;
        if(listOfFiles!=null){
            System.out.println(ANSI_RESET+"File(s) Found !"  );
            for (File listOfFile : listOfFiles) {
                if (listOfFile.isFile()) {

                    LogTypes logType = getLogType(listOfFile.getName());



                    Path path = Paths.get(folderpath+listOfFile.getName());
                    File textfile = new File(folderpath+listOfFile.getName());
                    System.out.println(ANSI_RESET+"Checking "+ textfile  );
                    BufferedReader br = new BufferedReader(new FileReader(textfile));
                    String err;
                    long total = Files.lines(path).count();
                    System.out.println("Total lines: " + total);
                    while ((err = br.readLine()) != null){
                        currentLine ++;
                        printProgress(0, total, currentLine);
                        if(logType== LogTypes.EARCHIVEPORTAL_JAVA){
                            if(isGetErrorOrWarn){
                                if( delayedLine == currentLine){
                                    if(!ErrorLogs.contains(err)){
                                        ErrorLogs.add(err);
                                        isGetErrorOrWarn =false;
                                        continue;
                                    }
                                    if(!WarnLogs.contains(err)){
                                        WarnLogs.add(err);
                                        isGetErrorOrWarn =false;
                                        continue;
                                    }
                                }else{
                                    */
/*continue;*//*

                                }
                            }
                        }

                        if(err.contains("ERROR")){
                            if(logType== LogTypes.EARCHIVEPORTAL_JAVA){
                                delayedLine = currentLine+2; // ERROR AND WARN DETAIL IS COMING 2 LINE LATER ERROR TAG
                                isGetErrorOrWarn =true;
                                continue;
                            }

                            if(!ErrorLogs.contains(err)){
                                ErrorLogs.add(err);
                            }
                        }
                        if(err.contains("WARN")){
                            if(logType== LogTypes.EARCHIVEPORTAL_JAVA){
                                delayedLine = currentLine+2; // ERROR AND WARN DETAIL IS COMING 2 LINE LATER ERROR TAG
                                isGetErrorOrWarn =true;
                                continue;
                            }


                            if(!WarnLogs.contains(err)){
                                WarnLogs.add(err);
                            }
                        }
                    }
                }
                }

            for(String err : ErrorLogs){
                System.out.println("\n" +ANSI_RED+err  );
            }
            for(String wrn : WarnLogs){
                System.out.println("\n" +ANSI_YELLOW+wrn  );
            }
            System.out.println("\n" +ANSI_RESET+" Log Check Process finished with " +ANSI_RED +  ErrorLogs.size() + ANSI_RESET+
                    " Error logs and " + ANSI_YELLOW + WarnLogs.size() + ANSI_RESET+ " Warn logs" );
            }
        }

    private static LogTypes getLogType(String name) {

        if(name.contains("earchiveportal_java")){
            System.out.print(ANSI_GREEN+ "Earşiv Portal Service"+ANSI_RESET);
            return LogTypes.EARCHIVEPORTAL_JAVA;
        }


        return null;
    }

    private static void printProgress(long startTime, long total, long current) {


        StringBuilder string = new StringBuilder(140);
        int percent = (int) (current * 100 / total);
        string
                .append('\r')
                .append(String.join("", Collections.nCopies(percent == 0 ? 2 : 2 - (int) (Math.log10(percent)), " ")))
                .append(String.format(" %d%% ", percent))
                .append(String.join("", Collections.nCopies(percent, "|")))
                .append('|')
                .append(String.join("", Collections.nCopies(100 - percent, " ")))
                .append(String.join("", Collections.nCopies(current == 0 ? (int) (Math.log10(total)) : (int) (Math.log10(total)) - (int) (Math.log10(current)), " ")))
                .append(String.format(" %d/%d, %s", current, total, ""));

        System.out.print(string);
    }

    }


*/
