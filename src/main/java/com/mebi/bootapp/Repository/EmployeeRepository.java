package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.Employee;


import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, Long> {


}
