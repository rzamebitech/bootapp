package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.ERole;
import com.mebi.bootapp.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(ERole name);

    Role findRoleByName(ERole name);

    @Query(nativeQuery = true, value = "select * from roles where name in  :list")
    Set<Role> findRolesByName(List<String> list);
}
