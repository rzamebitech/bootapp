package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {


    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User findByEmail(String email);

    Optional<User> findByUsername(String username);


    User findUserByUsername(String toString);


    User findUserById(long id);



}
