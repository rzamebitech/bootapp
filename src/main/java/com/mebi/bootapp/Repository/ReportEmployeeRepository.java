package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.ReportEmployee;
import com.mebi.bootapp.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ReportEmployeeRepository extends JpaRepository<ReportEmployee, Long> {


    ArrayList<ReportEmployee> findByReportId(Long id);


    @Query("DELETE FROM ReportEmployee r WHERE r.reportId = (?1)")
    void deleteByReportId(Long reportId);
}
