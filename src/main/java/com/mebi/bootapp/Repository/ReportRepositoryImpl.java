package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.QReport;
import com.mebi.bootapp.Model.QReportEmployee;
import com.mebi.bootapp.Model.Report;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;


public class ReportRepositoryImpl extends QuerydslRepositorySupport implements ReportRepositoryCustom {



    public ReportRepositoryImpl() {
        super(Report.class);
    }

    @Override
    public Page<Report> searchAllReportByIssueNo(String searchTerm, Pageable pageable) {

        QReport qReport = QReport.report;
        JPQLQuery<Report> query = from(qReport).where(qReport.issue.contains(searchTerm));
        query = getQuerydsl().applyPagination(pageable, query);

        List<Report> matchedNames = query.fetch(); // .list() yerine .fetch()

        Long count = from(qReport).where(qReport.issue.contains(searchTerm))
                .select(qReport.issue.countDistinct()).fetchFirst(); // .singleResult() yerine .fetchFirst()

        Page<Report> myObjectPage = new PageImpl<>(matchedNames, pageable, count);

        return myObjectPage;




    }

    @Override
    public Page<Report> findAllByFilterForm(Predicate predicate, Pageable page) {

        QReport qReport = QReport.report;
        QReportEmployee reportEmployee = QReportEmployee.reportEmployee;


       /* JPQLQuery query = from(qReport).where(contract.contract_no.contains(searchTerm));*/



        return null;
    }
}
