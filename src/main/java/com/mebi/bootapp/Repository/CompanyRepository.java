package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.Company;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
