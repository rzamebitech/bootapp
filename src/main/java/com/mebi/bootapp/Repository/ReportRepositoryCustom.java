package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.Report;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReportRepositoryCustom {
    Page<Report> searchAllReportByIssueNo(String searchTerm, Pageable pageable);

    Page<Report> findAllByFilterForm(Predicate predicate, Pageable page);
}
