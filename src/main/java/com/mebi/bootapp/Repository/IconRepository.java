package com.mebi.bootapp.Repository;

import com.mebi.bootapp.Model.Menu.Icon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IconRepository extends JpaRepository<Icon, Long> {


}
