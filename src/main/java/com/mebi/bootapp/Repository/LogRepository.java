package com.mebi.bootapp.Repository;



import com.mebi.bootapp.Model.LogArchive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository("logRepository")
public interface LogRepository  extends JpaRepository<LogArchive, Long> {


    @Query(value = "SELECT * FROM logarchive WHERE type = ?1 AND level = ?2 AND logdate = ?3", nativeQuery = true)
    List<LogArchive> loadAllLogByTypeAndLevelAndDate(String type, String level, String logdate);


    @Query(value ="SELECT type, " +
            "SUM(CASE WHEN level = 'error' THEN count ELSE 0 END) AS error_count, " +
            "SUM(CASE WHEN level = 'warn' THEN count ELSE 0 END) AS warn_count " +
            "FROM logarchive " +
            "WHERE logdate = :logDate " +
            "AND level IN ('error', 'warn') " +
            "GROUP BY type",nativeQuery = true)
    List<Map<String, Object>> findErrorAndWarnCountByType(@Param("logDate") String logDate);



}
