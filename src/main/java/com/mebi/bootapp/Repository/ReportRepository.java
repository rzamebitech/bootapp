package com.mebi.bootapp.Repository;


import com.mebi.bootapp.Model.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long>, QuerydslPredicateExecutor<Report>, ReportRepositoryCustom {


    Report findByIssue(String ticketId);
}
