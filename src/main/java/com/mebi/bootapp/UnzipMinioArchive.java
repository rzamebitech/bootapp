package com.mebi.bootapp;


import org.apache.commons.io.FilenameUtils;
import org.zeroturnaround.zip.NameMapper;
import org.zeroturnaround.zip.ZipUtil;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;

import java.util.zip.ZipFile;

import java.util.zip.ZipInputStream;

import static com.mebi.bootapp.Utilities.Log.ANSI_RESET;
import static com.mebi.bootapp.Utilities.Log.ANSI_YELLOW;

/**
 * @author Riza on 8/18/2022
 * @project IntelliJ IDEA
 */
public class UnzipMinioArchive {

    static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    static String filepath  = "C:/Helper-output/MinioArchive/";

    public static void main(String[] args) throws IOException {

        List<String> results = new ArrayList<String>();
        File[] files = new File(filepath).listFiles();
//If this pathname does not denote a directory, then listFiles() returns null.
        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
                if(isZipped(file)){
                    //unzip parent
                    System.out.println("First Zip : " + file.getName());
                    String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
                    String dir = filepath+fileNameWithOutExt;
                    File directory = new File(dir);
                    if (! directory.exists()){
                        directory.mkdir();
                    }
                    ZipUtil.unpack(new File(filepath+file.getName()), new File(filepath+"/"+fileNameWithOutExt));

                    try (ZipFile zipFile = new ZipFile(file.getPath())) {
                        Enumeration zipEntries = zipFile.entries();
                        int i=0;
                        while (zipEntries.hasMoreElements()) {

                            String fileName = ((ZipEntry) zipEntries.nextElement()).getName();
                            String extension = FilenameUtils.getExtension(fileName);
                            if(extension.equals("zip")){
                                i++;

                                String ChildPath = filepath+fileNameWithOutExt+"/"+fileName;

                                Path path = Paths.get(ChildPath);

                                //TODO check for exist dont overwrite
                                Path fileOrijinalName = Paths.get(fileName).getFileName();
                                String xml = fileOrijinalName.toString().split("_")[0]+".xml";
                                File f = new File(path.getParent().toString()+xml);
                                if(!f.exists()){
                                    ZipUtil.unpack(new File(ChildPath), new File(path.getParent().toString()));
                                    //TODO delete zip
                                    File myObj = new File(ChildPath);
                                    if (myObj.delete()) {

                                    } else {
                                        System.out.println("Failed to delete the file.");
                                    }
                                }else{
                                    System.out.println(ANSI_YELLOW+path.getParent().toString()+xml+" is already exist!"+ANSI_RESET);
                                }
                            }else{
                                File myObj = new File(filepath+fileNameWithOutExt+"/"+fileName);
                                if (myObj.delete()) {
                                   //
                                } else {
                                    System.out.println("Failed to delete the file.");
                                }
                            }


                        }
                    }
                }

            }
        }






    }

    public static boolean isZipped(File f) {

        try {
            RandomAccessFile raf = new RandomAccessFile(f, "r");
            long n = raf.readInt();
            raf.close();
            if (n == 0x504B0304) {
                return true;
            } else {
                return false;
            }
        } catch (Throwable e) {
            e.printStackTrace(System.err);
            return false;
        }

    }


}
