package com.mebi.bootapp;

import com.mebi.bootapp.Utilities.GetAppPropertiesData;
import org.springframework.beans.factory.annotation.Value;

import java.sql.*;
import java.util.Properties;

public class TestConnection {


    private static String DBURL = null;
    private static String DBYUSER = null;
    private static String DBPASS =null ;

    public static void main(String[] args) {
        try {

            GetAppPropertiesData prop = new GetAppPropertiesData();
            Properties result = prop.GetProp();
            if(result!=null){
                DBURL = result.getProperty("efatura.prod.url");
                DBYUSER = result.getProperty("efatura.prod.username");
                DBPASS = result.getProperty("efatura.prod.password");

            }

            String OUTBOUND_INVOICES_QUERY = "SELECT oi.INVOICESERIALNO ,oi.INVOICEUUID,oi.ENVELOPEUUID  FROM EFATURAPROD.OUTBOUND_INVOICES oi" +
                    " WHERE oi.INVOICESERIALNO = 'MTR2022000007123'";

            String INBOUND_INVOICES_QUERY = "SELECT oi.INVOICESERIALNO ,oi.INVOICEUUID,oi.ENVELOPEUUID  FROM EFATURAPROD.INBOUND_INVOICES oi WHERE oi.INVOICESERIALNO = 'MTR2022000007123'";
            Connection connectionTestDb = DriverManager.getConnection(DBURL, DBYUSER, DBPASS);
            PreparedStatement preparedStatement;
            preparedStatement  = connectionTestDb.prepareStatement(OUTBOUND_INVOICES_QUERY);
            ResultSet OutboundResultSet = preparedStatement.executeQuery();


                while (OutboundResultSet.next()) {

                    System.out.println(
                            "TYPE : "  +"Giden Fatura"+
                            "ENVELOPEUUID : "  + OutboundResultSet.getString("ENVELOPEUUID")+
                                    "INVOICEUUID : "  +OutboundResultSet.getString("INVOICEUUID")+
                                    "INVOICESERIALNO : "  +OutboundResultSet.getString("INVOICESERIALNO"));

                    return;
                }

                preparedStatement  = connectionTestDb.prepareStatement(INBOUND_INVOICES_QUERY);
                ResultSet InboundResultSet = preparedStatement.executeQuery();


                    while (InboundResultSet.next()) {

                        System.out.println(
                                "TYPE : "  +"Gelen Fatura  "+
                                " ENVELOPEUUID : "  + InboundResultSet.getString("ENVELOPEUUID")+
                                        " INVOICEUUID : "  +InboundResultSet.getString("INVOICEUUID")+
                                        " INVOICESERIALNO : "  +InboundResultSet.getString("INVOICESERIALNO"));

                        return;
                    }






            preparedStatement.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }



}
