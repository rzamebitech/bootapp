package com.mebi.bootapp.Form;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ObjectNode;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Riza on 9/14/2022
 * @project IntelliJ IDEA
 */
@Setter
public class EnvelopeForm {

    public EnvelopeForm(){}

    public ObjectNode createEnvelope (ResultSet EnvelopeResultSet) throws SQLException {
        ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,false);
        ObjectNode json = mapper.createObjectNode();

        json.put("CREATIONDATETIME", EnvelopeResultSet.getString("CREATIONDATETIME"));
        json.put("ENVELOPESTATUS", EnvelopeResultSet.getString("ENVELOPESTATUS"));
        json.put("STATUSDESC", EnvelopeResultSet.getString("STATUSDESC"));
        json.put("ENVELOPETYPE", EnvelopeResultSet.getString("ENVELOPETYPE"));
        json.put("ENVELOPEUUID", EnvelopeResultSet.getString("ENVELOPEUUID"));
        json.put("RECEIVERTITLE", EnvelopeResultSet.getString("RECEIVERTITLE"));
        json.put("RECEIVERVKNTCKN", EnvelopeResultSet.getString("RECEIVERVKNTCKN"));
        json.put("SENDERTITLE", EnvelopeResultSet.getString("SENDERTITLE"));
        json.put("SENDERVKNTCKN", EnvelopeResultSet.getString("SENDERVKNTCKN"));
        json.put("UPDATEDATE", EnvelopeResultSet.getString("UPDATEDATE"));

        return json;
    }

    public ObjectNode createEnvelopeDeatil (ResultSet EnvelopeResultSet) throws SQLException {
        ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,false);
        ObjectNode json = mapper.createObjectNode();

        json.put("ENVELOPESTATUSDETAIL", EnvelopeResultSet.getString("STATUSDETAIL"));
        json.put("ENVELOPELASTMODIFIED", EnvelopeResultSet.getString("LASTMODIFIED"));
        json.put("ENVELOPESTATUSCODE", EnvelopeResultSet.getString("STATUSCODE"));
        json.put("DOCUMENTSTATUS", EnvelopeResultSet.getString("DOCUMENTSTATUS"));

        return json;
    }


}
