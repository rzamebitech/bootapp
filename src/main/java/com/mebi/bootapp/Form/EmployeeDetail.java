package com.mebi.bootapp.Form;

public class EmployeeDetail {
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public void setTimeValue(String timeValue) {
        this.timeValue = timeValue;
    }

    private int employeeId;
    private String timeType;
    private String timeValue;

    public int getEmployeeId() {
        return employeeId;
    }

    public String getTimeType() {
        return timeType;
    }

    public String getTimeValue() {
        return timeValue;
    }
}
