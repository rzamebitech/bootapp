package com.mebi.bootapp.Form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author Riza on 9/14/2022
 * @project IntelliJ IDEA
 */
@Getter
@Setter
public class InvoiceForm {

    public InvoiceForm(){

    }


    private String serialNo;

    private boolean detailed;

    private String iban;

    private String secondTax;

    private String bank;

    private String CurrencyCode;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date paymentDay;








    public ObjectNode CreateInvoice(ResultSet resultSet, String type) throws SQLException {

        ObjectMapper mapper = new ObjectMapper().configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES,false);
        ObjectNode json = mapper.createObjectNode();



        json.put("INVOICESERIALNO", resultSet.getString("INVOICESERIALNO"));
        json.put("INVOICEDATE", resultSet.getString("INVOICEDATE"));
        json.put("INVOICEPROFILE", resultSet.getString("INVOICEPROFILE"));
        json.put("INVOICETYPECODE", resultSet.getString("INVOICETYPECODE"));
        json.put("INVOICEUUID", resultSet.getString("INVOICEUUID"));
        json.put("RESPONSETYPE", resultSet.getString("RESPONSETYPE"));
        json.put("ACTIONDATE", resultSet.getString("ACTIONDATE"));
        json.put("ACTIONTYPE", resultSet.getString("ACTIONTYPE"));
        json.put("RESPONSEDATETIME", resultSet.getString("RESPONSEDATETIME"));
        json.put("COMPANYNAME", resultSet.getString("COMPANYNAME"));
        json.put("COMPANYVKN", resultSet.getString("COMPANYVKN"));

        if(type.equals("Giden Fatura")){json.put("INVOICESOURCE", resultSet.getString("INVOICESOURCE"));}

        json.put("INVOICETYPE", type);

        return  json;
    }




}
