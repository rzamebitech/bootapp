package com.mebi.bootapp.Form;


import com.mebi.bootapp.Model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
public class UserForm { //Data Transfer Object

    @NotBlank
    private String name;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDarkmode() {
        return darkmode;
    }

    public void setDarkmode(String darkmode) {
        this.darkmode = darkmode;
    }

    private String darkmode;

    public User CreateUser() {

        User user = new User();
        user.setName(name);
        user.setPassword(password);
        user.setUsername(username);

        return user;

    }

    public User convertToUser() {
        User user = new User();
        user.setName(name);
        user.setUsername(username);
        user.setPassword(password);
        return user;
    }
}
