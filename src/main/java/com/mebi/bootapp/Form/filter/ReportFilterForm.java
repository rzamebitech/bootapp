package com.mebi.bootapp.Form.filter;

import com.mebi.bootapp.Model.QCompany;
import com.mebi.bootapp.Model.QEmployee;
import com.mebi.bootapp.Model.QReport;
import com.mebi.bootapp.Model.QReportEmployee;
import com.mebi.bootapp.Utilities.Time;
import com.querydsl.core.types.CollectionExpression;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPAExpressions;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


public class ReportFilterForm  extends FilterForm{

    private List<Long> listId;

    private int nametype;

    private String sort;

    private List<Long> companies;

    private List<Long> issues;

    private List<Long> employess;

    private String daterange;


    public List<Long> getListId() {
        return listId;
    }

    public void setListId(List<Long> listId) {
        this.listId = listId;
    }

    public List<Long> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Long> companies) {
        this.companies = companies;
    }

    public int getNametype() {
        return nametype;
    }

    public void setNametype(int nametype) {
        this.nametype = nametype;
    }



    public List<Long> getIssues() {
        return issues;
    }

    public void setIssues(List<Long> issues) {
        this.issues = issues;
    }

    public List<Long> getEmployess() {
        return employess;
    }

    public void setEmployess(List<Long> employess) {
        this.employess = employess;
    }

    public String getDaterange() {
        return daterange;
    }

    public void setDaterange(String daterange) {
        this.daterange = daterange;
    }

    @Override
    public Predicate predicate() {


        QReport qReport = QReport.report;
        QCompany company = QCompany.company;
        QReportEmployee reportEmployee = QReportEmployee.reportEmployee;

        if(!companies.isEmpty()&& companies.size()>0){
            addExp(qReport.company.id.in(companies));
        }
        if(listId.size()>0){
            addExp(qReport.id.in(listId));
        }


        if(employess.size()>0){

            addExp(qReport.id.in(
                    JPAExpressions.select(reportEmployee.reportId)
                            .from(reportEmployee)
                            .where(reportEmployee.employeeId.in(employess))
            ));


        }

        if (daterange != null) {
            String[] ss = daterange.split("-");
            try {
                Date from = Time.ConvertToDate(ss[0]);

                Date to = Time.ConvertToDate(ss[1]);

                addExp(qReport.createDate.between(from, to));

            } catch (ParseException exception) {
                // TODO Auto-generated catch-block stub.
                exception.printStackTrace();
            }

        }






        return predicate;


    }



}
