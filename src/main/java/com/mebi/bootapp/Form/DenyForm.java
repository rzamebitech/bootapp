package com.mebi.bootapp.Form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Riza on 9/23/2022
 * @project IntelliJ IDEA
 */

@Getter
@Setter
public class DenyForm {



    public String DBURL;
     public String DBYUSER;
     public String DBPASS;
     public String OID;
     public String CREATIONDATETIME;
     public String CUSTOMEROID;
     public String ENVELOPETYPE;
     public String ENVELOPEUUID;
     public String RECEIVERIDENTIFIER;
     public String RECEIVERTITLE;
     public String RECEIVERVKNTCKN;
     public String SENDERIDENTIFIER;
     public String SENDERTITLE;
     public String SENDERVKNTCKN;
     public String ELEMENTTYPE;
     public String CGROUPID;
     public String RESPONSEDDOCUMENTUUID;
     public String CRCVPRN;
      public String RESPONSECODE;
      public String RESPONSEDESCRIPTION;
      public String RESPONSEISSUEDATETIME;
      public String RESPONSEUUID;
      public String CFUNC;
      public String ApplicationResponseContent;
      public String CMSGTYPE;
      public String CTEST;
     public String RESPONSEDDOCUMENTTYPE;

     public int HASDRAFT;
      public int ENVELOPESIZE;
      public int CSTATUS;
      public int SQL_TRIGGER;
      public int LASTRECORD;





}
