package com.mebi.bootapp.Model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String taxId;


    public Company(String name, String taxId) {
        this.name = name;
        this.taxId = taxId;
    }

    public Company() {

    }
}
