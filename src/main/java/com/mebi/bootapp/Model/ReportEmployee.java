package com.mebi.bootapp.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "report_employess")
public class ReportEmployee {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private int minute;

    @Column
    private float hour;

    @Column
    private float day;

    @Column
    private float week;

    @Column
    private float mount;

    @Column
    private long employeeId;

    @Column
    private long reportId;


    @Column(name = "time_spent")
    private int timeSpent;




}