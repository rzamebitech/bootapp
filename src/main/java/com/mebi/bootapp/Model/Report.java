package com.mebi.bootapp.Model;

import com.mebi.bootapp.Model.Menu.Menu;
import com.mebi.bootapp.Model.Menu.MenuItems;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@QueryEntity
@Table(name = "report")
public class Report {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String summary;

    @Column(unique = true)
    private String issue;



    @Column
    private Date createDate;


    @Column
    private Date issueDate;


    @OneToOne
    @JoinColumn(name = "company")
    private Company company;
/*

    @ManyToMany(mappedBy = "reports" )
    private Set<Employee> employees;
*/




}
