package com.mebi.bootapp.Model;

public enum ERole
{
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN;
}