package com.mebi.bootapp.Model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String mail;

    @Column
    private String name;

    public Employee(Long id, String mail, String name) {
        this.id = id;
        this.mail = mail;
        this.name = name;
    }

  /*  @ManyToMany()
    @JoinTable(name = "report_employess", joinColumns = {
            @JoinColumn(name = "employee_id", referencedColumnName = "id") }, inverseJoinColumns = {
            @JoinColumn(name = "report_id", referencedColumnName = "id") })
    private Set<Report> reports;*/

    public Employee() {

    }
}
