package com.mebi.bootapp.Model.Menu;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "menu")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String cls;

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getExpanded() {
        return expanded;
    }

    public void setExpanded(String expanded) {
        this.expanded = expanded;
    }

    public String getVue_permission() {
        return vue_permission;
    }

    public void setVue_permission(String vue_permission) {
        this.vue_permission = vue_permission;
    }

    private String expanded;

    private String vue_permission;


    public Menu() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Set<MenuItems> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(Set<MenuItems> menuItems) {
        this.menuItems = menuItems;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "menu", fetch = FetchType.EAGER)
    private Set<MenuItems> menuItems;



    /*TODO One2Many Relation to items here*/


}
