package com.mebi.bootapp.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "logarchive")
@Getter
@Setter
public class LogArchive {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String logdate;

    @NotBlank
    @Column
    private String type;


    @Column
    private Integer count;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(2000)")
    private String title;

    @NotBlank
    @Column
    private String level;
}
