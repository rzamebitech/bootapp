package com.mebi.bootapp;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.mebi.bootapp.Utilities.Log.*;

public class UnzipApplication {

    static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    static String filepath  = "C:/Helper-output/20/";

    public static void main(String[] args) throws InterruptedException {

        //get zip files
        int count = 0;
        int invoicesCount = 0;
        int matchedInvoice = 0;
        Path source = null;
        String xmlPath;
        Path target = Paths.get("C:/Helper-output/unzip/");
        File folder = new File("C:/Helper-output/20/");
        File[] listOfFiles = folder.listFiles();
        int limit=0;
        StringBuilder invoices = new StringBuilder();
        invoices.append("CON2016000045122");
        invoices.append("CON2016000045123");
        invoices.append("CON2016000045124");
        invoices.append("CON2016000045656");
        invoices.append("CON2016000045657");
        invoices.append("CON2016000046732");
        invoices.append("CON2016000046733");
        invoices.append("CON2016000046734");
        invoices.append("CON2016000046735");
        invoices.append("CON2016000046737");
        invoices.append("CON2016000047199");
        invoices.append("CON2016000047200");
        invoices.append("CON2016000047201");
        invoices.append("CON2016000047202");
        invoices.append("CON2016000047204");
        invoices.append("CON2016000047568");
        invoices.append("CON2016000047953");
        invoices.append("CON2017000000960");
        invoices.append("CON2017000026777");
        invoices.append("CON2017000026776");
        invoices.append("CON2017000027372");
        invoices.append("CON2017000027373");
        invoices.append("CON2017000032333");
        invoices.append("CON2017000037466");
        invoices.append("CON2017000050809");


       if(listOfFiles!=null){
           for (File listOfFile : listOfFiles) {
               limit++;
               count++;


               if(limit>100){
                   Thread.sleep(500);
                   limit = 0;
               }

               if (listOfFile.isFile()) {

                    source = Paths.get(filepath+listOfFile.getName());
                   try {

                       String fileNameWithOutExt = FilenameUtils.removeExtension(listOfFile.getName());
                       xmlPath = target.toUri().getPath()+fileNameWithOutExt+".xml";
                     /* xmlPath = target.toUri().getPath()+"09bc9ffd-3f29-4b2b-a9dc-3a17b659eaf9.xml";*/

                       File f = new File(xmlPath);
                       if(!f.exists()) {
                         /*  System.out.println(" Unzipping : " +listOfFile.getName());*/
                           unzipFolder(source, target);
                       }else{
                          /* System.out.println(listOfFile.getName()+ " is already unzipped!");*/
                       }

                      /* System.out.println("Start Read Xml");*/
                       //TODO read xml
                       dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                       DocumentBuilder db = dbf.newDocumentBuilder();
                       Document doc = db.parse(new File(xmlPath));
                       doc.getDocumentElement().normalize();
                      /* System.out.println("Root Element :" + doc.getDocumentElement().getNodeName());
                       System.out.println("------");*/


                       NodeList BusinessDocuments = doc.getElementsByTagName("sh:StandardBusinessDocument");
                       if(BusinessDocuments.getLength()<1){
                          /* System.out.println("Can not Find <sh:StandardBusinessDocument> Tag in " +listOfFile.getName()+ " Xml File");*/
                           continue;
                       }
                       Node BusinessDocument = BusinessDocuments.item(0);
                       Element element = (Element) BusinessDocument;
                       NodeList PackageList = element.getElementsByTagName("ef:Package");
                       if(PackageList.getLength()<1){
                          /* System.out.println("Can not Find <ef:Package> Tag in " +listOfFile.getName()+ " Xml File");*/
                           continue;
                       }
                       Node Package = PackageList.item(0);
                       Element pkgelement = (Element) Package;
                       NodeList Elements = pkgelement.getElementsByTagName("Elements");
                       if(Elements.getLength()<1){
                          /* System.out.println("Can not Find <Elements> Tag in " +listOfFile.getName()+ " Xml File");*/
                           continue;
                       }
                       Node ElementNode = PackageList.item(0);
                       Element ElementList = (Element) ElementNode;
                       NodeList ElementNodes = ElementList.getElementsByTagName("ElementList");
                       if(ElementNodes.getLength()<1){
                          /* System.out.println("Can not Find <Elements> Tag in " +listOfFile.getName()+ " Xml File");*/
                           continue;
                       }
                       Node InvoiceNode = ElementNodes.item(0);
                       Element InvoiceList = (Element) InvoiceNode;
                       NodeList InvoiceListNodes = InvoiceList.getElementsByTagName("Invoice");
                       if(InvoiceListNodes.getLength()<1){
                         /*  System.out.println("Can not Find <Invoice> Tag in " +listOfFile.getName()+ " Xml File");*/
                           continue;
                       }
                       for (int temp = 0; temp < InvoiceListNodes.getLength(); temp++) {
                           Node node = InvoiceListNodes.item(temp);
                           if (node.getNodeType() == Node.ELEMENT_NODE) {
                               Element elm = (Element) node;
                               String id = "";
                               Node ser = elm.getElementsByTagName("cbc:ID").item(0);

                               if(ser!=null && ser.getTextContent()!=null){
                                   id = ser.getTextContent();
                                   if(id.equals("")){
                                       /*  System.out.println("Can not Find cbc:ID Tag in " +listOfFile.getName()+ " Xml File");*/
                                   }else{
                                       invoicesCount++;
                                       System.out.println(ANSI_GREEN + "Invoice Serial No is :" + ANSI_RESET + id);
                                       if(invoices.toString().contains(id)){
                                           System.out.println(ANSI_RED + "FOUND Invoice Serial No is :" + ANSI_RESET + id);
                                           matchedInvoice++;
                                       }
                                   }
                               }

                           }



                       }





                   } catch (IOException | ParserConfigurationException | SAXException e) {
                       e.printStackTrace();
                   }
                  /* System.out.println("File " + listOfFile.getName());*/



               } else if (listOfFile.isDirectory()) {
                 /*  System.out.println("Directory " + listOfFile.getName());*/
               }
           }
           System.out.println("Checked " + count + " files and " + invoicesCount + " invoices / " + matchedInvoice + " Invoices Matched" );
       }
    }
    public static void unzipFolder(Path source, Path target) throws IOException {

        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(source.toFile()))) {

            // list files in zip
            ZipEntry zipEntry = zis.getNextEntry();

            while (zipEntry != null) {

                boolean isDirectory = false;
                // example 1.1
                // some zip stored files and folders separately
                // e.g data/
                //     data/folder/
                //     data/folder/file.txt
                if (zipEntry.getName().endsWith(File.separator)) {
                    isDirectory = true;
                }

                Path newPath = zipSlipProtect(zipEntry, target);

                if (isDirectory) {
                    Files.createDirectories(newPath);
                } else {

                    // example 1.2
                    // some zip stored file path only, need create parent directories
                    // e.g data/folder/file.txt
                    if (newPath.getParent() != null) {
                        if (Files.notExists(newPath.getParent())) {
                            Files.createDirectories(newPath.getParent());
                        }
                    }

                    // copy files, nio
                    Files.copy(zis, newPath, StandardCopyOption.REPLACE_EXISTING);

                    // copy files, classic
                    /*try (FileOutputStream fos = new FileOutputStream(newPath.toFile())) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }*/
                }

                zipEntry = zis.getNextEntry();

            }
            zis.closeEntry();

        }

    }

    // protect zip slip attack
    public static Path zipSlipProtect(ZipEntry zipEntry, Path targetDir)
            throws IOException {

        // test zip slip vulnerability
        // Path targetDirResolved = targetDir.resolve("../../" + zipEntry.getName());

        Path targetDirResolved = targetDir.resolve(zipEntry.getName());

        // make sure normalized file still has targetDir as its prefix
        // else throws exception
        Path normalizePath = targetDirResolved.normalize();
        if (!normalizePath.startsWith(targetDir)) {
            throw new IOException("Bad zip entry: " + zipEntry.getName());
        }

        return normalizePath;
    }
}
