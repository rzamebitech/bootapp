
package com.mebi.bootapp.Utilities.XmlUtil;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.HashSet;
import java.io.OutputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.helpers.DefaultHandler;
import java.io.InputStream;
import java.util.regex.Pattern;
import org.slf4j.Logger;

public class Utils
{
    private static final Logger logger;
    public static final Pattern XML_ENTITY_PATTERN;

    public static void parseSAX(final InputStream iStream, final DefaultHandler handler, final String encoding) {
        try {
            final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
            final SAXParser parser = parserFactory.newSAXParser();
            final InputSource source = new InputSource(iStream);
            if (encoding != null) {
                source.setEncoding(encoding);
            }
            parser.parse(source, handler);
        }
        catch (Exception e) {
            Utils.logger.error(e.getMessage(), (Throwable)e);
        }
        finally {
            close(iStream);
        }
    }

    public static void close(final InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            }
            catch (IOException ex) {}
        }
    }

    public static void close(final OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            }
            catch (IOException ex) {}
        }
    }

    public static String getCleanedXml(final String xmlString) {
        final Matcher m = Utils.XML_ENTITY_PATTERN.matcher(xmlString);
        final Set<String> replaceSet = new HashSet<String>();
        while (m.find()) {
            String group = m.group(1);
            if (group != null) {
                final int val = Integer.parseInt(group, 16);
                if (!isInvalidXmlChar(val)) {
                    continue;
                }
                replaceSet.add("&#x" + group + ";");
            }
            else {
                if ((group = m.group(2)) == null) {
                    continue;
                }
                final int val = Integer.parseInt(group);
                if (!isInvalidXmlChar(val)) {
                    continue;
                }
                replaceSet.add("&#" + group + ";");
            }
        }
        String cleanedXmlString = xmlString;
        for (final String replacer : replaceSet) {
            cleanedXmlString = cleanedXmlString.replaceAll(replacer, "");
        }
        return cleanedXmlString;
    }

    private static boolean isInvalidXmlChar(final int val) {
        return val != 9 && val != 10 && val != 13 && (val < 32 || val > 55295) && (val < 65536 || val > 1114111);
    }

    static {
        logger = LoggerFactory.getLogger((Class)Utils.class);
        XML_ENTITY_PATTERN = Pattern.compile("\\&\\#(?:x([0-9a-fA-F]+)|([0-9]+))\\;");
    }

    public static Document parseDOM(InputStream iStream, String encoding) {
        try {

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory
                    .newDocumentBuilder();
            InputSource source = new InputSource(iStream);
            if (encoding != null)
                source.setEncoding(encoding);
            return documentBuilder.parse(source);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


}