package com.mebi.bootapp.Utilities.XmlUtil;

import com.mebi.bootapp.Config.Properties;
import org.slf4j.LoggerFactory;
import net.sf.saxon.s9api.XsltTransformer;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;
import net.sf.saxon.s9api.Destination;

import java.io.*;

import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.XsltCompiler;
import org.xml.sax.SAXException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.Processor;
import org.slf4j.Logger;

public class SchematronXSLTVerifier
{
    private static final Logger logger;

    private static Processor saxonProc;
    private static XsltExecutable xsltExecutable;
    private static Schema envelopeSchema;
    private static Schema invoiceSchema;
    private static Schema appResponseSchema;
    private static Schema despatchSchema;
    private static Schema receiptAdviceSchema;

    private synchronized void initComponents() throws SAXException {
        if (SchematronXSLTVerifier.envelopeSchema != null) {
            return;
        }
        final SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        SchematronXSLTVerifier.envelopeSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/Envelope/PackageProxy.xsd")));
        SchematronXSLTVerifier.invoiceSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-Invoice-2.1.xsd")));
        SchematronXSLTVerifier.appResponseSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-ApplicationResponse-2.1.xsd")));
        SchematronXSLTVerifier.despatchSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-DespatchAdvice-2.1.xsd")));
        SchematronXSLTVerifier.receiptAdviceSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-ReceiptAdvice-2.1.xsd")));
    }

    public XSDVerifyOutput verifyXSDSchema(final String xmlContent, String type) throws UnsupportedEncodingException, SAXException {
        if (SchematronXSLTVerifier.envelopeSchema == null) {
            this.initComponents();
        }
        final Source xmlFile = new StreamSource(new ByteArrayInputStream(xmlContent.getBytes("UTF-8")));
        final XSDVerifyOutput verifyResult = new XSDVerifyOutput();
        final List<String> exceptions = new LinkedList<>();
        final int[] num = {0};


        try {

           // type = "appResponse";
            if ("envelope".equals(type)) {

                Validator envelopeValidator = SchematronXSLTVerifier.envelopeSchema.newValidator();
                SetHandler(xmlFile, exceptions, envelopeValidator, num);
                //SchematronXSLTVerifier.envelopeSchema.newValidator().validate(xmlFile);
            }
            else if ("invoice".equals(type)) {

                Validator invoiceValidator = SchematronXSLTVerifier.invoiceSchema.newValidator();
                SetHandler(xmlFile, exceptions, invoiceValidator,num);
            }
            else if ("appResponse".equals(type)) {

                Validator appResponseValidator = SchematronXSLTVerifier.appResponseSchema.newValidator();
                SetHandler(xmlFile, exceptions, appResponseValidator, num);
                // SchematronXSLTVerifier.appResponseSchema.newValidator().validate(xmlFile);
            }
            else if ("despatch".equals(type)) {

                Validator despatchValidator = SchematronXSLTVerifier.despatchSchema.newValidator();
                SetHandler(xmlFile, exceptions, despatchValidator, num);
               // SchematronXSLTVerifier.despatchSchema.newValidator().validate(xmlFile);
            }
            else if ("receiptAdvice".equals(type)) {

                Validator receiptAdviceValidator = SchematronXSLTVerifier.receiptAdviceSchema.newValidator();
                SetHandler(xmlFile, exceptions, receiptAdviceValidator, num);
               // SchematronXSLTVerifier.receiptAdviceSchema.newValidator().validate(xmlFile);
            }
            verifyResult.setValid(true);
        }
        catch (Exception e) {
            SchematronXSLTVerifier.logger.error("XSD verification failed.!! ", e);
            verifyResult.setValid(false);
            assert e instanceof SAXParseException;
            verifyResult.setMessage(e.getMessage());

        }
        if(exceptions.size()>0){
            verifyResult.setValid(false);
        }
        verifyResult.setMessages(exceptions);
        return verifyResult;
    }

    private void SetHandler(Source xmlFile, List<String> exceptions, Validator validator, int[] num) throws SAXException, IOException {

        validator.setErrorHandler(new ErrorHandler()
        {
            @Override
            public void warning(SAXParseException e) {
                num[0]++;
                exceptions.add(num[num.length-1]+ "- WARNING: "+e.getMessage() + " ||  Line Number: " + e.getLineNumber() + " | Column Number:  " + e.getColumnNumber());
            }

            @Override
            public void fatalError(SAXParseException e) {
                num[0]++;
                exceptions.add(num[num.length-1]+ "- FATAL ERROR: "+e.getMessage() + " ||  Line Number: " + e.getLineNumber() + " | Column Number:  " + e.getColumnNumber());

            }

            @Override
            public void error(SAXParseException e) {
                num[0]++;
                exceptions.add(num[num.length-1]+ "-  ERROR: "+e.getMessage() + " ||  Line Number: " + e.getLineNumber() + " | Column Number:  " + e.getColumnNumber());

            }
        });
        validator.validate(xmlFile);
    }

    private void initSaxonService() {
        try {
            if (SchematronXSLTVerifier.xsltExecutable != null) {
                return;
            }
            SchematronXSLTVerifier.saxonProc = new Processor(false);
            final XsltCompiler xsltCompiler = SchematronXSLTVerifier.saxonProc.newXsltCompiler();
            SchematronXSLTVerifier.xsltExecutable = xsltCompiler.compile((Source)new StreamSource(new FileInputStream(new File(Properties.schematronPath+"schematron/UBL-TR_Main_Schematron.xslt"))));
        }
        catch (Exception ex) {
            SchematronXSLTVerifier.logger.error(ex.getMessage(), (Throwable)ex);
            throw new RuntimeException("Saxon init failed.!!");
        }
    }

    public SchematronVerifyOutput verifySchematron(final String xmlContent) {
        return this.verifySchematron(xmlContent, null);
    }

    public SchematronVerifyOutput verifySchematron(final String xmlContent, final String type) {
        final SchematronVerifyOutput verifyResult = new SchematronVerifyOutput();
        verifyResult.setValid(false);
        try {
            this.initSaxonService();
            final XsltTransformer xsltTransformer = SchematronXSLTVerifier.xsltExecutable.load();
            if (type != null) {
                xsltTransformer.setParameter(new QName("type"), (XdmValue)new XdmAtomicValue(type));
            }
            xsltTransformer.setSource((Source)new StreamSource(new ByteArrayInputStream(xmlContent.getBytes("UTF-8"))));
            final StringWriter resultWriter = new StringWriter();
            xsltTransformer.setDestination((Destination)SchematronXSLTVerifier.saxonProc.newSerializer((Writer)resultWriter));
            xsltTransformer.transform();
            final String resultString = resultWriter.toString();
            if (resultString.length() == 0) {
                verifyResult.setValid(false);
                verifyResult.setMessage("XSLT schematron processing failed with no return values");
            }
            else {
                final SchematronValidateResultHandler schematronValidateResultHandler = new SchematronValidateResultHandler();
                Utils.parseSAX(new ByteArrayInputStream(resultString.getBytes("UTF-8")), schematronValidateResultHandler, "UTF-8");
                if (schematronValidateResultHandler.getParseResult().equals("")) {
                    verifyResult.setValid(true);
                }
                else {
                    verifyResult.setValid(false);
                    String lines[] = schematronValidateResultHandler.getParseResult().split("\\r?\\n");
                    verifyResult.setMessages(Arrays.asList(lines));
                    verifyResult.setMessage(schematronValidateResultHandler.getParseResult());
                }
            }
        }
        catch (Exception e) {
            SchematronXSLTVerifier.logger.error(e.getMessage(), (Throwable)e);
            verifyResult.setValid(false);
            verifyResult.setMessage(e.getMessage());
        }
        return verifyResult;
    }

    public SchematronVerifyOutput verifySchematronWithoutCBCID(final String xmlContent) {
        final SchematronVerifyOutput verifyResult = new SchematronVerifyOutput();
        verifyResult.setValid(false);
        try {
            this.initSaxonService();
            final XsltTransformer xsltTransformer = SchematronXSLTVerifier.xsltExecutable.load();
            xsltTransformer.setParameter(new QName("ignoreCBCID"), (XdmValue)new XdmAtomicValue("yes"));
            xsltTransformer.setSource((Source)new StreamSource(new ByteArrayInputStream(xmlContent.getBytes("UTF-8"))));
            final StringWriter resultWriter = new StringWriter();
            xsltTransformer.setDestination((Destination)SchematronXSLTVerifier.saxonProc.newSerializer((Writer)resultWriter));
            xsltTransformer.transform();
            final String resultString = resultWriter.toString();
            if (resultString.length() == 0) {
                verifyResult.setValid(false);
                verifyResult.setMessage("XSLT schematron processing failed with no return values");
            }
            else {
                final SchematronValidateResultHandler schematronValidateResultHandler = new SchematronValidateResultHandler();
                Utils.parseSAX(new ByteArrayInputStream(resultString.getBytes("UTF-8")), schematronValidateResultHandler, "UTF-8");
                if (schematronValidateResultHandler.getParseResult().equals("")) {
                    verifyResult.setValid(true);
                }
                else {
                    verifyResult.setValid(false);
                    verifyResult.setMessage(schematronValidateResultHandler.getParseResult());
                }
            }
        }
        catch (Exception e) {
            SchematronXSLTVerifier.logger.error(e.getMessage(), (Throwable)e);
            verifyResult.setValid(false);
            verifyResult.setMessage(e.getMessage());
        }
        return verifyResult;
    }

    static {
        logger = LoggerFactory.getLogger((Class)XSDVerifier.class);
        SchematronXSLTVerifier.envelopeSchema = null;
        SchematronXSLTVerifier.invoiceSchema = null;
        SchematronXSLTVerifier.appResponseSchema = null;
        SchematronXSLTVerifier.despatchSchema = null;
        SchematronXSLTVerifier.receiptAdviceSchema = null;
    }
}