
package com.mebi.bootapp.Utilities.XmlUtil;

import org.springframework.xml.xpath.Jaxp13XPathTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.transform.dom.DOMSource;
import java.io.FileInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class XmlUtilities
{
    public HashMap<String, List<String>> schematronCheck(final String xmlFile, String type) {
        String schematronType = null;
        final SchematronXSLTVerifier schematronXSLTVerifier = new SchematronXSLTVerifier();
        File outputFile = null;
        HashMap<String, List<String>> response = new HashMap<>();
        try {
            schematronType = "efatura";
            outputFile = new File(xmlFile);
            FileInputStream fis = null;
            final byte[] xmlByte = new byte[(int)outputFile.length()];
            fis = new FileInputStream(xmlFile);
            fis.read(xmlByte);
            fis.close();
            final String xml = new String(xmlByte, "UTF-8");

           /* Document appResponse = Utils.parseDOM(new org.fusesource.hawtbuf.ByteArrayInputStream(xmlByte), "UTF-8");
            Jaxp13XPathTemplate jaxp13xPathTemplate = new Jaxp13XPathTemplate();
            DOMSource domSource = new DOMSource(appResponse);

            String xmlDocTag = "Invoice";
            String typeTag = "InvoiceTypeCode";
            String supplierParty = "AccountingSupplierParty";
            String customerParty = "AccountingCustomerParty";

            String serialNo = jaxp13xPathTemplate.evaluateAsString("//".concat(xmlDocTag).concat("/ID"), domSource);



            String  orderRef = jaxp13xPathTemplate.evaluateAsString("//".concat(xmlDocTag).concat("/OrderReference/ID"), domSource);

            String supVkn =  getParty(jaxp13xPathTemplate, domSource, supplierParty, xmlDocTag);
            String cusID = getParty(jaxp13xPathTemplate, domSource, customerParty, xmlDocTag);
            String cusName = getNameParty(jaxp13xPathTemplate, domSource, customerParty, xmlDocTag);
            String supName = getNameParty(jaxp13xPathTemplate, domSource, supplierParty, xmlDocTag);

            String invoiceTypeCode = jaxp13xPathTemplate.evaluateAsString("//".concat(xmlDocTag).concat("/".concat(typeTag)), domSource);*/



            //TODO check xml type here
            //case BISENVELOPE remove tag



            List<String> SchemaOK = new ArrayList<>();
            SchemaOK.add("Xslt Schema Kontrolu Başarılı");
            List<String> SchematronOK = new ArrayList<>();
            SchematronOK.add("Xslt Schematron Kontrolu Başarılı");

            final XSDVerifyOutput xsdVerifyOutput = schematronXSLTVerifier.verifyXSDSchema(xml, type);
            final SchematronVerifyOutput schematronVerifyOutput = schematronXSLTVerifier.verifySchematron(xml, schematronType);


            if (schematronVerifyOutput.isValid() && xsdVerifyOutput.isValid()) {
                response.put("Şema Konrol Sonucu", SchemaOK);
                response.put("Şematron Konrol Sonucu", SchematronOK);
                System.out.println("Xslt Schematron Kontrolu Başarılı");
                outputFile.delete();
                return response;
            }

            response.put("Şema Konrol Sonucu",xsdVerifyOutput.isValid() ? SchemaOK: xsdVerifyOutput.getMessages());
            response.put("Şematron Konrol Sonucu",schematronVerifyOutput.isValid() ? SchematronOK: schematronVerifyOutput.getMessages());

           // final String errorMessage = (xsdVerifyOutput.isValid() ? "" : xsdVerifyOutput.getMessage()) + System.lineSeparator() + (schematronVerifyOutput.isValid() ? "" : schematronVerifyOutput.getMessage());
            outputFile.delete();
            //System.out.println("Xslt Schematron Kontrolu Hatalı:" + errorMessage);
            return response;
        }
        catch (Exception ex) {
            outputFile.delete();
            System.out.println(ex.getMessage());
            List<String> hata = new ArrayList<>();
            hata.add(ex.getMessage());
            response.put("Xslt Schematron Kontrolu Hatalı: " ,hata);
            return response;
            //return "Xslt Schematron Kontrolu Hatalı:" + ex;
        }
    }

    public String getParty(Jaxp13XPathTemplate jaxp13xPathTemplate, DOMSource domSource, String partyName, String docTag){
        List<Node> nodeList = jaxp13xPathTemplate.evaluateAsNodeList("//".concat(docTag).concat("/").concat(partyName).concat("/Party/PartyIdentification/ID"), domSource);
        for (Iterator iterator = nodeList.iterator(); iterator.hasNext();) {
            Node node = (Node) iterator.next();
            Node attr = node.getAttributes().getNamedItem("schemeID");
            if(attr != null && (attr.getNodeValue().equals("VKN") || attr.getNodeValue().equals("TCKN"))) {
                return node.getTextContent();
            }

        }
        return null;
    }

    public String getNameParty(Jaxp13XPathTemplate jaxp13xPathTemplate, DOMSource domSource, String partyName, String docTag){
        List<Node> nodeList = jaxp13xPathTemplate.evaluateAsNodeList("//".concat(docTag).concat("/").concat(partyName).concat("/Party/PartyName/Name"), domSource);
        for (Iterator iterator = nodeList.iterator(); iterator.hasNext();) {
            Node node = (Node) iterator.next();
                return node.getTextContent();
        }
        return null;
    }
}