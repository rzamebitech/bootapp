package com.mebi.bootapp.Utilities.XmlUtil;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SchematronValidateResultHandler extends DefaultHandler
{
    private StringBuffer buffer;
    private boolean failFound;
    private boolean failText;
    private int itemNumber;

    public SchematronValidateResultHandler() {
        this.buffer = new StringBuffer();
        this.failFound = false;
        this.failText = false;
        this.itemNumber = 0;
    }

    public String getParseResult() {
        if (this.buffer != null) {
            return this.buffer.toString();
        }
        return "";
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if ("svrl:failed-assert".equals(qName)) {
            this.failFound = true;
        }
        else if (this.failFound && "svrl:text".equals(qName)) {
            this.failText = true;
            this.buffer.append(++this.itemNumber + "- ");
        }
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.failText) {
            this.buffer.append(ch, start, length);
        }
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if (this.failFound) {
            this.failFound = false;
            this.failText = false;
            this.buffer.append(System.lineSeparator());
        }
    }
}