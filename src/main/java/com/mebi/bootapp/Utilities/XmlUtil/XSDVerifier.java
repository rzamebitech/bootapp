package com.mebi.bootapp.Utilities.XmlUtil;

import com.mebi.bootapp.Config.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;

@Component
public class XSDVerifier {

    private final static Logger logger = LoggerFactory.getLogger(XSDVerifier.class);

    private static Schema envelopeSchema = null;
    private static Schema invoiceSchema = null;
    private static Schema appResponseSchema = null;
    private static Schema despatchSchema = null;
    private static Schema receiptAdviceSchema = null;

    private synchronized void initComponents() throws SAXException {

        if (envelopeSchema != null)
            return;

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        envelopeSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/Envelope/PackageProxy.xsd")));
        invoiceSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-Invoice-2.1.xsd")));
        appResponseSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-ApplicationResponse-2.1.xsd")));
        despatchSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-DespatchAdvice-2.1.xsd")));
        receiptAdviceSchema = schemaFactory.newSchema(new StreamSource(new File(Properties.schematronPath+"xsd/maindoc/UBL-ReceiptAdvice-2.1.xsd")));

    }

    public XSDVerifyOutput verifyXSDSchema(String xmlFileContent, String type) throws UnsupportedEncodingException, SAXException {

        if (envelopeSchema == null)
            initComponents();

        Source xmlFile = new StreamSource(new ByteArrayInputStream(xmlFileContent.getBytes("UTF-8")));
        XSDVerifyOutput verifyResult = new XSDVerifyOutput();

        try {
            if ("envelope".equals(type)) {
                envelopeSchema.newValidator().validate(xmlFile);
            } else if ("invoice".equals(type)) {
                invoiceSchema.newValidator().validate(xmlFile);
            } else if ("appResponse".equals(type)) {
                appResponseSchema.newValidator().validate(xmlFile);
            } else if ("despatch".equals(type)) {
                despatchSchema.newValidator().validate(xmlFile);
            } else if ("receiptAdvice".equals(type)) {
                receiptAdviceSchema.newValidator().validate(xmlFile);
            }
            verifyResult.setValid(true);
        } catch (Exception e) {
            logger.error("XSD verification failed.!! ", e);
            verifyResult.setValid(false);
            verifyResult.setMessage(e.getMessage());
        }

        return verifyResult;
    }

}
