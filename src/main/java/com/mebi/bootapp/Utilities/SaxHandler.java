package com.mebi.bootapp.Utilities;
import java.util.UUID;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

    private boolean bInstanceIdentifier = false;
    private boolean bUUID = false;
    private StringBuilder xmlContent;
    private String instanceIdentifierValue;
    private String UUIDValue;

    public SaxHandler() {
        xmlContent = new StringBuilder();
    }

    public String getXmlContent() {
        return xmlContent.toString();
    }

    public String getInstanceIdentifierValue() {
        return instanceIdentifierValue;
    }

    public String getUUIDValue() {
        return UUIDValue;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        String elementName = qName.substring(qName.indexOf(":") + 1);

        if (elementName.equalsIgnoreCase("InstanceIdentifier")) {
            bInstanceIdentifier = true;
        }
        if (elementName.equalsIgnoreCase("UUID")) {
            bUUID = true;
        }
        xmlContent.append("<").append(qName);
        for (int i = 0; i < attributes.getLength(); i++) {
            xmlContent.append(" ")
                    .append(attributes.getQName(i))
                    .append("=\"")
                    .append(attributes.getValue(i))
                    .append("\"");
        }
        xmlContent.append(">");
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        xmlContent.append("</").append(qName).append(">");
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if (bInstanceIdentifier) {
            instanceIdentifierValue = new String(ch, start, length);
            String  newEnvelopeUUID = UUID.randomUUID().toString();
            xmlContent.append(newEnvelopeUUID);
            instanceIdentifierValue = newEnvelopeUUID;
            bInstanceIdentifier = false;

        }else if (bUUID){
            UUIDValue = new  String(ch, start, length);
            xmlContent.append(UUID.randomUUID().toString());
            bUUID = false;
        }

        else {
            xmlContent.append(new String(ch, start, length));
        }
    }




}
