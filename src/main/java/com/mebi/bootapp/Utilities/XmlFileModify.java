package com.mebi.bootapp.Utilities;

import com.mebi.bootapp.Config.Properties;
import com.mebi.bootapp.Form.InvoiceForm;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.UUID;

/**
 * @author Riza on 9/21/2022
 * @project IntelliJ IDEA
 */
public class XmlFileModify {


    private final String url = "jdbc:oracle:thin:@172.26.36.40:1521:efatura";
    private final String username = "efaturaprod";
    private final String password = "MNsCsa72";
    boolean willZip;


    public XmlFileModify(boolean willZip) {
        this.willZip = willZip;
    }

    public String prepareInvoiceXml(ResultSet resultSet, InvoiceForm invoiceForm) {

        try{
              return  createInvoiceXmlFile(resultSet.getString("INVOICECONTENT")
                        ,resultSet.getString("SENDERVKNTCKN")
                        ,resultSet.getString("RECEIVERVKNTCKN")
                        ,resultSet.getString("RECEIVERTITLE")
                        ,resultSet.getString("INVOICESERIALNO"),invoiceForm);
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return ex.getMessage().toString();
        }

    }

    public String createInvoiceXmlFile(String content, String senderVKNTCKN, String receiverVKNTCKN, String RecieverTitle, String invoiceNo, InvoiceForm invoiceForm) throws IOException {

        String newContent="";
        String xmlFile = Properties.path+"/Fatura/"+invoiceNo+".xml";
        if(!willZip){
            File dir = new File(Properties.path+"/Fatura/");
            FileUtils.cleanDirectory(dir);
            System.out.println("createInvoiceXmlFile => "+Properties.path+"/Fatura/ cleaned ");
        }



        try {


            newContent= "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                    "<BISEnvelope type=\"send\">" +
                    "<EnvelopeDetail  senderVKN_TCKN=\"" + senderVKNTCKN+"\" receiverVKN_TCKN=\"" + receiverVKNTCKN + "\">"+
                    "</EnvelopeDetail>"+
                    "<Invoices>"+
                    content +
                    "</Invoices>"+
                    "</BISEnvelope>";

            Files.write(Paths.get(xmlFile),newContent.getBytes(StandardCharsets.UTF_8));
            System.out.println("Sign node is cleaning ");
            //İmza node Siliniyor
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            System.out.println("Parsing xml ");
            Document doc = db.parse(xmlFile);
            System.out.println("xml pardes successfully");
            Node imza = doc.getElementsByTagName("ext:ExtensionContent").item(0);
            NodeList list = imza.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                if ("ds:Signature".equals(node.getNodeName())) {
                    imza.removeChild(node);
                }
            }

            // dosya güncellenir
            System.out.println("Update File ");
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();

            DOMSource src = new DOMSource(doc);
            StreamResult res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);


            //UUID node yeni guid ile güncellenir
            System.out.println("Updating new uuid ");
            doc = db.parse(xmlFile);
            Node uuiNode = doc.getElementsByTagName("Invoice").item(0);
            NodeList uuidList = uuiNode.getChildNodes();
            for (int i = 0; i < uuidList.getLength(); i++) {
                Node node = uuidList.item(i);
                if ("cbc:UUID".equals(node.getNodeName())) {
                    node.setTextContent(UUID.randomUUID().toString());
                }
            }

            src = new DOMSource(doc);
            res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);



            if(invoiceForm.isDetailed()){

                if(invoiceForm.getSecondTax()!= null && invoiceForm.getSecondTax()!= ""){
                    setManKamu(doc,invoiceForm);
                    src = new DOMSource(doc);
                    res = new StreamResult(new File(xmlFile));
                    transformer.transform(src, res);

                    setPeymant(doc,invoiceForm);

                    src = new DOMSource(doc);
                    res = new StreamResult(new File(xmlFile));
                    transformer.transform(src, res);

                }


            }







            //partyName veya Person Name Ekleniyor
           if(receiverVKNTCKN.length()>10){ // TCKN
                //adding peron tag
                doc = db.parse(xmlFile);
                Node partyNode = doc.getElementsByTagName("cac:Person").item(0);
                NodeList partyList = partyNode.getChildNodes();
                for (int i = 0; i < partyList.getLength(); i++) {
                    Node node = partyList.item(i);
                    if ("cbc:FirstName".equals(node.getNodeName())) {
                        node.setTextContent("NECDET DEMİREL");
                    }
                    if ("cbc:FamilyName".equals(node.getNodeName())) {
                        node.setTextContent("HAYAT ECZANESİ");
                    }

                }
                src = new DOMSource(doc);
                res = new StreamResult(new File(xmlFile));
                transformer.transform(src, res);
                 transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                doc = db.parse(xmlFile);
                setParyName(RecieverTitle, doc);// Alici TCKN'li ise hem person hem de paryname bilgileri giriliyor



            }else{// VKN
                doc = db.parse(xmlFile);
                setParyName(RecieverTitle, doc);
            }

           /* src = new DOMSource(doc);
            res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);

            XPath xp = XPathFactory.newInstance().newXPath();
            NodeList nl = (NodeList) xp.evaluate("//text()[normalize-space(.)='']", doc, XPathConstants.NODESET);

            for (int i=0; i < nl.getLength(); ++i) {
                Node node = nl.item(i);
                node.getParentNode().removeChild(node);
            }*/



           // transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
           // transformer.setOutputProperty(OutputKeys.INDENT, "yes");



            src = new DOMSource(doc);
            res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);
            System.out.println(invoiceNo+" xml oluşturuldu.");
            
           
            return xmlFile;


        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return ex.getMessage().toString();
        }

    }

    private void setPeymant(Document doc, InvoiceForm invoiceForm) {

        NodeList PaymentMeanss = doc.getElementsByTagName("cac:PaymentMeans");
        int len = PaymentMeanss.getLength();
        for (int i = 0; i < len; i++){
            Node node = PaymentMeanss.item(0);
            NodeList list = node.getChildNodes();
            System.out.println(list.getLength());
            node.getParentNode().removeChild(node);

        }


        Node TaxTotal = doc.getElementsByTagName("cac:PaymentTerms").item(0);
        Text cbcIDTXT = doc.createTextNode(invoiceForm.getIban().replaceAll("\\s", ""));
        Text CurrenvyTXT = doc.createTextNode(invoiceForm.getCurrencyCode());
        Text PaymentNoteTXT = doc.createTextNode(invoiceForm.getBank());
        Text PaymentMeansCodeTXT = doc.createTextNode("60");
        Text PaymentDueDateTxt = doc.createTextNode(Time.getDateString(invoiceForm.getPaymentDay()));

        Element PaymentMeans = doc.createElement("cac:PaymentMeans");
        Element PaymentMeansCode = doc.createElement("cbc:PaymentMeansCode");
        Element PaymentDueDate = doc.createElement("cbc:PaymentDueDate");
        Element PayeeFinancialAccount = doc.createElement("cac:PayeeFinancialAccount");
        Element cbcID = doc.createElement("cbc:ID");
        Element CurrencyCode = doc.createElement("cbc:CurrencyCode");
        Element PaymentNote = doc.createElement("cbc:PaymentNote");

        cbcID.appendChild(cbcIDTXT);
        CurrencyCode.appendChild(CurrenvyTXT);
        PaymentNote.appendChild(PaymentNoteTXT);
        PaymentDueDate.appendChild(PaymentDueDateTxt);
        PaymentMeansCode.appendChild(PaymentMeansCodeTXT);



        PayeeFinancialAccount.appendChild(cbcID);
        PayeeFinancialAccount.appendChild(CurrencyCode);
        PayeeFinancialAccount.appendChild(PaymentNote);

        PaymentMeans.appendChild(PaymentMeansCode);
        PaymentMeans.appendChild(PaymentMeansCode);
        PaymentMeans.appendChild(PaymentDueDate);
        PaymentMeans.appendChild(PayeeFinancialAccount);

        TaxTotal.getParentNode().insertBefore(PaymentMeans,TaxTotal);
    }


    private Document setManKamu(Document doc, InvoiceForm invoiceForm) {

        //TODO first delete if exist

        NodeList BuyerCustomerParties = doc.getElementsByTagName("cac:BuyerCustomerParty");
        int len = BuyerCustomerParties.getLength();
        for (int i = 0; i < len; i++){
            Node node = BuyerCustomerParties.item(0);
            NodeList list = node.getChildNodes();
            System.out.println(list.getLength());
            node.getParentNode().removeChild(node);

        }



        Node PaymentMeans = doc.getElementsByTagName("cac:PaymentMeans").item(0);
        Text VKN = doc.createTextNode(invoiceForm.getSecondTax());
        Element BuyerCustomerParty = doc.createElement("cac:BuyerCustomerParty");
        Element Party = doc.createElement("cac:Party");
        Element PartyIdentification = doc.createElement("cac:PartyIdentification");
        Element ID = doc.createElement("cbc:ID");
        ID.setAttribute("schemeID","VKN");
        ID.appendChild(VKN);
        Element PostalAddress = doc.createElement("cac:PostalAddress");
        Element CitySubdivisionName = doc.createElement("cbc:CitySubdivisionName");
        Element CityName = doc.createElement("cbc:CityName");
        Element Country = doc.createElement("cac:Country");
        Element Name = doc.createElement("cbc:Name");
        Country.appendChild(Name);
        PostalAddress.appendChild(CitySubdivisionName);
        PostalAddress.appendChild(CityName);
        PostalAddress.appendChild(Country);
        PartyIdentification.appendChild(ID);
        Party.appendChild(PartyIdentification);
        Party.appendChild(PostalAddress);
        BuyerCustomerParty.appendChild(Party);




        PaymentMeans.getParentNode().insertBefore(BuyerCustomerParty,PaymentMeans);


        return doc;
    }

    private void setParyName(String RecieverTitle, Document doc) {
        Node partyNode = doc.getElementsByTagName("cac:PartyName").item(2);
        NodeList partyList = partyNode.getChildNodes();
        boolean hasChild = false;
        for (int i = 0; i < partyList.getLength(); i++) {
            Node node = partyList.item(i);
            if ("cbc:Name".equals(node.getNodeName())) {
                node.setTextContent(RecieverTitle);
                hasChild = true;
            }
        }
        if(!hasChild){
            Node name = doc.createElement("cbc:Name");
            name.setTextContent(RecieverTitle);
            partyNode.appendChild(name);
        }
    }


    public String prepareDespatchXml(ResultSet resultSet) {

        try{
              return  createDespatchXmlFile(resultSet.getString("CONTENT")
                        ,resultSet.getString("SENDERVKNTCKN")
                        ,resultSet.getString("RECEIVERVKNTCKN")
                        ,resultSet.getString("RECEIVERTITLE")
                        ,resultSet.getString("DESPATCH_SERIAL_NO"));
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return ex.getMessage().toString();
        }

    }

    public String createDespatchXmlFile(String content,String senderVKNTCKN,String receiverVKNTCKN,String RecieverTitle,String invoiceNo) throws IOException {

        String newContent="";
        String xmlFile = Properties.path+"/Irsaliye/"+invoiceNo+".xml";

        if(!willZip){
            File dir = new File(Properties.path+"/Irsaliye/");
            FileUtils.cleanDirectory(dir);
        }

        try {


            newContent= "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                    "<BISEnvelope type=\"send\">" +
                    "<EnvelopeDetail  senderVKN_TCKN=\"" + senderVKNTCKN+"\" receiverVKN_TCKN=\"" + receiverVKNTCKN + "\">"+
                    "</EnvelopeDetail>"+
                    "<DespatchAdvices>"+
                    content +
                    "</DespatchAdvices>"+
                    "</BISEnvelope>";

            Files.write(Paths.get(xmlFile),newContent.getBytes(StandardCharsets.UTF_8));

            //İmza node Siliniyor
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            Node imza = doc.getElementsByTagName("ext:ExtensionContent").item(0);
            NodeList list = imza.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);

                if ("ds:Signature".equals(node.getNodeName())) {
                    imza.removeChild(node);
                }
            }

            // dosya güncellenir
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            DOMSource src = new DOMSource(doc);
            StreamResult res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);


            //UUID node yeni guid ile güncellenir

            doc = db.parse(xmlFile);
            Node uuiNode = doc.getElementsByTagName("DespatchAdvice").item(0);
            NodeList uuidList = uuiNode.getChildNodes();
            for (int i = 0; i < uuidList.getLength(); i++) {
                Node node = uuidList.item(i);
                if ("cbc:UUID".equals(node.getNodeName())) {
                    node.setTextContent(UUID.randomUUID().toString());
                }
            }

            src = new DOMSource(doc);
            res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);



            //partyName veya Person Name Ekleniyor
         /*   if(receiverVKNTCKN.length()>10){ // TCKN
                //adding peron tag
                doc = db.parse(xmlFile);
                Node partyNode = doc.getElementsByTagName("cac:Person").item(0);
                NodeList partyList = partyNode.getChildNodes();
                for (int i = 0; i < partyList.getLength(); i++) {
                    Node node = partyList.item(i);
                    if ("cbc:FirstName".equals(node.getNodeName())) {
                        node.setTextContent("Hüseyin");
                    }
                    if ("cbc:FamilyName".equals(node.getNodeName())) {
                        node.setTextContent("Altunay");
                    }
                }

            }else{// VKN
                doc = db.parse(xmlFile);
                setParyName(RecieverTitle, doc);
            }*/



            src = new DOMSource(doc);
            res = new StreamResult(new File(xmlFile));
            transformer.transform(src, res);
            System.out.println(invoiceNo+" xml oluşturuldu.");
            return xmlFile;


        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
            return ex.getMessage().toString();
        }

    }
}
