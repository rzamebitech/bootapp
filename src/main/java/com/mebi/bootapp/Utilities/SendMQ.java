package com.mebi.bootapp.Utilities;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class SendMQ {



    public void sendHpEmailQueue(String text) throws JMSException {

         // URL of the JMS server. DEFAULT_BROKER_URL will just mean that JMS server is on localhost
         //
         // default broker URL is : tcp://localhost:61616"


         //String url = "tcp://172.26.36.11:61616";
        String url = "tcp://localhost:61616";
        //String url = "ActiveMQConnection.DEFAULT_BROKER_URL";



         // Queue Name.You can create any/many queue names as per your requirement.


       String queueName = "downloadExcelReport";
        //String queueName = "send-emails";

        System.out.println("url = " + url);
         // Getting JMS connection from the JMS server and starting it


        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin", url);
        Connection connection = connectionFactory.createConnection();
        connection.start();

         // Creating a non transactional session to send/receive JMS message.


        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

         // The queue will be created automatically on the server.


        Destination destination = session.createQueue(queueName);

         // Destination represents here our queue 'MESSAGE_QUEUE' on the JMS server.
         //
         // MessageProducer is used for sending messages to the queue.


        MessageProducer producer = session.createProducer(destination);
        TextMessage message = session.createTextMessage(text);
        message.setStringProperty("envelope-xml",text);

         // Here we are sending our message!


        producer.send(message);

        System.out.println("Message '" + message.getText() + ", Sent Successfully to the Queue");
        connection.close();


    }

}
