package com.mebi.bootapp.Utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

public class KeyPairGeneratorWithJavaSecurity {

    public static void main(String[] args) throws Exception {
        // Anahtar çifti oluşturma için KeyPairGenerator nesnesi oluşturun
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");

        // Anahtar çiftini oluştur
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        // Public key ve private key'i al
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        // Anahtarları kullanabilirsiniz
        System.out.println("Public Key: " + publicKey);
        System.out.println("Private Key: " + privateKey);

        // Anahtarların formatını değiştirmek isterseniz:
        byte[] publicKeyBytes = publicKey.getEncoded();
        byte[] privateKeyBytes = privateKey.getEncoded();
        writeBytesToFile(com.mebi.bootapp.Config.Properties.path+"publicKey.der", publicKeyBytes);
        writeBytesToFile(com.mebi.bootapp.Config.Properties.path+"privateKey.der", privateKeyBytes);

        // Daha sonra bu byte dizilerini istediğiniz formatta kaydedebilirsiniz.
    }

    private static void writeBytesToFile(String fileName, byte[] data) {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            fos.write(data);
            System.out.println(fileName + " dosyasına başarıyla yazıldı.");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Dosya yazma hatası: " + e.getMessage());
        }
    }

}
