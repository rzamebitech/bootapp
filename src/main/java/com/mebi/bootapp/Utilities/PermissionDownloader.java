package com.mebi.bootapp.Utilities;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.sql.*;
import java.util.*;

import static com.mebi.bootapp.Utilities.Log.*;

public class PermissionDownloader {


    private static int COUNT = 0;
    public static String DBURL = null;
    public static String DBYUSER = null;
    public static String DBPASS =null ;
    static Map<String,String> DB = null;
    public static String CUSTOMEROID =null ;
    public static String TAXID =null ;
    public static String MODE =null ;
    public static String CustomerUsersQuery =null ;
    public static String UsersQuery =null ;
    public static String CustomerUsersPermissionsQuery =null ;
    public static boolean Result =false ;
    public static boolean CustomerResult =false ;
    public static String FileName =null ;

    static ResultSet CustomerUserSet = null;


    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, SQLException {



        MODE = "EFATURA"; //EARCHIVE  EFATURA

        TAXID = "4110032901";

        GetAppPropertiesData prop = new GetAppPropertiesData();
        Properties result = prop.GetProp();
        /*if(result!=null){*/
        //DBURL = result.getProperty("efatura.prod.url");
        DBURL = "jdbc:oracle:thin:@172.26.36.40:1521:efatura";
        // DBYUSER = result.getProperty("efatura.prod.username");
        //DBYUSER = "EFATURAPROD";
        DBYUSER = "efaturaprod";
        //DBPASS = result.getProperty("efatura.prod.password");
        //DBPASS = "EFATURAPROD";
        DBPASS = "MNsCsa72";
        DB = new HashMap<>();
        DB.put("DBURL",DBURL);
        DB.put("DBYUSER",DBYUSER);
        DB.put("DBPASS",DBPASS);
        /* }*/




        /* CUSTOMEROID= "''"; //Replace CustomerOid Here - Efaura and Earchive CustomerUsers's OID are different*/ //R

        if(MODE.equals("EFATURA")){ //Create CustomerUser Query
            UsersQuery = getEinvoiceCustomerOidBYTaxID(TAXID);
            ResultSet sqlResult = getSqlResult(DB, UsersQuery);
            while (sqlResult.next()) {
                CUSTOMEROID = sqlResult.getString("OID");
            }
            if(CUSTOMEROID!=null){
                CustomerUsersQuery = " SELECT \"OID\",USERNAME  FROM EFATURAPROD.CUSTOMER_USERS WHERE CUSTOMEROID ="+"'"+CUSTOMEROID+"'";
                CustomerUserSet = getSqlResult(DB,CustomerUsersQuery);

            }else{
                System.out.println("Can not found CustomerOid for Tax: " + TAXID);
            }



        }else if (MODE.equals("EARCHIVE")){
            UsersQuery = getEarchiveCustomerOidBYTaxID(TAXID);
            ResultSet sqlResult = getSqlResult(DB, UsersQuery);
            while (sqlResult.next()) {
                CUSTOMEROID = sqlResult.getString("OID");
            }
            if(CUSTOMEROID!=null){
                CustomerUsersQuery = " SELECT \"OID\",USERNAME  FROM EARCHIVEPROD.CUSTOMER_USER WHERE CUSTOMEROID ="+"'"+CUSTOMEROID+"'";
                CustomerUserSet = getSqlResult(DB,CustomerUsersQuery);

            }else{
                System.out.println("Can not found CustomerOid for Tax: " + TAXID);
            }


        }else{
            return;
        }

        Connection connectionTestDb = DriverManager.getConnection(DBURL, DBYUSER, DBPASS);
        PreparedStatement preparedStatement;
        preparedStatement  = connectionTestDb.prepareStatement(CustomerUsersQuery);
        CustomerUserSet = preparedStatement.executeQuery();
        Map<String,String> customers = new HashMap<>();






        CustomerResult = false;
        while (CustomerUserSet.next()) {
            customers.put(CustomerUserSet.getString("OID"),CustomerUserSet.getString("USERNAME"));
            CustomerResult = true;

        }
        if(!CustomerResult){
            System.out.println("\n" +ANSI_RED+"CustomerUser Not found in " + MODE+ " Table for CustomerOid: " + CUSTOMEROID +ANSI_RESET);
            return;
        }

        List<List<Map<String, String>>> name = new ArrayList<>();
        System.out.println(ANSI_CYAN+"Start Create Excel Permissions For Customer By TaxID : " + TAXID+ANSI_RESET);

        for (Map.Entry<String, String> customer : customers.entrySet()) {


            if(MODE.equals("EFATURA")){
                CustomerUsersPermissionsQuery = getInvoicePermissionsQuery(customer.getKey());
            }else if (MODE.equals("EARCHIVE")){
                CustomerUsersPermissionsQuery = getArchivePermissionsQuery(customer.getKey());
            }else{
                return;
            }

            Result = false;
            preparedStatement  = connectionTestDb.prepareStatement(CustomerUsersPermissionsQuery);
            ResultSet PermissionSet = preparedStatement.executeQuery();
            List<Map<String, String>> listOfMaps = new ArrayList<Map<String, String>>();

            while (PermissionSet.next()) {
                Map<String,String> map = new HashMap<String,String>();
                map.put("USERNAME",PermissionSet.getString(1));
                map.put("PERNAME",PermissionSet.getString(2));
                map.put("DESCRIPTION",PermissionSet.getString(3));
                map.put("ROLENAME",PermissionSet.getString(4));

                listOfMaps.add(map);
                Result = true;


            }
            if(!Result){
                Map<String,String> map = new HashMap<String,String>();
                map.put("USERNAME",customer.getValue());
                listOfMaps.add(map);
            }
            name.add(listOfMaps);


        }
        int count = 0;
        String Directory = "C:/Helper-output/PERMISSIONS/"+TAXID.replaceAll("\'","")+"/"+MODE+"/";

        for (List<Map<String, String>> maps : name) {
            count++;
            CreateExlFile(maps,Directory);

        }
        System.out.println(ANSI_GREEN+ count +" Excel file(s) has been transferred to " + Directory+ ANSI_RESET);



    }


    public static void CreateExlFile(List<Map<String, String>> maps, String Directory){





        FileName = maps.get(0).get("USERNAME");


        File directory = new File(Directory);
        if (! directory.exists()){
            directory.mkdirs();

        }

        String Foldername = Directory+FileName+".xls" ;
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("PERMISSIONS");

        COUNT = 1;

        HSSFRow rowhead = sheet.createRow((short)0);
        rowhead.createCell(0).setCellValue("USERNAME");
        rowhead.createCell(1).setCellValue("PERMISSION_NAME");
        rowhead.createCell(2).setCellValue("DESCRIPTION");
        rowhead.createCell(3).setCellValue("ROLE_NAME");

        for (Map<String, String> map : maps) {

            HSSFRow rowbody = sheet.createRow((short)COUNT);

            if(map.get("USERNAME")!=null){
                rowbody.createCell(0).setCellValue(map.get("USERNAME"));
            }
            if(map.get("PERNAME")!=null){
                rowbody.createCell(1).setCellValue(map.get("PERNAME"));
            }
            if(map.get("DESCRIPTION")!=null){
                rowbody.createCell(2).setCellValue(map.get("DESCRIPTION"));
            }
            if(map.get("ROLENAME")!=null){
                rowbody.createCell(3).setCellValue(map.get("ROLENAME"));
            }
            COUNT++;
        }


        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(Foldername);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            workbook.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    };

    public static ResultSet getSqlResult( Map<String,String> DB,String Query) {
        if(DB!=null){
            try {
                Connection connectionTestDb = DriverManager.getConnection(DB.get("DBURL"),DB.get("DBYUSER") , DB.get("DBPASS"));
                PreparedStatement preparedStatement;
                preparedStatement  = connectionTestDb.prepareStatement(Query);
                return preparedStatement.executeQuery();
            }catch (Exception e){
                System.out.println(e.getMessage());
                return null;
            }

        }
        return null;
    }

    public static String getInvoicePermissionsQuery(String oid ){
        String query = " SELECT cu.USERNAME , p.NAME , p.DESCRIPTION  , r.NAME   FROM EFATURAPROD.CUSTOMER_USER_ROLE cur"+
                " INNER JOIN EFATURAPROD.CUSTOMER_USERS cu ON cu.OID = cur.CUSTOMER_USER_OID"+
                " INNER JOIN EFATURAPROD.ROLE_PERMISSION rp ON  rp.ROLE_OID = cur.ROLE_OID"+
                " INNER JOIN EFATURAPROD.ROLE r ON r.OID = cur.ROLE_OID"+
                " INNER JOIN EFATURAPROD.PERMISSION p ON p.OID = rp.PERMISSION_OID"+
                " WHERE  CUSTOMER_USER_OID = "+"'"+oid+"'";

        return query;
    }
    public static String getEinvoiceCustomerOidBYTaxID(String TaxId ){
        String query = "SELECT c.\"OID\"  FROM EFATURAPROD.CUSTOMERS c"+
                " WHERE c.TAXID  = "+"'"+TaxId+"'";
        return query;
    }

    public static String getEarchiveCustomerOidBYTaxID(String TaxId ){
        String query = "SELECT c.\"OID\"  FROM EARCHIVEPROD.CUSTOMER c"+
                " WHERE c.TAXID  = "+"'"+TaxId+"'";
        return query;
    }

    public static String getArchivePermissionsQuery(String oid ){
        String query = " SELECT cu.USERNAME , p.NAME , p.DESCRIPTION  , r.NAME   FROM EARCHIVEPROD.CUSTOMER_USER_ROLE cur"+
                " INNER JOIN EARCHIVEPROD.CUSTOMER_USER cu ON cu.OID = cur.CUSTOMER_USER_OID"+
                " INNER JOIN EARCHIVEPROD.ROLE_PERMISSION rp ON  rp.ROLE_OID = cur.ROLES_OID"+
                " INNER JOIN EARCHIVEPROD.ROLE r ON r.OID = cur.ROLES_OID"+
                " INNER JOIN EARCHIVEPROD.PERMISSION p ON p.OID = rp.PERMISSION_OID"+
                " WHERE  CUSTOMER_USER_OID = "+"'"+oid+"'";

        return query;
    }

  /* JSONObject json = readJsonFromUrl("https://ecalismaizni.csgb.gov.tr/api/izinSorgula/basvuruDTO?basvuruSecimi=0&yabanciKimlikNo=99467654178&belgeNo=3098020");
        //System.out.println(json.toString());
        System.out.println("Durum Kodu: " + json.get("durum"));
        System.out.println("Son Durum: " + json.get("izingecerlilikdurum"));*/

    /*public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }*/
}

