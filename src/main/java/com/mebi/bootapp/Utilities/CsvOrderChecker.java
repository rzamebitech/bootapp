package com.mebi.bootapp.Utilities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CsvOrderChecker {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        //checkCsv();

        //  checkedleger();

        accountMainID();


    }

    private static void checkedleger() throws ParserConfigurationException, IOException, SAXException {
        File folder = new File("D:/xml-temp/csvChecker/");
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles!=null){
            for (File file : listOfFiles) {

                // XML dosyasını okuyan bir DocumentBuilder nesnesi oluşturun
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();

                // XML dosyasını okuyun ve bir Document nesnesi oluşturun
                Document doc = builder.parse(new File(file.getPath()));

                String nVal = "0";
                // Tüm <gl-cor:entryDetail> etiketlerini seçin
                NodeList entryDetails = doc.getElementsByTagName("gl-cor:entryDetail");

                // Her bir <gl-cor:entryDetail> etiketi için döngü oluşturun
                for (int i = 0; i < entryDetails.getLength(); i++) {
                    Element entryDetail = (Element) entryDetails.item(i);

                    // <gl-cor:lineNumber> etiketini seçin ve değerini alın
                    NodeList lineNumbers = entryDetail.getElementsByTagName("gl-cor:lineNumber");
                    String lineNumber = lineNumbers.item(0).getTextContent();

                    if(Integer.parseInt(lineNumber)>=Integer.parseInt(nVal)){
                        nVal = lineNumber;
                    }else{

                        System.out.println(lineNumber);

                    }

                    // lineNumber değerini kullanarak yapılacak işlemleri burada gerçekleştirin

                }


            }

        }
    }

    private static void checkCsv() {
        try {
            File folder = new File("D:/xml-temp/csvChecker/");
            File[] listOfFiles = folder.listFiles();
            if(listOfFiles!=null){
                for (File file : listOfFiles) {
                    InputStream targetStream = new FileInputStream(file);

                    String line = "";
                    int lineNumber = 0;
                    String nVal = "0";



                    BufferedReader reader = new BufferedReader(new InputStreamReader(targetStream));
                    while ((line = reader.readLine()) != null) {
                        lineNumber++;
                        String[] values = line.split("\\t");
                        String yevmiye = values[11];
                        if(Integer.parseInt(yevmiye)>=Integer.parseInt(nVal)){
                            nVal = yevmiye;
                        }else{

                            System.out.println(lineNumber);

                        }

                    }


                }


            }else{
                System.out.println("Can not find any file in path: " + folder.getAbsolutePath());
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static void accountMainID() {

        try {

            File folder = new File("D:/xml-temp/csvChecker/");
            File[] listOfFiles = folder.listFiles();
            StringBuilder builder = new StringBuilder();
            List<Integer> lst = new ArrayList<>();


            if(listOfFiles!=null){
                for (File file : listOfFiles) {
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(file);
                    doc.getDocumentElement().normalize();
                    NodeList entryHeadernodeList = doc.getElementsByTagName("gl-cor:entryHeader");
                    for (int i = 0; i < entryHeadernodeList.getLength(); i++) {
                        NodeList nodeList = doc.getElementsByTagName("gl-cor:entryDetail");
                        Node node = nodeList.item(0);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;
                            NodeList accountMainIDList = element.getElementsByTagName("gl-cor:accountMainID");
                            Element accountMainIDElement = (Element) accountMainIDList.item(0);
                            String accountMainID = accountMainIDElement.getTextContent();
                            lst.add(Integer.parseInt(accountMainID));

                        }

                    }


                }

                int oldval= 0;

                for (Integer integer: lst) {

                    if(integer<oldval){
                        System.out.println(integer);
                    }else{
                        oldval= integer;
                    }

                }





            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }



    }

}
