package com.mebi.bootapp.Utilities;
import org.bouncycastle.openssl.PEMEncryptor;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcePEMEncryptorBuilder;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;

public class GenerateKeyPairWithBouncyCastle {
    public static void main(String[] args) throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        // Create a key pair
        KeyPair keyPair = generateKeyPair();

        // Export private and public key in PEM format
        exportPrivateKey(keyPair,  com.mebi.bootapp.Config.Properties.path+"private_key.pem", "AS2MEBITECH2023!");
        exportPublicKey(keyPair,  com.mebi.bootapp.Config.Properties.path+"public_key.pem");
    }

    private static KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");
        generator.initialize(2048);
        return generator.generateKeyPair();
    }

    private static void exportPrivateKey(KeyPair keyPair, String fileName, String password) throws Exception {
        PEMEncryptor encryptor = new JcePEMEncryptorBuilder("AES-256-CFB")
                .setProvider("BC")
                .build(password.toCharArray());

        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName);
             OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
             JcaPEMWriter pemWriter = new JcaPEMWriter(writer)) {

            pemWriter.writeObject(keyPair.getPrivate(), encryptor);
            System.out.println("Keys export to your path: " + fileName);
        }
    }
    private static void exportPublicKey(KeyPair keyPair, String fileName) throws Exception {
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName);
             OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
             JcaPEMWriter pemWriter = new JcaPEMWriter(writer)) {

            pemWriter.writeObject(keyPair.getPublic());
        }
    }
}