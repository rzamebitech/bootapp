package com.mebi.bootapp.Utilities;

import java.io.FileInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class CustomCertificateLoader {

    public static X509Certificate loadCertificate(FileInputStream certificateFile) throws Exception {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509"); // Sertifika tipi
        return (X509Certificate) certificateFactory.generateCertificate(certificateFile);
    }
}
