package com.mebi.bootapp.Utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetAppPropertiesData {

    public Properties GetProp(){
        try (
                InputStream input = GetAppPropertiesData.class.getClassLoader().getResourceAsStream("application.properties")) {

            Properties prop = new Properties();

            if (input == null) {
                System.out.println("Sorry, unable to find config.properties");
                return null;
            }

            //load a properties file from class path, inside static method
            prop.load(input);

            return prop;

          /*  //get the property value and print it out
            System.out.println(prop.getProperty("db.url"));
            System.out.println(prop.getProperty("db.user"));
            System.out.println(prop.getProperty("db.password"));*/

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }



}
