package com.mebi.bootapp.Utilities;

import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

public class CustomKeyPairLoader {
    public static KeyPair loadPrivateKey(FileInputStream privateKeyFile) throws Exception {
        byte[] privateKeyBytes = new byte[privateKeyFile.available()];
        privateKeyFile.read(privateKeyBytes);

        // Özel anahtarın PKCS#8 formatından dönüştürülmesi
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA"); // Kullanılan algoritma
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);

        return new KeyPair(null, privateKey); // Genel anahtar yok, sadece özel anahtar
    }
}
