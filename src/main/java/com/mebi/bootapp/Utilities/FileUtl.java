package com.mebi.bootapp.Utilities;

public class FileUtl {

    public static String formatFileSize(long fileSize) {
        double fileSizeKB = (double) fileSize / 1024;
        if (fileSizeKB < 1) {
            return fileSize + " B";
        }
        double fileSizeMB = fileSizeKB / 1024;
        if (fileSizeMB < 1) {
            return String.format("%.2f KB", fileSizeKB);
        }
        return String.format("%.2f MB", fileSizeMB);
    }
}
