package com.mebi.bootapp.Utilities.minIO;

public class MinIOConf {

    private String entPoint;

    private int port;

    private String accessKey;

    private String secretKey;

    public String getEntPoint() {
        return entPoint;
    }

    public void setEntPoint(String entPoint) {
        this.entPoint = entPoint;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }



}
