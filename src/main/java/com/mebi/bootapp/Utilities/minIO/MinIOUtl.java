package com.mebi.bootapp.Utilities.minIO;

import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.errors.*;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class MinIOUtl {

    private final static Logger LOGGER = LoggerFactory.getLogger(MinIOUtl.class);


    public static MinioClient getMinIOConnection(MinIOConf minIOConf) throws InvalidPortException, InvalidEndpointException {
        return new MinioClient(minIOConf.getEntPoint(),minIOConf.getPort(), minIOConf.getAccessKey(), minIOConf.getSecretKey());
    }

    public static boolean checkMinioConnection(MinioClient minioClient,String bucketName) throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InvalidResponseException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException, RegionConflictException {
        try {
            return minioClient.bucketExists(bucketName);
        }catch (Exception e){
            return false;
        }
    }

    public static InputStream downloadData(MinioClient minioClient, String bucketName, String objectName, String objectPath) throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, InvalidArgumentException, InvalidResponseException, InternalException, NoResponseException, InvalidBucketNameException, InsufficientDataException, ErrorResponseException {
        return minioClient.getObject(bucketName, getMinIOObjectName(objectName,objectPath));
    }

    public static ObjectStat statObject(MinioClient minioClient, String bucketName, String objectName, String objectPath) throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, InvalidArgumentException, InvalidResponseException, InternalException, NoResponseException, InvalidBucketNameException, InsufficientDataException, ErrorResponseException {
        return minioClient.statObject(bucketName, getMinIOObjectName(objectName,objectPath));
    }

    public static InputStream getMinIOFile(String fileName , String filePath , String customerOid , MinioClient minioClient,String bucketName) throws IOException, InvalidKeyException, NoSuchAlgorithmException, XmlPullParserException, InvalidArgumentException, InvalidResponseException, ErrorResponseException, NoResponseException, InvalidBucketNameException, InsufficientDataException, InternalException {
        InputStream is = null;
        try {
            is = downloadData(minioClient,bucketName,fileName,filePath);
        } catch (Exception e) {
            LOGGER.error("MinIO reader has Error. CustomerTaxId:" + customerOid + " \n" +
                    "Error: "+ e.getMessage());
            throw e;
        }
        if (!isEmpty(is)) {
            LOGGER.info("Found file on Aras MinIO ! ... " + fileName );
            return is;
        }
        return null;
    }

    public static String getMinIOObjectName(String objectName,String objectPath){
        if(!isEmpty(objectPath)){
            if (!objectPath.endsWith("/") && !objectPath.endsWith("\\")) {
                objectPath = objectPath +"/";
            }
            objectName = objectPath +objectName;
        }

        objectName = FilenameUtils.separatorsToUnix(objectName);
        if(objectName.startsWith("/")){
            objectName = objectName.replaceFirst("/","");
        }

        return objectName;
    }

    public static boolean isEmpty(Object object){
        return object==null;
    }

}
