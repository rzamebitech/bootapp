package com.mebi.bootapp.Utilities.Sign;

import org.apache.activemq.util.ByteArrayOutputStream;

import java.io.FileInputStream;
import java.io.InputStream;

public class FileReader {
	
	public static byte[] getFileContent(String fileName){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			
			int BUFFER_SIZE = 4096;
			byte[] buffer = new byte[BUFFER_SIZE];
			InputStream input = new FileInputStream(fileName);
			int n = input.read(buffer, 0, BUFFER_SIZE);
			while (n >= 0) {
			    bos.write(buffer, 0, n);
			    n = input.read(buffer, 0, BUFFER_SIZE);
			}
			input.close();
			bos.close();

		}catch (Exception e) {
			e.printStackTrace();
		}
		

		return bos.toByteArray();
	}
}
