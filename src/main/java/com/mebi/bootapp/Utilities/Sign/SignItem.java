package com.mebi.bootapp.Utilities.Sign;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by kamilbukum on 11/04/16.
 */
public class SignItem {

    private String signType;
    private String signTag;
    private byte[] content;
    private Map<String, String> extraParameters = new LinkedHashMap<>();

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getSignTag() {
        return signTag;
    }

    public void setSignTag(String signTag) {
        this.signTag = signTag;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public void setExtraParameters(Map<String, String> extraParameters) {
        this.extraParameters = extraParameters;
    }

    public Map<String, String> getExtraParameters() {
        return extraParameters;
    }
}
