package com.mebi.bootapp.Utilities.Sign;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ArrayUtils;


import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by kamilbukum on 16/10/14.
 */
public class FileByteUtil {
    private  static int headerOffset=4;
    private static ObjectMapper objectMapper=new ObjectMapper();
    public  static FilePackage bytesToPackage(byte[] output){
        FilePackage filePackage=new FilePackage();
        try{

            // Header Size
            int headerLength;
            try{
                if(output==null||output.length<headerOffset){
                    filePackage.setBody("Data Header Size is not  Avaliable !".getBytes("UTF-8"));
                    filePackage.setState(DataState.ILLEGAL_HEADER_SIZE);
                    return filePackage;
                }
                headerLength= ByteBuffer.wrap(output, 0, headerOffset).getInt();
            }catch (Exception e){
                filePackage.setBody(("Data Header Size is not  compatible !"+e.getMessage()).getBytes("UTF-8"));
                e.printStackTrace();
                e.printStackTrace();
                filePackage.setState(DataState.ILLEGAL_HEADER_SIZE);
                return filePackage;
            }

            // Header
            if(headerLength>0){
                if(output.length<(headerOffset+headerLength)){
                    filePackage.setBody("Data Header is not  Avaliable !".getBytes("UTF-8"));
                    filePackage.setState(DataState.ILLEGAL_HEADER);
                    return filePackage;
                }
                try{
                    byte[] headerBytes= ArrayUtils.subarray(output, headerOffset, headerOffset + headerLength);
                    FilePackage.Header header=objectMapper.readValue(headerBytes, FilePackage.Header.class);
                    filePackage.setHeader(header);
                }catch (Exception e){
                    filePackage.setBody(("Data Header is not  compatible ! Detail : " + e.getMessage()).getBytes());
                    e.printStackTrace();
                    filePackage.setState(DataState.ILLEGAL_HEADER);
                    return filePackage;
                }
            }

            int dataOffset=headerOffset+headerLength;
            int dataLength=output.length-dataOffset;
            // Body

            if(dataLength>=0){
                try{
                    byte[] dataBytes=ArrayUtils.subarray(output, dataOffset, output.length);
                    filePackage.setBody(dataBytes);
                }catch (Exception e){
                    filePackage.setBody(("Data is not  compatible ! Detail : "+e.getMessage()).getBytes());
                    e.printStackTrace();
                    filePackage.setState(DataState.ILLEGAL_DATA);
                    return filePackage;
                }
            }
            filePackage.setState(DataState.DATA_OK);
        }catch (Exception e){
            filePackage.setBody(e.getMessage().getBytes());
            e.printStackTrace();
            filePackage.setState(DataState.ERROR);
        }
        return filePackage;
    }
    public  static FilePackage inputStreamToPackage(InputStream inputStream) throws IOException {

        DataInputStream dIn = new DataInputStream(inputStream);

        int length = dIn.readInt();                    // read length of incoming message
        if(length>0) {
            byte[] message = new byte[length];
            dIn.readFully(message, 0, message.length); // read the message
            return bytesToPackage(message);
        }
        return null;
    }
    //byte[] to Byte[]
    public static Byte[] toObjects(byte[] bytesPrim) {
        Byte[] bytes = new Byte[bytesPrim.length];
        int i = 0;
        for (byte b : bytesPrim) bytes[i++] = b; //Autoboxing
        return bytes;

    }
    public static  byte[] filePackageToByte(FilePackage filePackage) throws Exception{
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        objectMapper.writeValue(outputStream,filePackage.getHeader());
        byte[] header=outputStream.toByteArray();
        int headerSize=header.length;
        byte[] headerSizeBytes= ByteBuffer.allocate(headerOffset).putInt(headerSize).array();
        return ArrayUtils.addAll(ArrayUtils.addAll(headerSizeBytes,header),filePackage.getBody());
    }

    public static class FilePackage{
        private DataState state=DataState.ILLEGAL_HEADER_SIZE;
        private Header header;
        private byte [] body;

        public void setState(DataState state) {
            this.state = state;
        }

        public DataState getState() {
            return state;
        }

        public void setHeader(Header header) {
            this.header = header;
        }

        public Header getHeader() {
            return header;
        }

        public void setBody(byte[] body) {
            this.body = body;
        }

        public byte[] getBody() {
            return body;
        }

        public static class Header{
            private boolean success;
            private String message;
            private String signType;
            private String tagName;

            public boolean isSuccess() {
                return success;
            }

            public String getMessage() {
                return message;
            }

            public void setSuccess(boolean success) {
                this.success = success;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public void setSignType(String signType) {
                this.signType = signType;
            }

            public String getSignType() {
                return signType;
            }

            public void setTagName(String tagName) {
                this.tagName = tagName;
            }

            public String getTagName() {
                return tagName;
            }
        }
    }


    /*
    public static void main(String [] args){
        FilePackage filePackage=new FilePackage();
        try {
            filePackage.setHeader("Merhaba     ".getBytes("UTF-8"));
            byte[] result=filePackageToByte(filePackage);
            ByteArrayInputStream inputStream=new ByteArrayInputStream(result);
            FilePackage aPackage=FileByteUtil.byteToHeader(inputStream);

            System.out.println(aPackage.getState().toString());
            System.out.println(new String(aPackage.getHeader()));
            System.out.println(new String(aPackage.getBody()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
    */
}
