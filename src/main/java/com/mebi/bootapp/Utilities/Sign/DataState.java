package com.mebi.bootapp.Utilities.Sign;

/**
 * Created by kamilbukum on 16/10/14.
 */
public enum DataState {
    UNKNOWN(-100),ILLEGAL_HEADER_SIZE(-3),ILLEGAL_HEADER(-2),ILLEGAL_DATA(-1),ERROR(0),DATA_OK(1);
    private int value;
    private DataState(int value){
        this.value=value;
    }
}
