package com.mebi.bootapp.Utilities.Sign;

/**
 * Created by kamilbukum on 16/10/14.
 */
//java socket client example

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {

    private String url;
    private int port;

    public SocketClient(String url, int port) {
        this.url = url;
        this.port = port;
    }

    public byte[] sendData(byte[] bytes) throws Exception{
        Socket socket = null;
        try {
            InetAddress address = InetAddress.getByName(url);

            try{
            socket = new Socket(address, port);
            }catch (IOException ex){
                throw new IOException("Didn't open socket on "+address+":"+port+" Error Detail :"+ ex.getLocalizedMessage());
            }
            //Send the message to the server
            DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());

            try{
                dOut.writeInt(bytes.length); // write length of the message
                dOut.write(bytes);
            }catch (Exception ex){
               throw new IOException("Didn't write data on socket ! Socket Detail :"+address+":"+port+" Error Detail :"+ ex.getLocalizedMessage());
            }
                      // write the message

            try{
                DataInputStream dIn = new DataInputStream(socket.getInputStream());
                int length = dIn.readInt();                    // read length of incoming message
                if(length>0) {
                    byte[] message = new byte[length];
                    dIn.readFully(message, 0, message.length); // read the message
                    return message;
                }else{
                   throw new RuntimeException("Data is null ! ");
                }
            }catch (IOException ex){
                throw new IOException("Didn't read data on socket ! Socket Detail :"+address+":"+port);
            }
        }catch (UnknownHostException ex){
            throw new RuntimeException("Unknown Host ! Host Detail :"+url);
        }catch (IOException ex){
            throw ex;
        }
        finally {
            try {
                if(socket!=null){
                socket.close();
                }
            } catch (Exception e) {
                throw  e;
            }
        }
    }

}
