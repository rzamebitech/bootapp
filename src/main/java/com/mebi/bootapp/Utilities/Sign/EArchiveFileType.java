package com.mebi.bootapp.Utilities.Sign;

public enum EArchiveFileType {
    EARCHIVE_INVOICE,EARCHIVE_REPORT;
    public static final String TYPE_FIELD_NAME="TYPE";
    public static final String FILE_FIELD_NAME="FILE";
    public static final String SIGN_TAG_NAME="SIGN_TAG_NAME";
}
