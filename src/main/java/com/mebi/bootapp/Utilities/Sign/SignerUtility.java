package com.mebi.bootapp.Utilities.Sign;

import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import javax.crypto.Cipher;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Collection;

public class SignerUtility {

    public static byte[] signDataWithJavaSecurity(byte[] data, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance("SHA256withRSA"); // İmza algoritması seçimi
        signature.initSign(privateKey);
        signature.update(data);


        return signature.sign();
    }


    public byte[] encryptFile(byte[] fileData, Key encryptionKey) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // Şifreleme algoritması ve modu
        cipher.init(Cipher.ENCRYPT_MODE, encryptionKey);

        byte[] encryptedData = cipher.doFinal(fileData);
        return encryptedData;
    }


    public static boolean verifySignature(byte[] signedData, X509Certificate certificate, KeyPair keyPair) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        // İmza doğrulama işlemi
        CMSSignedData cmsSignedData = new CMSSignedData(signedData);

        SignerInformationStore signerInfos = cmsSignedData.getSignerInfos();
        Collection<SignerInformation> signers = signerInfos.getSigners();

        for (SignerInformation signerInfo : signers) {
            if (signerInfo.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(certificate))) {
                 return true;
            }
        }

        // İmza doğrulanamadı
        return false;
    }


}
