package com.mebi.bootapp.Utilities.Sign;

/**
 * Created by kamilbukum on 05/06/14.
 */
public enum ErrorCode {
    NULLPOINTER,
    MSUCCESS,
    MEMPTY,
    DOMPARSER,
    MJAX,
    MERROR;
}
