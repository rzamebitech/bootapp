package com.mebi.bootapp.Utilities.Sign;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import tr.gov.tubitak.uekae.esya.api.asn.x509.ECertificate;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.CertificateStatus;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.CertificateValidation;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.ValidationSystem;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.check.certificate.CertificateStatusInfo;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.PolicyReader;
import tr.gov.tubitak.uekae.esya.api.certificate.validation.policy.ValidationPolicy;
import tr.gov.tubitak.uekae.esya.api.common.ESYAException;
import tr.gov.tubitak.uekae.esya.api.common.crypto.Algorithms;
import tr.gov.tubitak.uekae.esya.api.common.crypto.BaseSigner;
import tr.gov.tubitak.uekae.esya.api.common.util.LicenseUtil;
import tr.gov.tubitak.uekae.esya.api.crypto.alg.DigestAlg;
import tr.gov.tubitak.uekae.esya.api.smartcard.pkcs11.CardType;
import tr.gov.tubitak.uekae.esya.api.smartcard.pkcs11.P11SmartCard;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.*;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.config.Config;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.document.InMemoryDocument;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.Reference;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.Transform;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.Transforms;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.keyinfo.KeyValue;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.xades.ClaimedRole;
import tr.gov.tubitak.uekae.esya.api.xmlsignature.model.xades.SignerRole;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

public class XMLSigner {




	private final static Logger logger = LoggerFactory.getLogger(XMLSigner.class);

	static {
		try {
			LicenseUtil.setLicenseXml(XMLSigner.class
					.getResourceAsStream("/lisans.xml"));
		} catch (ESYAException e) {
			e.printStackTrace();
		}
	}

	private final KeyStore keyStore;


	public XMLSigner(KeyStore keyStore){
		
		this.keyStore = keyStore;
	}
	
	public String signFile(byte[] fileContent,String operationMode) throws Exception {
		return signFile(fileContent, null, null, null, operationMode, "46869609282651447");
	}

	public String signFile(byte[] fileContent,String operationMode,String serialNumber) throws Exception {
		return signFile(fileContent, null, null, null,operationMode,serialNumber);
	}
	
	private String signFile(byte[] fileContent, XMLSignature signature,
			Context context, Document envelopeDoc,String operationMode,String serialNumber) throws Exception {
		
		if (envelopeDoc == null) {
			envelopeDoc = newEnvelope(fileContent);
		}

		if (context == null) {
			context = new Context();
			context.setConfig(new Config(XMLSigner.class
					.getResourceAsStream("/xmlsignature-config.xml")));
		}

		context.setDocument(envelopeDoc);

		if (signature == null) {
			signature = new XMLSignature(context, false);
		}

		NodeList l = envelopeDoc
				.getElementsByTagNameNS(
						"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2",
						"ExtensionContent");

		if (l.item(0) == null) {
			l = envelopeDoc.getElementsByTagNameNS(
					"http://www.openapplications.org/oagis/9", "Signature");
		}

		l.item(0).appendChild(signature.getElement());

		Transforms transforms = new Transforms(context);
		Transform transform = new Transform(context,
				TransformType.ENVELOPED.getUrl());
		transforms.addTransform(transform);

		Reference asd = new Reference(context);
		asd.setURI("");
		asd.setDigestMethod(DigestMethod.SHA_256);
		asd.setTransforms(transforms);

		NodeList uri = envelopeDoc
				.getElementsByTagNameNS(
						"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2",
						"URI");

		String uriStr = "";
		if (uri.item(0) != null) {
			uriStr = uri.item(0).getTextContent().replace("#", "");
		} else {
			uriStr = "Signature_" + UUID.randomUUID().toString();
		}

		signature.setId(uriStr);
		signature.getSignedInfo().setSignatureMethod(SignatureMethod.RSA_SHA256);
		signature.getSignedInfo().addReference(asd);

		signature.setSigningTime(Calendar.getInstance());

		SignerRole rol = new SignerRole(context, new ClaimedRole[]{new ClaimedRole(context, "Supplier")});
		signature.getQualifyingProperties().getSignedSignatureProperties().setSignerRole(rol);
		
		XMLGregorianCalendar calendar = DatatypeFactory.newInstance()
		        .newXMLGregorianCalendar(new GregorianCalendar());
		signature.getQualifyingProperties().getSignedSignatureProperties().setSigningTime(calendar);
		
		try {
			
			/*
			 * HSM SIGNER
			 */



			P11SmartCard p11SmartCard = new P11SmartCard(CardType.NCIPHER);
			long[] slots = p11SmartCard.getSmartCard().getSlotList();

			p11SmartCard.openSession(slots[0]);
			p11SmartCard.login("123456");
			List<byte[]> certs = p11SmartCard.getSignatureCertificates();

			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = null;
			boolean certFound = false;
			for(int i = 0; i < certs.size(); i++) {
			 	cert = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(certs.get(i)));
				if (cert.getSerialNumber().equals(new BigInteger(serialNumber))){
					certFound = true;
					break;
				}
			}

			if (!certFound){
				throw new Exception("Certificate error");
			}
			
			signature.addKeyInfo(new ECertificate(cert.getEncoded()));
			signature.getKeyInfo().add(new KeyValue(context, cert.getPublicKey()));

			BaseSigner signer = p11SmartCard.getSigner(cert, Algorithms.SIGNATURE_RSA_SHA256);
			logger.info("İmzalama işlemi Başlatılıyor...");
			signature.sign(signer);
			logger.info("İmza Sertifika Bilgileri: " + "\n" +
					"Serial Number: " + cert.getSerialNumber() +
					"Başlangıç / Bitiş Tarihleri: " + cert.getNotBefore() + " - " + cert.getNotAfter());
			
			/*
			 * DONGLE SIGNER
			 */
			
//			APDUSmartCard sc = new APDUSmartCard();
//			long[] slots = sc.getSlotList();
//			sc.openSession(slots[0]);
//			sc.login("500367");
//			List<byte []> certs = sc.getSignatureCertificates();
//			CertificateFactory cf = CertificateFactory.getInstance("X.509"); 
//			X509Certificate cert = (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(certs.get(0))); 
//			signature.addKeyInfo(new ECertificate(cert.getEncoded()));
//			BaseSigner signer = sc.getSigner(cert, Algorithms.SIGNATURE_RSA_SHA1);
//			signature.sign(signer);

		} catch (Exception e){
			logger.error("HSM Error" , e);
			if ("test".equals(operationMode)){
				logger.error("Test environment signing with keystore" , e);
				try {

					String alias = keyStore.aliases().nextElement();
					PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias,
							"123456".toCharArray());
					Certificate cert = keyStore.getCertificate(alias);
					signature.addKeyInfo(new ECertificate(cert.getEncoded()));
					signature.getKeyInfo().add(new KeyValue(context, cert.getPublicKey()));
					signature.sign(privateKey);
				}catch(Exception e2){logger.error("Error signing with keystore", e2);}
			}
		}

		Source source = new DOMSource(envelopeDoc);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();

		trans.setOutputProperty("method", "xml");
		trans.setOutputProperty("omit-xml-declaration", "yes");

		Writer writer = new StringWriter();
		trans.transform(source, new StreamResult(writer));

		return writer.toString();
	}
	private static final String POLICY_FILE_MM = "/certval-policy-malimuhur.xml";

	public static boolean validateSignature(String document)
			throws Exception {
		Context context = new Context();
		context.setConfig(new Config(XMLSigner.class
				.getResourceAsStream("/xmlsignature-config.xml")));

		XMLSignature signature=null;
		try {
			 signature = XMLSignature.parse(new InMemoryDocument(document.getBytes("UTF-8"), null, null, "UTF-8"), context);
		}catch (Exception e){
			logger.error("couldn't parse given Envelope-XML with UTF-8 ! e: "+e.getLocalizedMessage());
			try {
				signature = XMLSignature.parse(new InMemoryDocument(document.getBytes(), null, "application/xml", "UTF-8"), context);
			}catch (Exception ex){
				logger.error("couldn't parse given Envelope-XML without UTF-8 ! ex: "+ex.getLocalizedMessage());
				return false;
			}
		}

		boolean isMaliMuhur = signature.getSigningCertificate()
				.isMaliMuhurCertificate();

		ValidationPolicy validationPolicy;
		if (isMaliMuhur) {
			validationPolicy = PolicyReader
					.readValidationPolicy(XMLSigner.class
							.getResourceAsStream(POLICY_FILE_MM));
		} else {
			validationPolicy = PolicyReader
					.readValidationPolicy(XMLSigner.class
							.getResourceAsStream("/certval-policy-malimuhur.xml"));
		}
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("dizin", System.getProperty("cert.dir"));
		validationPolicy.bulmaPolitikasiAl().addTrustedCertificateFinder("tr.gov.tubitak.uekae.esya.api.certificate.validation.find.certificate.trusted.TrustedCertificateFinderFromFileSystem", params);

		context.getConfig().getValidationConfig()
				.setCertificateValidationPolicy(validationPolicy);

		ValidationSystem vs = CertificateValidation
				.createValidationSystem(validationPolicy);
		vs.setBaseValidationTime(Calendar.getInstance());
		CertificateStatusInfo csi = CertificateValidation.validateCertificate(
				vs, signature.getSigningCertificate());

		DigestAlg digestAlg = signature.getSignatureMethod().getDigestAlg();


		String documentType = getDocumentType(signature.getDocument());
		if (documentType==null){
			return false;
		} else if (digestAlg.equals(DigestAlg.MD5)) {
			return false;
		} else if (csi.getCertificateStatus()
				.compareTo(CertificateStatus.VALID) != 0) {
			return false;
		} else {

			ValidationResult validationResult = signature.verify();
			return (validationResult.getType() == ValidationResultType.VALID);
		}
	}
	
	private static String getDocumentType(Document document){
		NodeList nodes = document.getElementsByTagNameNS("*","Invoice");
		if (nodes.getLength()>0){
			return "Invoice";
		}
		
		nodes = document.getElementsByTagNameNS("*","ProcessUserAccount");
		if (nodes.getLength()>0){
			return "UserAccount";
		}
		
		nodes = document.getElementsByTagNameNS("*","CancelUserAccount");
		if (nodes.getLength()>0){
			return "UserAccount";
		}
		
		nodes = document.getElementsByTagNameNS("*","ApplicationResponse");
		if (nodes.getLength()>0){
			return "AppResponse";
		}

		nodes = document.getElementsByTagNameNS("*","DespatchAdvice");
		if (nodes.getLength()>0){
			return "DespatchAdvice";
		}
		
		return null;
	}


	public static void main(String[] args) throws UnsupportedEncodingException, Exception {
	
		byte[] filecontent = FileReader.getFileContent("C:\\app\\faturalar\\DA7DE007-58EB-4A1E-851C-1E18DD01DB15\\E8293E8B-C2D3-46CA-BE77-B902950FD831.xml");
		boolean cc = validateSignature(new String(filecontent,"UTF-8"));
		System.err.println(cc);
		
//		String fileContent = new String(FileReader.getFileContent("C:\\Users\\taha\\Downloads\\cov_patlak.xml")).toString();
//
//		KeyStore keyStore = KeyStore.getInstance("pkcs12");
//		keyStore.load(new FileInputStream(new File("C:\\Users\\taha\\fatura\\schema\\faturadeneme@mmshs.gov.tr.pfx")), "075239".toCharArray());
//		XMLSigner xmlSigner = new XMLSigner(keyStore);
//		
//		new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
//		System.err.println(xmlSigner.signFile(fileContent.getBytes("UTF-8")));
		
	}

	private Document newEnvelope(byte[] fileContent)
			throws ESYAException {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			return db.parse(new ByteArrayInputStream(fileContent));
		} catch (Exception x) {
			// we shouldnt be here if ENVELOPE_XML is valid
			x.printStackTrace();
		}
		throw new ESYAException("Cant construct envelope xml ");
	}

}
