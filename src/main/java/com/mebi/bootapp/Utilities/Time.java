package com.mebi.bootapp.Utilities;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Time {


    public static boolean  isToday(Date date){
        Date bugun = new Date();
        Calendar gelenTarihCalendar = Calendar.getInstance();
        gelenTarihCalendar.setTime(date);
        Calendar bugunCalendar = Calendar.getInstance();
        bugunCalendar.setTime(bugun);
        int gelenTarihGun = gelenTarihCalendar.get(Calendar.DAY_OF_MONTH);
        int gelenTarihAy = gelenTarihCalendar.get(Calendar.MONTH);
        int gelenTarihYil = gelenTarihCalendar.get(Calendar.YEAR);

        int bugunGun = bugunCalendar.get(Calendar.DAY_OF_MONTH);
        int bugunAy = bugunCalendar.get(Calendar.MONTH);
        int bugunYil = bugunCalendar.get(Calendar.YEAR);

        return gelenTarihGun == bugunGun && gelenTarihAy == bugunAy && gelenTarihYil == bugunYil;
    }

    public static String  getDateString(Date date){

        LocalDate gelenTarih = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String gelenTarihFormatted = gelenTarih.format(formatter);

        return gelenTarihFormatted;





    }

    public static String[] SplitedDate(String value) {

        ArrayList<String> retList = new ArrayList<String>();
        String[] splits = value.split("-");
        for (String s : splits) {
            retList.add(s);
        }

        return retList.toArray(new String[retList.size()]);
    }


    public static Date ConvertToDate(String dateString) throws ParseException {
        String month = dateString.trim().substring(0, 2);
        String day = dateString.trim().substring(3, 5);
        String year = dateString.trim().substring(6, 10);

//		String hourse = dateString.trim().substring(11, 13);
//		String min = dateString.trim().substring(14, 16);
//		String second = dateString.trim().substring(17, 19);
        // 20120106
        dateString = year + month + day ;
        Date tradeDate;

        tradeDate = new SimpleDateFormat("yyyyMMdd").parse(dateString);

        // String krwtrDate = new SimpleDateFormat("yyyy-MM-dd",
        // Locale.ENGLISH).format(tradeDate);

        return tradeDate;
    }


    public static Date ConvertVueDateToJavaDate(String dateString) throws ParseException {
        DateTimeFormatter inputFormatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter inputFormatter2 = DateTimeFormatter.ofPattern("EEE MMM dd yyyy HH:mm:ss 'GMT'Z (z)");

        try {
            LocalDateTime dateTime;
            Date date;

            // dateString formatına göre işlem yap
            if (dateString.matches("\\d{4}-\\d{2}-\\d{2}")) {
                // Eğer yyyy-MM-dd formatında ise bu blok çalışır
                LocalDate localDate = LocalDate.parse(dateString, inputFormatter1);
                date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                return date;
            } else {
                // Diğer formatta ise bu blok çalışır
                dateTime = LocalDateTime.parse(dateString, inputFormatter2);
                date = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
                return date;
            }
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static int CalculateTimeSpent(String timeValue, String timeType) {

        int timeSpent = 0;

        if(timeType.equals("m")){

            timeSpent = Integer.parseInt(timeValue);
        }

        else if (timeType.equals("h")){

            timeSpent = (int) (Float.parseFloat(timeValue)*60);
        }

        else if (timeType.equals("d")){

            timeSpent = (int) ((Float.parseFloat(timeValue)*8)*60);
        }

        return timeSpent;
    }
}
