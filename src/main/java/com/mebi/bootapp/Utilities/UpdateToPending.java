package com.mebi.bootapp.Utilities;



import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.mebi.bootapp.Utilities.Log.*;

/**
 * @author Riza on 9/26/2022
 * @project IntelliJ IDEA
 */
public class UpdateToPending {

    private static final String OUTBOUND_INVOICES_QUERY = "";
    public static String DBURL = null;
    public static String DBYUSER = null;
    public static String DBPASS =null ;
    public static String INVOICESERIALNO =null ;

    public static void main(String[] args) throws SQLException {

        //get envelopes
        GetAppPropertiesData prop = new GetAppPropertiesData();
        Properties result = prop.GetProp();
        if(result!=null){
            DBURL = result.getProperty("efatura.prod.url");
            DBYUSER = result.getProperty("efatura.prod.username");
            DBPASS = result.getProperty("efatura.prod.password");
        }

        INVOICESERIALNO =
                "'SNP2022000057361'," +
                "'SNP2022000057363'," +
                "'TFA2022000004249'";


        String query = " SELECT e.DOCUMENTID , es.DOCUMENTSTATUS , c.TITLE ,e.ENVELOPEUUID  FROM  EFATURAPROD.ENVELOPES e INNER JOIN  EFATURAPROD.ENVELOPE_STATUS_DETAILS es ON es.ENVELOPEUUID = e.ENVELOPEUUID\n" +
                "   INNER JOIN EFATURAPROD.CUSTOMERS c  ON e.SENDERVKNTCKN = c.TAXID WHERE e.DOCUMENTID IN ("+ INVOICESERIALNO+")";

        Connection connectionTestDb = DriverManager.getConnection(DBURL, DBYUSER, DBPASS);
        PreparedStatement preparedStatement;
        preparedStatement  = connectionTestDb.prepareStatement(query);
        ResultSet OutboundResultSet = preparedStatement.executeQuery();
        StringBuilder builder = new StringBuilder();
        StringBuilder last = new StringBuilder();

        List<String> customers = new ArrayList<String>();




        OutboundResultSet.getFetchSize();
        builder.append("(");
        while (OutboundResultSet.next()) {

            if(!customers.contains(OutboundResultSet.getString("TITLE"))){
                customers.add(OutboundResultSet.getString("TITLE"));
            }



            builder.append("'");
            builder.append(OutboundResultSet.getString("ENVELOPEUUID"));
            builder.append("'");

            builder.append(",");
        }
        String s = builder.length() > 0 ? builder.substring(0, builder.length() - 1) : "";
        last.append(s);
        last.append(")");

        String update = "UPDATE EFATURAPROD.ENVELOPE_STATUS_DETAILS\n" +
                "SET DOCUMENTSTATUS='PENDING' \n" +
                "WHERE ENVELOPEUUID IN " + last+ ";";

        System.out.println(ANSI_GREEN+update+ANSI_RESET);

        for (String name : customers){

            System.out.println(ANSI_YELLOW+name+ANSI_RESET);

        }













        //update status

    }
}
