package com.mebi.bootapp;

import com.mebi.bootapp.Config.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Arrays;

/**
 * @author Riza on 10/31/2022
 * @project IntelliJ IDEA
 */

public class live {




    public static void main(String[] args) {
        //TODO remove above line
        System.setProperty("java.net.preferIPv4Stack", "true");

        startListening();


    }

    public static void startListening()  {


        DatagramPacket dgp;
        InetAddress addr;
        int port = 50005;
        AudioFormat audioFormat;
        SourceDataLine sourceLine = null;


        try {

            /*  Java Sound supports a wide variety of file types including AIFF, AU, and WAV.
                             It can render both 8- and 16-bit audio data in sample rates from 8 KHz to 48 KHZ.*/
            File file = new File("C:\\Users\\Riza\\Downloads\\103139185.wav");
            AudioInputStream in= AudioSystem.getAudioInputStream(file);
            AudioInputStream din = null;

            audioFormat = in.getFormat();
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
            try {
                sourceLine = (SourceDataLine) AudioSystem.getLine(info);
                sourceLine.open(audioFormat);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
            sourceLine.start();

            addr = InetAddress.getByName("225.6.7.8");
            MulticastSocket socket = new MulticastSocket();
            int nBytesRead = 0;
            System.out.println("Music Running");
            byte[] data = new byte[4096];
            System.out.println(System.getProperty("com.sun.management.jmxremote.port"));
            WebSocket webSocket = new WebSocket();
            while (nBytesRead != -1) {
                try {
                    nBytesRead = in.read(data, 0, data.length);
                    dgp = new DatagramPacket (data, data.length,addr,port);
                  //  socket.send(dgp);

                    webSocket.sendAllMessage( Arrays.toString(data));

                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (nBytesRead >= 0) {
                    FloatControl volumeControl = (FloatControl) sourceLine.getControl(FloatControl.Type.MASTER_GAIN);
                    volumeControl.setValue(-80.00f);
                    @SuppressWarnings("unused")
                    int nBytesWritten = sourceLine.write(data, 0, nBytesRead);
                }else{
                    startListening();
                }
            }
            System.out.println("Music Stoped");

        }catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        } // TODO: handle exception


    }
}
